#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hwdesign.cadence.allegro"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc

class TestGenerate_mkArtwork(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_run(self):
    import pyauto_hw.config

    brdPath = os.path.join(pyauto_base.fs.getModulePath("pyauto_hwdesign"), "tests",
                           "files", "board")
    fOutPath = os.path.join(self.lclDir, "data_out", "board.layers.txt")
    try:
      os.remove(fOutPath)
    except OSError:
      pass

    dictCLIArgs = {"brd": brdPath, "list": False, "details": False}
    pyauto_base.misc.runScriptMainApp("pyauto_hwdesign.cadence.allegro.generate.mkArtwork",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

    hashVal = [
        "f7a2c436d140f6d2369d0a01b662eadcb0ca0205ee6cb6f3f5afb88366795145",  # linux
        "a43a465d383c52925fbe6c8728b8cfac5deef6e81ee672ceae109ab0144bf0a9",  # win
    ]
    self.assertTrue(os.path.isfile(fOutPath), "file DOES NOT exist: {}".format(fOutPath))
    tmp = pyauto_base.misc.getFileHash(fOutPath)
    self.assertIn(tmp, hashVal, "INCORRECT hash for {} ({})".format(fOutPath, tmp))
