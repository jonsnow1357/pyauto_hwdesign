#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hwdesign.cadence.allegro"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest
import shutil

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc

class TestmkscrAll(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_run(self):
    import pyauto_hwdesign
    import pyauto_base.db
    import pyauto_base.config

    pathMod = os.path.dirname(pyauto_hwdesign.__file__)
    os.chdir(os.path.join(pathMod, "cadence", "allegro", "generate", "padstack"))

    path = pyauto_base.fs.mkAbsolutePath("WORK/parts_database/parts_master.sqlite",
                                         bExists=False)
    if (not os.path.isfile(path)):
      logger.info("path '{}' DOES NOT exist".format(path))
      self.skipTest("")
    path = pyauto_base.fs.mkAbsolutePath("WORK/CAD_Cadence_Allegro/padstack", bExists=False)
    if (not os.path.isdir(path)):
      logger.info("path '{}' DOES NOT exist".format(path))
      self.skipTest("")

    try:
      shutil.rmtree("data_out")
    except OSError:
      pass

    dbCtl = pyauto_base.db.DBController()
    dbCfg = pyauto_base.config.DBConfig()
    dictCLIArgs = {
        "cfg": "mkscrAll.xml",
        "list": False,
        "smt": True,
        "pth": True,
        "npth": True,
        "yes": False
    }
    pyauto_base.misc.runScriptMainApp(
        "pyauto_hwdesign.cadence.allegro.generate.padstack.mkscrAll",
        logger,
        appDir=os.getcwd(),
        appCfgPath=os.path.join(os.getcwd(), "mkscrAll.xml"),
        dictCLIArgs=dictCLIArgs,
        dictObjGlobal={
            "dbCfg": dbCfg,
            "dbCtl": dbCtl
        })

    self.assertGreater(len(os.listdir("data_out")), 0)
