#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hwdesign.cadence.allegro"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_hwdesign.cadence.allegro.generate.base as genBase

class TestGenerate_base(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_filterNone(self):
    fPath = os.path.join(self.inFolder, "test_read_pcb_info_1.txt")

    flt = genBase.PCBItemFilter()
    flt.callback = flt.filterNone
    lstItems = genBase.readAllegroInfo(fPath, flt)
    self.assertEqual(len(lstItems), 15)

    fPath = os.path.join(self.inFolder, "test_read_pcb_info_2.txt")

    flt = genBase.PCBItemFilter()
    flt.callback = flt.filterNone
    lstItems = genBase.readAllegroInfo(fPath, flt)
    self.assertEqual(len(lstItems), 11)

  def test_filterType(self):
    fPath = os.path.join(self.inFolder, "test_read_pcb_info_1.txt")

    flt = genBase.PCBItemFilter()
    flt.regexType = r"ARC SEGMENT"
    flt.callback = flt.filterType
    lstItems = genBase.readAllegroInfo(fPath, flt)
    self.assertEqual(len(lstItems), 1)

    flt = genBase.PCBItemFilter()
    flt.regexType = r"LINE"
    flt.callback = flt.filterType
    lstItems = genBase.readAllegroInfo(fPath, flt)
    self.assertEqual(len(lstItems), 5)

    flt = genBase.PCBItemFilter()
    flt.regexType = r"SHAPE"
    flt.callback = flt.filterType
    lstItems = genBase.readAllegroInfo(fPath, flt)
    self.assertEqual(len(lstItems), 1)

    fPath = os.path.join(self.inFolder, "test_read_pcb_info_2.txt")

    flt = genBase.PCBItemFilter()
    flt.regexType = r"SHAPE"
    flt.callback = flt.filterType
    lstItems = genBase.readAllegroInfo(fPath, flt)
    self.assertEqual(len(lstItems), 11)

  def test_filterTypeRadius(self):
    fPath = os.path.join(self.inFolder, "test_read_pcb_info_1.txt")

    flt = genBase.PCBItemFilter()
    flt.regexType = r"(ARC SEGMENT|LINE)"
    flt.dim = [2.25, 0.0]
    flt.callback = flt.filterTypeRadius
    lstItems = genBase.readAllegroInfo(fPath, flt)
    self.assertEqual(len(lstItems), 5)

    flt = genBase.PCBItemFilter()
    flt.regexType = r"(ARC SEGMENT|LINE)"
    flt.dim = [2.3, 0.0]
    flt.callback = flt.filterTypeRadius
    lstItems = genBase.readAllegroInfo(fPath, flt)
    self.assertEqual(len(lstItems), 0)
