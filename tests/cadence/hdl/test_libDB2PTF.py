#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hwdesign.cadence.hdl"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest
import shutil

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc

class TestLibDB2PTF(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_run(self):
    import pyauto_base.db
    import pyauto_hw.config

    path = pyauto_base.fs.mkAbsolutePath("WORK/parts_database/parts_master.sqlite",
                                         bExists=False)
    if (not os.path.isfile(path)):
      logger.info("path '{}' DOES NOT exist".format(path))
      self.skipTest("")

    try:
      shutil.rmtree("data_out")
    except OSError:
      pass
    pyauto_base.fs.mkOutFolder()

    dbCfg = pyauto_hw.config.LibConfig()
    dictCLIArgs = {
        "action": "lib",
        "cfg": os.path.join(self.inFolder, "hdl.xml"),
        "list": False,
        "cell": None
    }
    pyauto_base.misc.runScriptMainApp("pyauto_hwdesign.cadence.hdl.libDB2PTF",
                                      logger,
                                      dictCLIArgs=dictCLIArgs,
                                      dictObjGlobal={"dbCfg": dbCfg})

    fPathOut = os.path.join("data_out", "capacitors.ptf")
    self.assertTrue(os.path.isfile(fPathOut))
    self.assertGreater(os.path.getsize(fPathOut), 0)

    fPathOut = os.path.join("data_out", "connectors.ptf")
    self.assertTrue(os.path.isfile(fPathOut))
    self.assertGreater(os.path.getsize(fPathOut), 0)

    fPathOut = os.path.join("data_out", "ics.ptf")
    self.assertTrue(os.path.isfile(fPathOut))
    self.assertGreater(os.path.getsize(fPathOut), 0)

    fPathOut = os.path.join("data_out", "inductors.ptf")
    self.assertTrue(os.path.isfile(fPathOut))
    self.assertGreater(os.path.getsize(fPathOut), 0)

    fPathOut = os.path.join("data_out", "misc.ptf")
    self.assertTrue(os.path.isfile(fPathOut))
    self.assertGreater(os.path.getsize(fPathOut), 0)

    fPathOut = os.path.join("data_out", "oscillators.ptf")
    self.assertTrue(os.path.isfile(fPathOut))
    self.assertGreater(os.path.getsize(fPathOut), 0)
