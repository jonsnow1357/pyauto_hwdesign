#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hwdesign.cadence.hdl"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc

class TestLibSymCheck(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_run(self):
    import pyauto_base.db
    import pyauto_base.config

    path = pyauto_base.fs.mkAbsolutePath("WORK/CAD_Cadence_HDL/symbols", bExists=False)
    if (not os.path.isdir(path)):
      logger.info("path '{}' DOES NOT exist".format(path))
      self.skipTest("")

    dbCfg = pyauto_base.config.DBConfig()
    dictCLIArgs = {
        "cfg": os.path.join(self.inFolder, "hdl.xml"),
        "list": False,
        "cell": None
    }
    pyauto_base.misc.runScriptMainApp("pyauto_hwdesign.cadence.hdl.libSymCheck",
                                      logger,
                                      dictCLIArgs=dictCLIArgs,
                                      dictObjGlobal={"dbCfg": dbCfg})
