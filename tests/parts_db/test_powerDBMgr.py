#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hwdesign.parts_db.powerDBMgr"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc

class TestPowerDBMgr(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_run_list(self):
    import pyauto_base.config

    path = pyauto_base.fs.mkAbsolutePath("WORK/parts_database/power", bExists=False)
    if (not os.path.isdir(path)):
      logger.info("path '{}' DOES NOT exist".format(path))
      self.skipTest("")

    dictCLIArgs = {"action": "list"}
    pyauto_base.misc.runScriptMainApp("pyauto_hwdesign.parts_db.powerDBMgr",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

  #@unittest.skip("")
  def test_run_check(self):
    import pyauto_base.config

    path = pyauto_base.fs.mkAbsolutePath("WORK/parts_database/power", bExists=False)
    if (not os.path.isdir(path)):
      logger.info("path '{}' DOES NOT exist".format(path))
      self.skipTest("")

    dictCLIArgs = {"action": "check"}
    pyauto_base.misc.runScriptMainApp("pyauto_hwdesign.parts_db.powerDBMgr",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)
