#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hwdesign.parts"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.config

class TestPinsConvert(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  # @unittest.skip("")
  def test_run_base(self):
    fInPath = os.path.join(self.inFolder, "test_read1.pins.csv")
    fOutPath = os.path.join(self.outFolder, "test_write1.pins.json")
    dictCLIArgs = {
        "action": "pins2json",
        "inPath": fInPath,
        "outPath": fOutPath,
        "yes": False
    }
    pyauto_base.misc.runScriptMainApp("pyauto_hwdesign.parts_db.pinsConvert",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

    hashVal = [
        "f8d855b4f96242a82446532aee6ecdb8a3598d2fe1ddf3736a1532735edae512",  # linux
        "a9cab416cb8022eeb3be798766461b73fef357de38269ce2765a8b15fe98f685",  # win
    ]
    self.assertTrue(os.path.isfile(fOutPath), "file DOES NOT exist: {}".format(fOutPath))
    tmp = pyauto_base.misc.getFileHash(fOutPath)
    self.assertIn(tmp, hashVal, "INCORRECT hash for {} ({})".format(fOutPath, tmp))

    fInPath = os.path.join(self.inFolder, "test_read1.pins.json")
    fOutPath = os.path.join(self.outFolder, "test_write1.pins.csv")
    dictCLIArgs = {
        "action": "json2pins",
        "inPath": fInPath,
        "outPath": fOutPath,
        "yes": False
    }
    pyauto_base.misc.runScriptMainApp("pyauto_hwdesign.parts_db.pinsConvert",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

    hashVal = [
        "b5bd84067eccf57708ed6709d6836a3cd4ca00c9b00211f9785388936f6fcf90",  # linux
        "ddeaeb3d0e445f8f04729c635bfa74e7a160d7d2ed1b1a7a7b25608c31397816",  # win
    ]
    self.assertTrue(os.path.isfile(fOutPath), "file DOES NOT exist: {}".format(fOutPath))
    tmp = pyauto_base.misc.getFileHash(fOutPath)
    self.assertIn(tmp, hashVal, "INCORRECT hash for {} ({})".format(fOutPath, tmp))

  #@unittest.skip("")
  def test_run_mat2pins(self):
    fInPath = os.path.join(self.inFolder, "test_read_matrix1.csv")
    fOutPath = os.path.join(self.outFolder, "test_write_matrix1.pins.csv")
    dictCLIArgs = {
        "action": "mat2pins",
        "inPath": fInPath,
        "outPath": fOutPath,
        "yes": False
    }
    pyauto_base.misc.runScriptMainApp("pyauto_hwdesign.parts_db.pinsConvert",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

    #hashVal = ["xxx",
    #           ]
    self.assertTrue(os.path.isfile(fOutPath), "file DOES NOT exist: {}".format(fOutPath))
    #tmp = pyauto_base.misc.getFileHash(fOutPath)
    #self.assertIn(tmp, hashVal, "INCORRECT hash for {} ({})".format(fOutPath, tmp))

    fInPath = os.path.join(self.inFolder, "test_read_matrix2.csv")
    fOutPath = os.path.join(self.outFolder, "test_write_matrix2.pins.csv")
    dictCLIArgs = {
        "action": "mat2pins",
        "inPath": fInPath,
        "outPath": fOutPath,
        "yes": False
    }
    pyauto_base.misc.runScriptMainApp("pyauto_hwdesign.parts_db.pinsConvert",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

    #hashVal = ["xxx",
    #           ]
    self.assertTrue(os.path.isfile(fOutPath), "file DOES NOT exist: {}".format(fOutPath))
    #tmp = pyauto_base.misc.getFileHash(fOutPath)
    #self.assertIn(tmp, hashVal, "INCORRECT hash for {} ({})".format(fOutPath, tmp))

  # @unittest.skip("")
  def test_run_pins2hdlcsv(self):
    fInPath = os.path.join(self.inFolder, "test_read1.pins.csv")
    fOutPath = os.path.join(self.outFolder, "test_write1.hdl.csv")
    dictCLIArgs = {
        "action": "pins2hdlcsv",
        "inPath": fInPath,
        "outPath": fOutPath,
        "yes": False
    }
    pyauto_base.misc.runScriptMainApp("pyauto_hwdesign.parts_db.pinsConvert",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

    #hashVal = ["xxx",
    #           ]
    self.assertTrue(os.path.isfile(fOutPath), "file DOES NOT exist: {}".format(fOutPath))
    #tmp = pyauto_base.misc.getFileHash(fOutPath)
    #self.assertIn(tmp, hashVal, "INCORRECT hash for {} ({})".format(fOutPath, tmp))

  # @unittest.skip("")
  def test_run_hdlcsv2pins(self):
    fInPath = os.path.join(self.inFolder, "test_read1.hdl.csv")
    fOutPath = os.path.join(self.outFolder, "test_write1.pins.csv")
    dictCLIArgs = {
        "action": "hdlcsv2pins",
        "inPath": fInPath,
        "outPath": fOutPath,
        "yes": False
    }
    pyauto_base.misc.runScriptMainApp("pyauto_hwdesign.parts_db.pinsConvert",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

    #hashVal = ["xxx",
    #           ]
    self.assertTrue(os.path.isfile(fOutPath), "file DOES NOT exist: {}".format(fOutPath))
    #tmp = pyauto_base.misc.getFileHash(fOutPath)
    #self.assertIn(tmp, hashVal, "INCORRECT hash for {} ({})".format(fOutPath, tmp))
