#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hwdesign.tools.mtbf"""

#import site #http://docs.python.org/library/site.html
import sys
import os
import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_hwdesign.tools.mtbf.config

class TestSR332(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_StandardExample(self):
    bomName = "SR332"
    appCfg = pyauto_hwdesign.tools.mtbf.config.MTBFConfig()
    appCtl = pyauto_hwdesign.tools.mtbf.config.MTBF_DBController()
    dictCLIArgs = {
        "BOM": os.path.join(self.inFolder, "test_read_{}.dx.csv".format(bomName)),
        "cfg": os.path.join(self.lclDir, "{}.cfg".format(bomName)),
        "list": False,
        "db": False,
        "bom": False
    }
    pyauto_base.misc.runScriptMainApp("pyauto_hwdesign.tools.mtbf.SR332",
                                      logger,
                                      dictCLIArgs=dictCLIArgs,
                                      dictObjGlobal={
                                          "appCfg": appCfg,
                                          "appCtl": appCtl
                                      })

    fOutName = "test_read_{}.dx.SR-332.mI.e50.t40.envGF.mtbf.csv".format(bomName)
    fOutPath = os.path.join(self.lclDir, "data_out", fOutName)
    self.assertTrue(os.path.isfile(fOutPath), "file DOES NOT exist: {}".format(fOutPath))

    fPath = os.path.join(self.lclDir, "data_out", "test.csv")
    with open(fOutPath, "r") as fIn:
      with open(fPath, "w") as fOut:
        for ln in fIn.readlines():
          if (not ln.startswith(",,,,,,")):
            continue
          #print("DBG", ln)
          fOut.write(ln)
    hashVal = [
        "e808b5c318dd3b22ce851b2b07930703fae851a26d94046a8e3a48e8fe64d81f",  # linux
        "c7000e79887919b672a6edc5cd4b19bf305e3dc89ea82472a0ba5b38b0442569",  # linux
        "545ff9c115008ff75eeff90c472e81a997592ef6072df97062b8c9810f60ac9d",  # win
        "dbfc02711e0efbf21e73cab8f5f55f925cc7cd3c7bad3bcd83e0e66390c7622c",  # win
    ]
    tmp = pyauto_base.misc.getFileHash(fPath)
    self.assertIn(tmp, hashVal, "INCORRECT hash for {} ({})".format(fPath, tmp))
