#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hwdesign.tools.mtbf"""

#import site #http://docs.python.org/library/site.html
import sys
import os
import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_hwdesign.tools.mtbf.config

class TestTR332(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_StandardExample(self):
    bomName = "TR332"
    appCfg = pyauto_hwdesign.tools.mtbf.config.MTBFConfig()
    appCtl = pyauto_hwdesign.tools.mtbf.config.MTBF_DBController()
    dictCLIArgs = {
        "BOM": os.path.join(self.inFolder, "test_read_{}.dx.csv".format(bomName)),
        "cfg": os.path.join(self.lclDir, "{}.cfg".format(bomName)),
        "list": False,
        "db": False,
        "bom": False
    }
    pyauto_base.misc.runScriptMainApp("pyauto_hwdesign.tools.mtbf.TR332",
                                      logger,
                                      dictCLIArgs=dictCLIArgs,
                                      dictObjGlobal={
                                          "appCfg": appCfg,
                                          "appCtl": appCtl
                                      })

    fOutName = "test_read_{}.dx.TR-332.m1.e50.t40.envGF.mtbf.csv".format(bomName)
    fOutPath = os.path.join(self.lclDir, "data_out", fOutName)
    self.assertTrue(os.path.isfile(fOutPath), "file DOES NOT exist: {}".format(fOutPath))

    fPath = os.path.join(self.lclDir, "data_out", "test.csv")
    with open(fOutPath, "r") as fIn:
      with open(fPath, "w") as fOut:
        for ln in fIn.readlines():
          if (not ln.startswith(",,,,,,")):
            continue
          #print("DBG", ln)
          fOut.write(ln)
    hashVal = [
        "41a6c1bf7960712082b087493015bb5a1ebd269d8b2821ff0b054eff1fdffaac",  # linux
        "b74b5157734ed62669399638c2fa2abf838da7bf4a662bfcd651e8e998d05f6e",  # win
    ]
    tmp = pyauto_base.misc.getFileHash(fPath)
    self.assertIn(tmp, hashVal, "INCORRECT hash for {} ({})".format(fPath, tmp))
