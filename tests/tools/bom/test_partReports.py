#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hwdesign.sch"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_hw.config

class TestPartReports(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_run(self):
    brdPath = os.path.join(pyauto_base.fs.getModulePath("pyauto_hw"), "tests", "files",
                           "config", "project", "board1")

    dictCLIArgs = {"brd": brdPath, "list": False}
    pyauto_base.misc.runScriptMainApp("pyauto_hwdesign.tools.bom.partReports",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

    fOutPath = os.path.join(self.outFolder, "board1", "ac_coupling.csv")
    #lstHash = [
    #    "???"
    #]
    #tt.chkHash_file(self, fOutPath, lstHash)
    self.assertTrue(os.path.isfile(fOutPath), "file DOES NOT exist: {}".format(fOutPath))
    self.assertGreater(os.path.getsize(fOutPath), 200)

    fOutPath = os.path.join(self.outFolder, "board1", "capacitors.csv")
    #lstHash = [
    #    "???"
    #]
    #tt.chkHash_file(self, fOutPath, lstHash)
    self.assertTrue(os.path.isfile(fOutPath), "file DOES NOT exist: {}".format(fOutPath))
    self.assertGreater(os.path.getsize(fOutPath), 200)

    fOutPath = os.path.join(self.outFolder, "board1", "decoupling_capacitors.csv")
    #lstHash = [
    #    "???"
    #]
    #tt.chkHash_file(self, fOutPath, lstHash)
    self.assertTrue(os.path.isfile(fOutPath), "file DOES NOT exist: {}".format(fOutPath))
    self.assertGreater(os.path.getsize(fOutPath), 1000)

    fOutPath = os.path.join(self.outFolder, "board1", "ferrites.csv")
    # lstHash = [
    #    "???"
    # ]
    # tt.chkHash_file(self, fOutPath, lstHash)
    self.assertTrue(os.path.isfile(fOutPath), "file DOES NOT exist: {}".format(fOutPath))
    self.assertGreater(os.path.getsize(fOutPath), 100)

    fOutPath = os.path.join(self.outFolder, "board1", "i2c.csv")
    #lstHash = [
    #    "???"
    #]
    #tt.chkHash_file(self, fOutPath, lstHash)
    self.assertTrue(os.path.isfile(fOutPath), "file DOES NOT exist: {}".format(fOutPath))
    self.assertGreater(os.path.getsize(fOutPath), 50)

    fOutPath = os.path.join(self.outFolder, "board1", "ic.csv")
    #lstHash = [
    #    "???"
    #]
    #tt.chkHash_file(self, fOutPath, lstHash)
    self.assertTrue(os.path.isfile(fOutPath), "file DOES NOT exist: {}".format(fOutPath))
    self.assertGreater(os.path.getsize(fOutPath), 1000)

    fOutPath = os.path.join(self.outFolder, "board1", "inductors.csv")
    #lstHash = [
    #    "???"
    #]
    #tt.chkHash_file(self, fOutPath, lstHash)
    self.assertTrue(os.path.isfile(fOutPath), "file DOES NOT exist: {}".format(fOutPath))
    self.assertGreater(os.path.getsize(fOutPath), 50)

    fOutPath = os.path.join(self.outFolder, "board1", "resistors.csv")
    #lstHash = [
    #    "???"
    #]
    #tt.chkHash_file(self, fOutPath, lstHash)
    self.assertTrue(os.path.isfile(fOutPath), "file DOES NOT exist: {}".format(fOutPath))
    self.assertGreater(os.path.getsize(fOutPath), 1000)
