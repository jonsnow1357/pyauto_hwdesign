#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hwdesign.tools.power"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest
import shutil

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
skipAll = False
try:
  import pyauto_base.fs
  import pyauto_base.misc
  import pyauto_base.plot
except RuntimeError:
  skipAll = True
import pyauto_hw.config

class TestPowerSequenceXML(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

    if (skipAll):
      self.skipTest("")

  def tearDown(self):
    pass

  def _run_script(self, brdPath, lstSeq):
    path = pyauto_base.fs.mkAbsolutePath("WORK/parts_database/power", bExists=False)
    if (not os.path.isdir(path)):
      logger.info("path '{}' DOES NOT exist".format(path))
      self.skipTest("")

    brdPath = pyauto_base.fs.mkAbsolutePath(brdPath, bExists=False)
    if (not os.path.isdir(brdPath)):
      logger.info("path '{}' DOES NOT exist".format(brdPath))
      self.skipTest("")

    dictCLIArgs = {
        "brd": brdPath,
        "flt": "",
        "list": False,
        "hline": None,
        "vline": None,
        "minticks": False,
        "grid": "maj",
        "legend": "best",
        "scale": None,
        "xlbl": "time [ms]",
        "ylbl": "voltage",
        "xtend": 1,
        "save": True,
        "noplot": False
    }
    pyauto_base.misc.runScriptMainApp("pyauto_hwdesign.tools.power.powerSequenceXML",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

    for f in lstSeq:
      fOutPath = os.path.join(self.lclDir, "data_out", "{} power-on.png".format(f))
      #hashVal = ["xxx",
      #           ]
      self.assertTrue(os.path.isfile(fOutPath), "file DOES NOT exist: {}".format(fOutPath))
      #tmp = pyauto_base.misc.getFileHash(fOutPath)
      #self.assertIn(tmp, hashVal, "INCORRECT hash for {} ({})".format(fOutPath, tmp))

    try:
      shutil.rmtree("data_out")
    except OSError:
      pass

  #@unittest.skip("")
  def test_run1(self):
    self._run_script("WORK/dev/python/ottawa_hw/cfg/projects/DCO_RFB/DCO_RFB", ["DSP"])

  #@unittest.skip("")
  def test_run2(self):
    self._run_script("WORK/dev/python/ottawa_hw/cfg/projects/Atlantic/Atlantic_EVB",
                     ["ATLANTIC EVB (DSP only)", "ATLANTIC EVB (no DSP)"])
