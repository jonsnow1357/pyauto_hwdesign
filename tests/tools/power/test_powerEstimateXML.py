#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hwdesign.tools.power"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc

class TestPowerEstimateXML(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

    for f in os.listdir(self.outFolder):
      if (f.lower().endswith(".csv")):
        os.remove(os.path.join(self.outFolder, f))
      if (f.lower().endswith(".html")):
        os.remove(os.path.join(self.outFolder, f))
      if (f.lower().endswith(".txt")):
        os.remove(os.path.join(self.outFolder, f))

  def tearDown(self):
    pass

  def _run_script(self, brdPath, brdName):
    import pyauto_hw.config

    path = pyauto_base.fs.mkAbsolutePath("WORK/parts_database/power", bExists=False)
    if (not os.path.isdir(path)):
      logger.info("path '{}' DOES NOT exist".format(path))
      self.skipTest("")

    brdPath = pyauto_base.fs.mkAbsolutePath(brdPath, bExists=False)
    if (not os.path.isdir(brdPath)):
      logger.info("path '{}' DOES NOT exist".format(brdPath))
      self.skipTest("")

    dictCLIArgs = {"brd": brdPath, "list": False, "tree": False, "power": False}
    pyauto_base.misc.runScriptMainApp("pyauto_hwdesign.tools.power.powerEstimateXML",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

    fOutPath = os.path.join(self.lclDir, "data_out",
                            "{}_powerEstimateXML.csv".format(brdName))
    #hashVal = ["xxx",
    #           ]
    self.assertTrue(os.path.isfile(fOutPath), "file DOES NOT exist: {}".format(fOutPath))
    #tmp = pyauto_base.misc.getFileHash(fOutPath)
    #self.assertIn(tmp, hashVal, "INCORRECT hash for {} ({})".format(fOutPath, tmp))

    fOutPath = os.path.join(self.lclDir, "data_out",
                            "{}_powerEstimateXML.html".format(brdName))
    #hashVal = ["xxx",
    #           ]
    self.assertTrue(os.path.isfile(fOutPath), "file DOES NOT exist: {}".format(fOutPath))
    #tmp = pyauto_base.misc.getFileHash(fOutPath)
    #self.assertIn(tmp, hashVal, "INCORRECT hash for {} ({})".format(fOutPath, tmp))

  #@unittest.skip("")
  def test_run1(self):
    self._run_script("WORK/dev/python/ottawa_hw/cfg/projects/DCO_RFB/DCO_RFB", "DCO_RFB")
