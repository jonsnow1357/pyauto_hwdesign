#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc

dictOpts = {
    "main": ["datasheets", "docs"],
    "hw": ["compliance", "sch", "sch_pdf", "pcb", "lab"],
    "scripts": ["scripts"],
    "fpga": ["fpga"],
    "sw": ["sw"],
}

def mainApp():
  projPath = os.path.abspath(cliArgs["path"])
  pyauto_base.fs.mkOutFolder(projPath)

  subfolders = []
  logger.info("project folder: {}".format(projPath))
  for opt in cliArgs["options"].split(","):
    #logger.info("option: {}".format(opt))
    subfolders += dictOpts[opt]
  #logger.info(subfolders)

  lstNeeded = list(set(subfolders) - set(os.listdir(projPath)))
  #logger.info(lstNeeded)
  for f in lstNeeded:
    fPath = os.path.join(projPath, f)
    if (cliArgs["create"]):
      os.mkdir(fPath)
      logger.info("creating folder: {}".format(fPath))
    else:
      logger.warning("folder NOT created: {}".format(fPath))

  lstExtra = list(set(os.listdir(projPath)) - set(subfolders))
  #logger.info(lstExtra)
  for f in lstExtra:
    fPath = os.path.join(projPath, f)
    if (os.path.isfile(fPath)):
      logger.warning("file SHOULD NOT exist: {}".format(f))
    elif (os.path.isdir(fPath)):
      logger.warning("folder SHOULD NOT exits: {}".format(f))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "check project folder"
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  parser.add_argument("path", help="project folder")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("-c",
                      "--create",
                      action="store_true",
                      default=False,
                      help="create subfolders")
  parser.add_argument("-o",
                      "--options",
                      default="main",
                      help="comma separated project options: [{}]".format("|".join(
                          dictOpts.keys())))

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
