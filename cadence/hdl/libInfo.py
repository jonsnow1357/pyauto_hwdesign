#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.config
import pyauto_hw.CAD.HDL as HDL

dbCfg = pyauto_base.config.DBConfig()
stats = pyauto_base.misc.StatCounter()

def _cell_read(cell):
  cell.read()
  logger.info("  " + str(cell))
  stats.inc("cell")

def _ptf_insert(results, libName, cellName, primitive, ptf):
  if (ptf is None):
    results.add([libName, cellName, primitive.name, "-"])
    return

  for d in ptf.partEntries:
    fptName = "-"
    if ("JEDEC_TYPE" in d.keys()):
      fptName = d["JEDEC_TYPE"]
    results.add([libName, cellName, primitive.name, fptName])
    stats.inc("ptf")

def _ptf():
  """
  make a report with all JEDEC_TYPE property from .ptf file
  """
  outPath = pyauto_base.fs.mkOutFolder()
  resultsName = "report_ptf"
  results = pyauto_base.misc.ResultLog(os.path.join(outPath, "{}.csv".format(resultsName)))
  results.add(["lib name", "cell name", "primitive", "footprint"])

  dictLib = HDL.getLibraries(dbCfg.path["lib"])
  for libName, lib in dictLib.items():
    lstCellNames = lib.getCellNames()
    logger.info(lib)
    stats.inc("lib")

    for cellName in lstCellNames:
      cell = lib.getCell(cellName)
      _cell_read(cell)

      if ((cell.nPrimitives == 0) and (cell.nPTFs == 0)):
        pass
      elif ((cell.nPrimitives == 0) and (cell.nPTFs != 0)):
        msg = "{}/{} has {} primitive(s) and {} PTF(s)".format(libName, cellName,
                                                               cell.nPrimitives, cell.nPTFs)
        logger.warning(msg)
        pass
      elif ((cell.nPrimitives == 1) and (cell.nPTFs == 0)):
        prim = cell.getPrimitive()
        _ptf_insert(results, libName, cellName, prim, None)
      elif ((cell.nPrimitives == 1) and (cell.nPTFs == 1)):
        prim = cell.getPrimitive()
        ptf = cell.getPTF()
        if (prim.name != ptf.name):
          msg = "{}/{} has primitive '{}' != PTF '{}'".format(libName, cellName, prim.name,
                                                              ptf.name)
          logger.warning(msg)
        _ptf_insert(results, libName, cellName, prim, ptf)
      elif (((cell.nPrimitives == 1) and (cell.nPTFs > 1))
            or ((cell.nPrimitives > 1) and (cell.nPTFs == 1))):
        for i in range(0, cell.nPrimitives):
          prim = cell.getPrimitive(i)
          cntPTF = 0
          for j in range(0, cell.nPTFs):
            ptf = cell.getPTF(j)
            if (prim.name == ptf.name):
              _ptf_insert(results, libName, cellName, prim, ptf)
              cntPTF += 1
          if (cntPTF == 0):
            _ptf_insert(results, libName, cellName, prim, cell.getPTF(cntPTF))
          elif (cntPTF > 1):
            raise RuntimeError
      else:
        for i in range(0, cell.nPrimitives):
          prim = cell.getPrimitive(i)
          cntPTF = 0
          for j in range(0, cell.nPTFs):
            ptf = cell.getPTF(j)
            if (prim.name == ptf.name):
              _ptf_insert(results, libName, cellName, prim, ptf)
              cntPTF += 1
          if (cntPTF != 1):
            raise RuntimeError

def _sym():
  """
  make a report with all properties from every symbol
  """
  outPath = pyauto_base.fs.mkOutFolder()
  resultsName = "report_sym"
  results = pyauto_base.misc.ResultLog(os.path.join(outPath, "{}.csv".format(resultsName)))
  results.add(["lib name", "cell name", "sym name"])

  dictLib = HDL.getLibraries(dbCfg.path["lib"])
  for libName, lib in dictLib.items():
    lstCellNames = lib.getCellNames()
    logger.info(lib)
    stats.inc("lib")

    for cellName in lstCellNames:
      cell = lib.getCell(cellName)
      _cell_read(cell)

      for sym in cell.symbols.values():
        logger.info("    {}".format(sym))
        stats.inc("sym")
        results.add([libName, cellName, sym.name] + sorted(sym.getAllProperties().keys()))

def _pins():
  """
  make a report with all properties from every pin
  """
  outPath = pyauto_base.fs.mkOutFolder()
  resultsName = "report_pins"
  results = pyauto_base.misc.ResultLog(os.path.join(outPath, "{}.csv".format(resultsName)))
  results.add(["cell name", "sym name", "pin name", "text size"])

  dictLib = HDL.getLibraries(dbCfg.path["lib"])
  for libName, lib in dictLib.items():
    lstCellNames = lib.getCellNames()
    logger.info(lib)
    stats.inc("lib")

    for cellName in lstCellNames:
      cell = lib.getCell(cellName)
      _cell_read(cell)

      if (cell.nPrimitives == 0):
        continue

      logger.info("    {}".format(cell.getPrimitive()))
      for sym in cell.symbols.values():
        logger.info("    {}".format(sym))
        #print("DBG", sym._outline)
        for pn in sym.pinNumbers:
          pin = sym.getPinByNumber(pn)
          logger.info("      {}".format(pin))
          results.add([cellName, sym.name, pin.name, pin.Cline.split()[8]])
          #print("DBG", sym.getPinGeometry(pin.length, pin.side, pin.order))
          #logger.info("        properties: {}".format(pin.getAllProperties()))

def mainApp():
  logger.info("using config file: {}".format(os.path.realpath(cliArgs["cfg"])))
  #appCfg.loadCfgFile(cliArgs["cfg"])
  dbCfg.loadXmlFile(cliArgs["cfg"])
  if (cliArgs["list"]):
    dbCfg.showInfo()
    return

  t0 = datetime.datetime.now()

  if (cliArgs["action"] == "lib"):
    dictLib = HDL.getLibraries(dbCfg.path["lib"])
    for libName, lib in dictLib.items():
      logger.info(lib)
      stats.inc("lib")
  if (cliArgs["action"] == "cell"):
    dictLib = HDL.getLibraries(dbCfg.path["lib"])
    for libName, lib in dictLib.items():
      lstCellNames = lib.getCellNames()
      logger.info(lib)
      stats.inc("lib")

      logger.info("== cells:")
      for cellName in lstCellNames:
        logger.info("  " + cellName)
        stats.inc("cell")
  elif (cliArgs["action"] == "ptf"):
    _ptf()
  elif (cliArgs["action"] == "sym"):
    _sym()
  elif (cliArgs["action"] == "pins"):
    _pins()

  stats.show()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  appCfgPath = os.path.join(appDir, (modName + ".xml"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "HDL library info/reports"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("action",
                      help="action",
                      choices=("lib", "cell", "ptf", "sym", "pins"))
  parser.add_argument("-f", "--cfg", default=appCfgPath, help="configuration file path")
  parser.add_argument("-l",
                      "--list",
                      action="store_true",
                      default=False,
                      help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
