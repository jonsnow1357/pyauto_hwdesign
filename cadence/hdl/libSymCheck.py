#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.config
import pyauto_base.db
import pyauto_hw.CAD.base as CADbase
import pyauto_hw.CAD.HDL as HDL

#regex_SymV1 = r"[-0-9\.]+ [-0-9\.]+ [-0-9\.]+ [-0-9\.]+ 32 0 0 1 0 0 1 0 0"
#regex_SymV2 = r"[-0-9\.]+ [-0-9\.]+ [-0-9\.]+ [-0-9\.]+ 32 0 0 1 0 0 1 0 70"
#regex_SymI1 = r"[-0-9\.]+ [-0-9\.]+ [-0-9\.]+ [-0-9\.]+ 32 0 0 1 0 0 0 0 32"
#regex_PinV1 = r"[-0-9\.]+ [-0-9\.]+ [-0-9\.]+ [-0-9\.]+ 24 0 0 [0-3] 0 0 1 0 0"
#regex_PinI1 = r"[-0-9\.]+ [-0-9\.]+ [-0-9\.]+ [-0-9\.]+ 32 0 0 [0-3] 0 0 0 0 0"

lstRegexSymProp = [
    r"[0-9\-\.]+ [0-9\-\.]+ (0|90|180|270)(.00)? 0(.00)? (22|32|48) 0 0 (0|1) 0 0 (0|1) 0 (0|32|74)",
]
lstRegexPinProp = [
    r"[0-9\-\.]+ [0-9\-\.]+ (0|90|180|270)(.00)? 0(.00)? 24 0 0 (0|2) 0 0 (1|2) 0 0",
]

dbCfg = pyauto_base.config.DBConfig()
#dbCtl = pyauto_base.db.DBController()
'''
class CellCheck(object):
  """
  Class for Cell checking information.
  :var cellName:
  :var template: template for checking symbol view
  :var partName:
  :var dictSymProps: dictionary of {<prop_name>: [<prop_value>, <prop_attrs>], ...}
    that have to be present on the symbol
  :var dictPinProps: dictionary of {<prop_name>: [<prop_value>, <prop_attrs>], ...}
    that have to be present on the pins
  """

  def __init__(self):
    self.cellName = None
    self.template = None
    self.partName = None
    self.dictSymProps = {}
    self.dictPinProps = {}

  def __str__(self):
    return "{}: {}<-{} ({}/{} sym/pin props)".format(self.__class__.__name__, self.cellName,
                                                     self.template, len(self.dictSymProps),
                                                     len(self.dictPinProps))

def _readTemplateDefaults(fPath):
  #logger.info(fPath)
  props_sym = {}
  props_pin = {}
  #bOK = False
  with open(fPath, "r") as fIn:
    for ln in [t.strip("\n\r") for t in fIn.readlines()]:
      if (ln.startswith("@#props_sym")):
        props_sym = json.loads(ln[11:])
      if (ln.startswith("@#props_pin")):
        props_pin = json.loads(ln[11:])
      #if(ln.startswith("@require(")):
      #  bOK = True
  #logger.info(props_sym)
  #logger.info(props_pin)
  #if(not bOK):
  #  msg = "INCORRECT template: {}".format(fPath)
  #  logger.error(msg)
  #  raise RuntimeError(msg)

  return [props_sym, props_pin]

def _compareTemplate(fPath, tplPath, dictTpl):
  """
  :param fPath: target file for comparison
  :param tplPath: template for the source file for comparison
  :param dictTpl: dictionary of template values
  """
  #logger.info(fPath)
  #logger.info(tplPath)
  #logger.info(dictTpl)
  lclPath = "symbol.css"
  pyauto_base.misc.templateWheezy(tplPath, lclPath, dictTpl)

  lclHash = pyauto_base.misc.getFileHash(lclPath)
  rmtHash = pyauto_base.misc.getFileHash(fPath)
  if (lclHash != rmtHash):
    logger.warning("replacing: {}".format(fPath))
    #raise RuntimeError
    shutil.move(lclPath, fPath)

  if (pyauto_base.fs.chkPath_File(lclPath)):
    os.remove(lclPath)

def _checkSymbolTemplate(hdlCfg, cellName, cellCheck):
  """
  checks cell symbols against pyauto_templates
  :param hdlCfg: pyauto_hw.config.HDL.HDLSymbolCfg object
  :param cellName:
  :param cellCheck: CellCheck object
  """
  lc = pyauto_hw.CAD.HDL.LibCellView(hdlCfg.path)
  symPath = lc.getCellFile_Symbols(cellName)

  for symId in symPath.keys():
    tplFile = "{}.{}.tpl".format(cellCheck.template, symId)
    tplPath = os.path.join("pyauto_templates", tplFile)
    logger.info("check template: {} vs. {}".format(tplFile, symPath[symId]))

    props_sym, props_pin = _readTemplateDefaults(tplPath)
    dictTpl = {}
    dictTpl.update(props_sym)
    dictTpl.update(props_pin)
    #logger.info(dictTpl)
    _compareTemplate(symPath[symId], tplPath, dictTpl)

def _updateSymbolProperties(cck):
  """
  :param cck: CellCheck() object
  :return: updated CellCheck().dictSymProps
  """
  #for k, v in cck.dictSymProps.items():
  #  logger.info([k, v])
  #if("VALUE" in cck.dictSymProps.keys()):
  cck.dictSymProps["VALUE"] = [cck.dictSymProps["VALUE"], regex_SymV2]
  cck.dictSymProps["$LOCATION"] = ["REFDES", regex_SymV1]
  cck.dictSymProps["DNP"] = [r"(\.|DNP)", regex_SymV2]
  cck.dictSymProps["MFR"] = ["-", regex_SymI1]
  cck.dictSymProps["MFR_PN"] = ["-", regex_SymI1]
  cck.dictSymProps["$PATH"] = [r"\?", regex_SymI1]
  cck.dictSymProps["$JEDEC_TYPE"] = [r"\?", regex_SymI1]
  cck.dictSymProps["CDS_LMAN_SYM_OUTLINE"] = [
      None, "0 0 0(\\.00)? 0(\\.00)? 22 0 0 0 0 0 0 0 0"
  ]
  #logger.info("~~~~")
  #for k, v in cck.dictSymProps.items():
  #  logger.info([k, v])

def _updatePinProperties(cck):
  """
  :param cck: CellCheck() object
  :return: updated CellCheck().dictPinProps
  """
  #for k, v in cck.dictPinProps.items():
  #  logger.info([k, v])
  cck.dictPinProps["$PN"] = [r"\?", regex_PinI1]
  cck.dictPinProps["PIN_TEXT"] = [None, regex_PinV1]
  #logger.info("~~~~")
  #for k, v in cck.dictPinProps.items():
  #  logger.info([k, v])

def _checkSymbolViews(hdlCfg, cp, cellCheck):
  """
  :param hdlCfg: pyauto_hw.config.HDL.HDLSymbolCfg object
  :param cp: Component() read from library
  :param cellCheck: CellCheck object
  """
  #logger.info(hdlCfg)
  #logger.info(propsSym)
  #logger.info(propsPin)

  lstSymIds = [k for k in cp.CAD.keys() if k.startswith("sym")]
  for symId in sorted(lstSymIds):
    logger.info("check properties for {}.{}".format(cp.value, symId))
    sym = cp.CAD[symId]
    set1 = set(list(sym.getAllProperties().keys()))
    set2 = set(list(cellCheck.dictSymProps.keys()))
    diffSet12 = set1 - set2
    diffSet21 = set2 - set1
    if (len(diffSet12) != 0):
      msg = "{} symbol {} has EXTRA properties: {}".format(cp.value, symId, diffSet12)
      logger.error(msg)
      raise RuntimeError(msg)
    if (len(diffSet21) != 0):
      msg = "{} symbol {} is MISSING properties: {}".format(cp.value, symId, diffSet21)
      logger.error(msg)
      raise RuntimeError(msg)

    set1 = set(list(sym.getPropsPin().keys()))
    set2 = set(list(cellCheck.dictPinProps.keys()))
    diffSet12 = set1 - set2
    diffSet21 = set2 - set1
    if (len(diffSet12) != 0):
      msg = "{} symbol {} pin has EXTRA properties: {}".format(cp.value, symId, diffSet12)
      logger.error(msg)
      raise RuntimeError(msg)
    if (len(diffSet21) != 0):
      msg = "{} symbol {} pin is MISSING properties: {}".format(cp.value, symId, diffSet21)
      logger.error(msg)
      raise RuntimeError(msg)

    for k, v in sym.getAllProperties().items():
      #logger.info([k, v])
      #logger.info(cellCheck.dictSymProps[k])
      f_propval = v[0]
      f_propattr = v[1]
      ck_propval = cellCheck.dictSymProps[k][0]
      ck_propattr = cellCheck.dictSymProps[k][1]
      if ((ck_propval is not None) and (re.match(ck_propval, f_propval) is None)):
        msg = "{} symbol {} has INCORRECT property {} value: '{}' != '{}'".format(
            cp.value, symId, k, f_propval, ck_propval)
        logger.error(msg)
        raise RuntimeError(msg)
      #elif(ck_propval is None):
      #  logger.warning("{} symbol {} IGNORE property {} value".format(cp.value, symId, k))
      if ((ck_propattr is not None) and (re.match(ck_propattr, f_propattr) is None)):
        msg = "{} symbol {} has INCORRECT property {} attrs: '{}' != '{}'".format(
            cp.value, symId, k, f_propattr, ck_propattr)
        logger.error(msg)
        raise RuntimeError(msg)
      elif (ck_propattr is None):
        logger.warning("{} symbol {} IGNORE property {} attrs".format(cp.value, symId, k))

    for k, v in sym.getPropsPin().items():
      #logger.info([k, v])
      #logger.info(propsPin[k])
      f_propval = v[0]
      f_propattr = v[1]
      ck_propval = cellCheck.dictPinProps[k][0]
      ck_propattr = cellCheck.dictPinProps[k][1]
      if ((ck_propval is not None) and (re.match(ck_propval, f_propval) is None)):
        msg = "{} symbol {} has INCORRECT pin property {} value: '{}' != '{}'".format(
            cp.value, symId, k, f_propval, ck_propval)
        logger.error(msg)
        raise RuntimeError(msg)
      #elif(ck_propval is None):
      #  logger.warning("{} symbol {} IGNORE pin property {} value".format(cp.value, symId, k))
      if ((ck_propattr is not None) and (re.match(ck_propattr, f_propattr) is None)):
        msg = "{} symbol {} has INCORRECT pin property {} attrs: '{}' != '{}'".format(
            cp.value, symId, k, f_propattr, ck_propattr)
        logger.error(msg)
        raise RuntimeError(msg)
      elif (ck_propattr is None):
        logger.warning("{} symbol {} IGNORE pin property {} attrs".format(
            cp.value, symId, k))

    for ln in sym.lnDrawLine:
      tmp = ln.split(" ")
      if ((tmp[-2] != "-1") and (tmp[-1] != "0")):
        msg = "{} symbol {} has INCORRECT draw line: '{}'".format(cp.value, symId, ln)
        logger.error(msg)
        raise RuntimeError(msg)

    for ln in sym.lnDrawWire:
      tmp = ln.split(" ")
      if ((tmp[-2] != "-1") and (tmp[-1] != "0")):
        msg = "{} symbol {} has INCORRECT draw wire: '{}'".format(cp.value, symId, ln)
        logger.error(msg)
        raise RuntimeError(msg)

def checkLibrary(hdlCfg):
  """
  :param hdlCfg: pyauto_hw.config.HDL.HDLSymbolCfg object
  """
  #logger.info(hdlCfg)
  if (hdlCfg.path is None):
    logger.warning("HDL path NOT DEFINED")
    return
  if (hdlCfg.database["table"] is None):
    logger.warning("HDL database table NOT DEFINED")
    return
  if (hdlCfg.database["map"] is None):
    logger.warning("HDL database map NOT DEFINED")
    return

  qryRes = dbCtl.selectTable(hdlCfg.database["table"])
  #logger.info(qryRes)

  dictCell = {}
  for row in qryRes:
    params = dict([(k, row[hdlCfg.database["map"][k]])
                   for k in hdlCfg.database["map"].keys()])
    #logger.info(params)
    if (params["HDL_CELL"] is None):
      continue  # ignoring parts that have no HDL symbol

    if (params["HDL_CELL"] not in dictCell.keys()):
      cck = CellCheck()
      cck.cellName = params["HDL_CELL"]
      cck.partName = params["HDL_PART_NAME"]
      cck.template = params["HDL_TEMPLATE"]
      for k in params.keys():
        if (k in hdlCfg.lstParams):
          cck.dictSymProps[k] = params[k]  # check property and value
      dictCell[cck.cellName] = cck
    else:
      cck = dictCell[params["HDL_CELL"]]
      if (params["HDL_PART_NAME"] != cck.partName):
        msg = "DIFFERENT 'HDL_PART_NAME' for {}".format(params["HDL_CELL"])
        logger.error((msg))
        raise RuntimeError(msg)
      if (params["HDL_TEMPLATE"] != cck.template):
        msg = "DIFFERENT 'HDL_TEMPLATE' for {}".format(params["HDL_CELL"])
        logger.error((msg))
        raise RuntimeError(msg)
      for k in params.keys():
        if (k in hdlCfg.lstParams):
          if (params[k] != cck.dictSymProps[k]):
            cck.dictSymProps[k] = None  # check property exists only
  #for v in dictCell.values():
  #  logger.info(v)
  logger.info("found {} HDL symbol(s) in table {}".format(len(dictCell),
                                                          hdlCfg.database["table"]))

  lstLibRefdes = []
  lstLibClass = []
  for cell in pyauto_hw.CAD.HDL.listCells(hdlCfg.path):
    #logger.info(cell)
    if (cell not in dictCell.keys()):
      continue  # ignoring cells that are in the library but not defined in the database

    cp = pyauto_hw.CAD.HDL.readCell(hdlCfg.path, cell)
    logger.info(cp)
    if ((cp.CAD["meta"].nPrimitives > 1) or (cp.CAD["meta"].nPackages > 1)):
      msg = "{} HAS TOO MANY primitives or packages : {}".format(cp.value, cp.CAD["meta"])
      logger.error(msg)
      raise RuntimeError(msg)

    cck = dictCell[cell]
    if (cp.CAD["meta"].nFGroups > 1):
      cck.dictSymProps["SPLIT_INST"] = ["TRUE", regex_SymI1]
      cck.dictSymProps["CDS_LMAN_SPLIT_NUMBER"] = [None, regex_SymV2]

    if (pyauto_base.misc.isEmptyString(cck.template)):
      _updateSymbolProperties(cck)
      _updatePinProperties(cck)

      if (cp.value != cck.partName):
        msg = "{} HAS INCORRECT part name : {} != {}".format(cp.value, cp.value,
                                                             cck.partName)
        logger.error(msg)
        raise RuntimeError(msg)

      if (cp.footprint != ""):
        logger.warning("cell {} has JEDEC_TYPE hard-coded".format(cp.value))
        if (cp.footprint != cck.dictSymProps["JEDEC_TYPE"]):
          msg = "{} HAS INCORRECT footprint : {} != {}".format(
              cp.value, cp.footprint, cck.dictSymProps["JEDEC_TYPE"])
          logger.error(msg)
          raise RuntimeError(msg)

      _checkSymbolViews(hdlCfg, cp, cck)
    else:
      _checkSymbolTemplate(hdlCfg, cell, cck)

    if (cp.refdes not in lstLibRefdes):
      if ((cp.refdes == "?") and (cp.nPins == 0)):
        continue  # ignoring documentation parts
      lstLibRefdes.append(cp.refdes)
    if (("CLASS" in cp.params.keys()) and (cp.params["CLASS"] not in lstLibClass)):
      lstLibClass.append(cp.params["CLASS"])

  logger.info("summary of library refdes: {}".format(lstLibRefdes))
  logger.info("summary of library class: {}".format(lstLibClass))
'''

def _chk_sym_pins(sym):
  for pn in sym.pinNumbers:
    pin = sym.getPinByNumber(pn)

    dictProps = pin.getAllProperties()
    for k, v in dictProps.items():
      bFmt = False
      for regex in lstRegexPinProp:
        if (re.match(regex, v[1]) is not None):
          bFmt = True
          break
      if (not bFmt):
        logger.error("pin property '{}' has WRONG FORMAT".format(k))
        logger.error("  '{}'".format(v[1]))

def _chk_sym_prop_outline(sym):
  propName = "CDS_LMAN_SYM_OUTLINE"
  prop = sym.getProperty(propName)
  if (prop is None):
    logger.error("property '{}' MISSING".format(propName))
    return

  x_min, y_min, x_max, y_max = [float(t) for t in prop[0].strip("\"").split(",")]
  if (((x_min + y_min + x_max + y_max) % sym.grid) != 0.0):
    logger.error("property '{}' NOT ON GRID".format(propName))

def _chk_sym_prop(sym):
  _chk_sym_prop_outline(sym)

  dictProps = sym.getAllProperties()
  for k, v in dictProps.items():
    bFmt = False
    for regex in lstRegexSymProp:
      if (re.match(regex, v[1]) is not None):
        bFmt = True
        break
    if (not bFmt):
      logger.error("property '{}' has WRONG FORMAT".format(k))
      logger.error("  '{}'".format(v[1]))

def mainApp():
  logger.info("using config file: {}".format(os.path.realpath(cliArgs["cfg"])))
  dbCfg.loadXmlFile(cliArgs["cfg"])
  if (cliArgs["list"]):
    dbCfg.showInfo()
    return

  t0 = datetime.datetime.now()

  #dbCtl.dbInfo = appCfg.dictDBInfo["0"]
  #dbCtl.connect()

  dictLib = HDL.getLibraries(dbCfg.path["lib"])
  for libName, lib in dictLib.items():
    lstCellNames = lib.getCellNames()
    logger.info(lib)

    for cellName in lstCellNames:
      cell = lib.getCell(cellName)
      cell.read()
      logger.info("  " + str(cell))

      for sym in cell.symbols.values():
        logger.info("    {}".format(sym))
        _chk_sym_prop(sym)
        _chk_sym_pins(sym)

  #dbCtl.disconnect()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  appCfgPath = os.path.join(appDir, (modName + ".xml"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "library symbol check"
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  parser.add_argument("-f", "--cfg", default=appCfgPath, help="configuration file path")
  parser.add_argument("-l",
                      "--list",
                      action="store_true",
                      default=False,
                      help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
