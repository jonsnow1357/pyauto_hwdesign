#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.db
import pyauto_hw.config
import pyauto_hw.CAD.HDL

dbCfg = pyauto_hw.config.LibConfig()
dbCtl = pyauto_base.db.DBController()

class PTFPart(object):

  def __init__(self, partName):
    self.partName = partName
    self.lstKey = []
    self.lstInject = []
    self._data = []

  def addData(self, dictData):
    if (set(dictData.keys()) != set(self.lstKey + self.lstInject)):
      logger.warning("INCORRECT data: {}".format(dictData))
      return

    self._data.append(dictData)

  @property
  def nData(self):
    return len(self._data)

  def getLine_hdr(self):
    return ":{} = {};".format(
        " | ".join(["'{}'".format(k) for k in sorted(self.lstKey)]),
        " | ".join(["'{}'".format(k) for k in sorted(self.lstInject)]))

  def getLine_data(self, dId):
    return " {} = {}".format(
        " | ".join(["'{}'".format(self._data[dId][k]) for k in sorted(self.lstKey)]),
        " | ".join(["'{}'".format(self._data[dId][k]) for k in sorted(self.lstInject)]))

def _writePTF(ptfCfg, dbInfo):
  logger.info(ptfCfg)
  #print("DBG", dbInfo)

  dictPtfParts = {}
  dbCtl.dbInfo = dbInfo
  dbCtl.connect()

  qryRes = dbCtl.selectTable(ptfCfg.dbTable)
  for res in qryRes:
    partName = res[ptfCfg.dbMap["HDL_PART_NAME"]]
    if (partName is None):
      continue
    if (re.match(ptfCfg.partName, partName) is None):
      continue
    if (partName not in dictPtfParts.keys()):
      tmp = PTFPart(partName)
      tmp.lstKey = list(ptfCfg.propsKey.keys())
      tmp.lstInject = list(ptfCfg.propsInj.keys())
      dictPtfParts[tmp.partName] = tmp

    data = {}
    for k, v in ptfCfg.propsKey.items():
      data[k] = res[ptfCfg.dbMap[v]]
    for k, v in ptfCfg.propsInj.items():
      data[k] = res[ptfCfg.dbMap[v]]
    dictPtfParts[partName].addData(data)

  dbCtl.disconnect()

  for k in dictPtfParts.keys():
    logger.info("part '{}' ({:d} entry(es))".format(k, dictPtfParts[k].nData))

  with open(ptfCfg.path, "w") as fOut:
    fOut.write("FILE_TYPE = MULTI_PHYS_TABLE;\n")
    fOut.write("\n")

    for part in dictPtfParts.values():
      fOut.write("PART '{}'\n".format(part.partName))
      fOut.write(
          "{========================================================================================}\n"
      )
      fOut.write(part.getLine_hdr() + "\n")
      fOut.write(
          "{========================================================================================}\n"
      )
      for i in range(0, part.nData):
        fOut.write(part.getLine_data(i) + "\n")
      fOut.write("END_PART\n")
      fOut.write("\n")
    fOut.write("END.\n")

  logger.info("{} written".format(ptfCfg.path))

def mainApp():
  logger.info("using config file: {}".format(os.path.realpath(cliArgs["cfg"])))
  dbCfg.loadXmlFile(cliArgs["cfg"])
  if (cliArgs["list"]):
    dbCfg.showInfo()
    return

  t0 = datetime.datetime.now()

  for ptfCfg in dbCfg.lstPTFs:
    if (ptfCfg.dbId not in dbCfg.dictDBInfo):
      msg = "CANNOT find database id '{}' for ptf '{}'".format(ptfCfg.dbId,
                                                               ptfCfg.regexPartName)
      logger.error(msg)
      raise RuntimeError(msg)
    _writePTF(ptfCfg, dbCfg.dictDBInfo[ptfCfg.dbId])

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  appCfgPath = os.path.join(appDir, (modName + ".xml"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "create HDL ptf files"
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  parser.add_argument("-f", "--cfg", default=appCfgPath, help="configuration file path")
  parser.add_argument("-l",
                      "--list",
                      action="store_true",
                      default=False,
                      help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
