#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.cli
import pyauto_base.fs
import pyauto_hw.config

_CADENCE_BIN = "C:\\Cadence\\SPB_17.4\\tools\\bin"

def exportPlacement(brdCfg):
  path_brd = brdCfg.getPath_brd()
  dir_out = brdCfg.path["fabout"]
  pyauto_base.fs.mkOutFolder(dir_out)

  path_out = os.path.join(dir_out, "ven_cntr.plc")
  args = ["-c", path_brd, "out.brd", path_out]
  #print("DBG", args)
  pyauto_base.cli.popenGenericCmd(os.path.join(_CADENCE_BIN, "plctxt.exe"), args)
  logger.info("{} written".format(path_out))

  path_out = os.path.join(dir_out, "ven_pin1.plc")
  args = ["-p", path_brd, "out.brd", path_out]
  #print("DBG", args)
  pyauto_base.cli.popenGenericCmd(os.path.join(_CADENCE_BIN, "plctxt.exe"), args)
  logger.info("{} written".format(path_out))

  for f in os.listdir("."):
    if (f.startswith("plctxt")):
      os.remove(f)

def mainApp():
  brdCfg = pyauto_hw.config.BoardConfig(cliArgs["brd"])
  if (cliArgs["list"]):
    brdCfg.showInfo()
    return

  t0 = datetime.datetime.now()

  exportPlacement(brdCfg)

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = ""
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  parser.add_argument("brd", help="board config path")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  parser.add_argument("-l",
                      "--list",
                      action="store_true",
                      default=False,
                      help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
