#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.cli
import pyauto_base.fs
import pyauto_hw.config

_CADENCE_BIN = "C:\\Cadence\\SPB_17.4\\tools\\bin"

def exportPDF(brdCfg):
  path_brd = brdCfg.getPath_brd()
  dir_out = brdCfg.path["fabout"]
  pyauto_base.fs.mkOutFolder(dir_out)

  lst_artwork = pyauto_base.cli.popenGenericCmd(os.path.join(_CADENCE_BIN, "artwork.exe"),
                                                ["-l", path_brd])

  lst_fab = []
  for v in lst_artwork:
    if (v.startswith("299-")):
      lst_fab.append(v)
  lst_fab = sorted(lst_fab)
  lst_assy = []
  for v in lst_artwork:
    if (v.startswith("399-")):
      lst_assy.append(v)
  lst_assy = sorted(lst_assy)

  args = [path_brd, "-B"]
  for v in lst_fab:
    args += ["-f", v]
  path_out = os.path.join(dir_out, "{}_fabdwg.pdf".format(lst_fab[0].split("_")[0]))
  args += ["-o", path_out]
  #print("DBG", args)
  pyauto_base.cli.popenGenericCmd(os.path.join(_CADENCE_BIN, "pdf_out.exe"), args)
  logger.info("{} written".format(path_out))

  args = [path_brd, "-B"]
  for v in lst_assy:
    args += ["-f", v]
  path_out = os.path.join(dir_out, "{}_asmdwg.pdf".format(lst_assy[0].split("_")[0]))
  args += ["-o", path_out]
  #print("DBG", args)
  pyauto_base.cli.popenGenericCmd(os.path.join(_CADENCE_BIN, "pdf_out.exe"), args)
  logger.info("{} written".format(path_out))

  for f in os.listdir("."):
    if (f.startswith("pdf_out")):
      os.remove(f)

def mainApp():
  brdCfg = pyauto_hw.config.BoardConfig(cliArgs["brd"])
  if (cliArgs["list"]):
    brdCfg.showInfo()
    return

  t0 = datetime.datetime.now()

  exportPDF(brdCfg)

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = ""
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  parser.add_argument("brd", help="board config path")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  parser.add_argument("-l",
                      "--list",
                      action="store_true",
                      default=False,
                      help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
