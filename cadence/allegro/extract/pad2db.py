#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse
import json

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.cli
import pyauto_base.config
import pyauto_base.db
import pyauto_base.fs
import pyauto_base.misc

_CADENCE_BIN = "C:\\Cadence\\SPB_17.4\\tools\\bin"
_CADENCE_VIEWS = "C:\\Cadence\\SPB_17.4\\share\\pcb\\text\\views"
_unit = None

class PCBPadStack(object):

  def __init__(self, strName):
    self.name = strName
    self.unit = ""
    self.top_cu = []
    self.int_cu = []
    self.bot_cu = []
    self.top_sm = []
    self.bot_sm = []
    self.top_pm = []
    self.bot_pm = []
    self.drl = []

dbCfg = pyauto_base.config.DBConfig()
dbCtl = pyauto_base.db.DBController()
_dbQry = None
_hdrRow = [
    "LAYER", "VIAFLAG", "PADSHAPE1", "PADWIDTH", "PADHGHT", "PADXOFF", "PADYOFF",
    "PADFLASH", "PADSHAPENAME"
]

class _ExtractColumns(object):

  def __init__(self):
    self.dictIdx = {}
    self.idx_ly = -1
    self.idx_shp = -1
    self.idx_w = -1
    self.idx_h = -1
    self.idx_xo = -1
    self.idx_yo = -1
    self.idx_flash = -1

_cols = _ExtractColumns()

def _run_extracta(path):
  global _dbQry

  logger.info(path)
  res = pyauto_base.cli.popenGenericCmd(
      os.path.join(_CADENCE_BIN, "extracta.exe"),
      [path, os.path.join(_CADENCE_VIEWS, "pad_rep.txt")])

  pad = PCBPadStack(os.path.basename(path))

  if (len(_cols.dictIdx) == 0):
    for ln in res:
      if (ln.startswith("A!")):
        _cols.dictIdx = pyauto_base.misc.getListIndexByName(ln.split("!"), _hdrRow)
        _cols.idx_ly = _cols.dictIdx["LAYER"]
        _cols.idx_shp = _cols.dictIdx["PADSHAPE1"]
        _cols.idx_w = _cols.dictIdx["PADWIDTH"]
        _cols.idx_h = _cols.dictIdx["PADHGHT"]
        _cols.idx_xo = _cols.dictIdx["PADXOFF"]
        _cols.idx_yo = _cols.dictIdx["PADYOFF"]
        _cols.idx_flash = _cols.dictIdx["PADFLASH"]
        break

  for ln in res:
    if (ln.startswith("J!")):
      pad.unit = ln.split("!")[8]

    if (ln.startswith("S!")):
      tmp = ln.split("!")
      if (tmp[_cols.idx_ly] == "TOP"):
        if (tmp[_cols.idx_flash] != ""):
          logger.warning("Flash detected on {}.top_cu: {} / {}".format(
              pad.name, tmp[_cols.idx_flash], tmp[_cols.idx_shp]))
        pad.top_cu = [
            tmp[_cols.idx_shp], tmp[_cols.idx_w], tmp[_cols.idx_h], tmp[_cols.idx_xo],
            tmp[_cols.idx_yo]
        ]
      elif (tmp[_cols.idx_ly] == "internal_pad_def"):
        if (tmp[_cols.idx_flash] != ""):
          logger.warning("Flash detected on {}.int_cu: {} / {}".format(
              pad.name, tmp[_cols.idx_flash], tmp[_cols.idx_shp]))
        pad.int_cu = [
            tmp[_cols.idx_shp], tmp[_cols.idx_w], tmp[_cols.idx_h], tmp[_cols.idx_xo],
            tmp[_cols.idx_yo]
        ]
      elif (tmp[_cols.idx_ly] == "BOTTOM"):
        if (tmp[_cols.idx_flash] != ""):
          logger.warning("Flash detected on {}.bot_cu: {} / {}".format(
              pad.name, tmp[_cols.idx_flash], tmp[_cols.idx_shp]))
        pad.bot_cu = [
            tmp[_cols.idx_shp], tmp[_cols.idx_w], tmp[_cols.idx_h], tmp[_cols.idx_xo],
            tmp[_cols.idx_yo]
        ]
      elif (tmp[_cols.idx_ly] == "~TSM"):
        if (tmp[_cols.idx_flash] != ""):
          logger.warning("Flash detected on {}.top_sm: {} / {}".format(
              pad.name, tmp[_cols.idx_flash], tmp[_cols.idx_shp]))
        pad.top_sm = [
            tmp[_cols.idx_shp], tmp[_cols.idx_w], tmp[_cols.idx_h], tmp[_cols.idx_xo],
            tmp[_cols.idx_yo]
        ]
      elif (tmp[_cols.idx_ly] == "~BSM"):
        if (tmp[_cols.idx_flash] != ""):
          logger.warning("Flash detected on {}.bot_sm: {} / {}".format(
              pad.name, tmp[_cols.idx_flash], tmp[_cols.idx_shp]))
        pad.bot_sm = [
            tmp[_cols.idx_shp], tmp[_cols.idx_w], tmp[_cols.idx_h], tmp[_cols.idx_xo],
            tmp[_cols.idx_yo]
        ]
      elif (tmp[_cols.idx_ly] == "~TPM"):
        if (tmp[_cols.idx_flash] != ""):
          logger.warning("Flash detected on {}.top_pm: {} / {}".format(
              pad.name, tmp[_cols.idx_flash], tmp[_cols.idx_shp]))
        pad.top_pm = [
            tmp[_cols.idx_shp], tmp[_cols.idx_w], tmp[_cols.idx_h], tmp[_cols.idx_xo],
            tmp[_cols.idx_yo]
        ]
      elif (tmp[_cols.idx_ly] == "~BPM"):
        if (tmp[_cols.idx_flash] != ""):
          logger.warning("Flash detected on {}.bot_pm: {} / {}".format(
              pad.name, tmp[_cols.idx_flash], tmp[_cols.idx_shp]))
        pad.bot_pm = [
            tmp[_cols.idx_shp], tmp[_cols.idx_w], tmp[_cols.idx_h], tmp[_cols.idx_xo],
            tmp[_cols.idx_yo]
        ]
      elif (tmp[_cols.idx_ly] == "DRILL"):
        if (tmp[_cols.idx_flash] != ""):
          raise NotImplementedError
        pad.drl = [
            tmp[_cols.idx_shp], tmp[_cols.idx_w], tmp[_cols.idx_h], tmp[_cols.idx_xo],
            tmp[_cols.idx_yo]
        ]

  for f in os.listdir("."):
    if (f.startswith("extract.log")):
      os.remove(f)

  _dbQry.insert("Padstacks", [
      "name", "unit", "top_cu", "int_cu", "bot_cu", "top_sm", "bot_sm", "top_pm", "bot_pm"
  ])
  dbCtl.query(_dbQry.sql, [
      pad.name, pad.unit,
      json.dumps(pad.top_cu),
      json.dumps(pad.int_cu),
      json.dumps(pad.bot_cu),
      json.dumps(pad.top_sm),
      json.dumps(pad.bot_sm),
      json.dumps(pad.top_pm),
      json.dumps(pad.bot_pm)
  ])

def mainApp():
  global _dbQry

  logger.info("using config file: {}".format(os.path.realpath(cliArgs["cfg"])))
  #dbCfg.loadCfgFile(cliArgs["cfg"])
  dbCfg.loadXmlFile(cliArgs["cfg"])
  if (cliArgs["list"]):
    dbCfg.showInfo()
    return

  t0 = datetime.datetime.now()

  dbCtl.dbInfo = dbCfg.dictDBInfo["1"]
  dbCtl.connect()
  dbCtl.doQuery = True
  _dbQry = pyauto_base.db.DBQuery(dbCtl.backend)
  _dbQry.delete("Padstacks")
  dbCtl.query(_dbQry.sql, [])

  for f in os.listdir(dbCfg.path["pad_main"]):
    if (not f.endswith(".pad")):
      continue
    if (f.endswith("(2).pad")):
      logger.warning(f)
      continue
    _run_extracta(os.path.join(dbCfg.path["pad_main"], f))

  dbCtl.disconnect()
  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  appDir = os.getcwd()  # current folder
  appCfgPath = os.path.join(appDir, (modName + ".xml"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "read Allegro pads and put the information in a database"
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  parser.add_argument("-f", "--cfg", default=appCfgPath, help="configuration file path")
  parser.add_argument("-l",
                      "--list",
                      action="store_true",
                      default=False,
                      help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
