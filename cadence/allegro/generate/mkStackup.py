#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_hw.config
import pyauto_hw.CAD.constants as CADct

def mainApp():
  brdCfg = pyauto_hw.config.BoardConfig(cliArgs["brd"])
  if (cliArgs["list"]):
    brdCfg.showInfo()
    return

  t0 = datetime.datetime.now()

  if ((brdCfg.pcb is None) or (brdCfg.pcb.stackup is None)):
    msg = "NO stackup found for {}".format(brdCfg.name)
    logger.error(msg)
    raise RuntimeError(msg)

  if (brdCfg.pcb.isConsistent() is not True):
    msg = "INCORRECT stackup/artwork"
    logger.error(msg)
    raise RuntimeError(msg)

  logger.info(brdCfg.pcb.stackup)

  out_path = brdCfg.path["pcb"]
  f_out_path = os.path.join(out_path, "stackup.il")
  with open(f_out_path, "w") as f_out:
    # Disable Dynamic fill update to smooth
    f_out.write("'axlDBControl('dynamicFillMode, nil)\n")
    f_out.write("\n")

    for i, ly in enumerate(brdCfg.pcb.stackup.layers):
      ly_id = (i + 2)  # in Cadence layer 0 is surface, layer 1 is soldermask
      # get dbid from layer
      f_out.write("xs = axlXSectionGet(nil {})\n".format(ly_id))

      # modify layer parameters
      f_out.write("lxc = axlXSectionModify(\n")
      f_out.write('?thickness {}\n'.format(ly.thickness.magnitude))
      #f_out.write('?dielectricConst "{}"\n'.format(ly.dk))
      #f_out.write('?lossTangent "{}"\n'.format(ly.df))
      f_out.write('?negativeArtwork nil\n')
      f_out.write('?layerId \"\"\n')
      #f_out.write('?unusedPin t\n')
      f_out.write('?unusedVia t\n')
      f_out.write(')\n'.format(i))

      # apply modification to selected dbid layer
      f_out.write('ret = axlXSectionSet(xs lxc)\n')
      f_out.write("axlMsgPut(\"update {} {} {}\")\n".format(ly_id, ly.type, ly.name))
      f_out.write("\n")

    # Re-enable Dynamic fill update to smooth
    f_out.write("'axlDBControl('dynamicFillMode, 'wysiwyg)\n")
    f_out.write("\n")
    f_out.write("th = axlXSectionGet(nil 'thickness)\n")
    f_out.write("axlMsgPut(\"stack-up thickness %f\", th)\n")

  logger.info("{} written".format(f_out_path))
  logger.info("use \"skill load(\"{}\")\" from Allegro command".format(
      os.path.basename(f_out_path)))

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "create Skill script to update stack-up"
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  parser.add_argument("brd", help="board config path")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  parser.add_argument("-l",
                      "--list",
                      action="store_true",
                      default=False,
                      help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
