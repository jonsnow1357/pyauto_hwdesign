#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

import math
#import csv

logger = logging.getLogger("lib")

import pyauto_base.fs
import pyauto_hw.geometry as geo

class PCBItem(object):

  def __init__(self):
    self._id = None
    self._type = None
    self._class = None
    self._subclass = None
    self.length = None
    self.area = None
    self.center = None
    self.radius = None
    self.netName = None
    self.refdes = None
    self.boundary = []

  def __str__(self):
    res = "{} id={}".format(self._type, self._id)
    if (self._type == "PIN"):
      res += " ({} {})".format(self.center.x, self.center.y)
    elif (self._type == "VIA"):
      res += " ({} {})".format(self.center.x, self.center.y)
    elif (self._type == "SHAPE"):
      res += " area={}".format(self.area)
    elif ((self._type == "LINE") or (self._type == "ODD-ANGLE LINE SEGMENT")
          or (self._type == "VERTICAL LINE SEGMENT")
          or (self._type == "HORIZONTAL LINE SEGMENT") or (self._type == "ARC SEGMENT")):
      res += " len={}".format(self.length)
    elif (self._type == "NET"):
      res += " name={}".format(self.netName)
    elif (self._type == "COMPONENT INSTANCE"):
      res += " refdes={}".format(self.refdes)
    elif (self._type == "SYMBOL"):
      res += " refdes={}".format(self.refdes)
    elif (self._type == "DRC ERROR"):
      res += " ({} {})".format(self.center.x, self.center.y)
    return res

  @property
  def id(self):
    return self._id

  @property
  def type(self):
    return self._type

  @property
  def cls(self):
    return self._class

  @property
  def subcls(self):
    return self._subclass

  def _parse_PIN(self, lstLn):
    for ln in lstLn:
      ln = ln.strip()
      if (ln.startswith("pin number:")):
        tmp = ln.split(" ")
        self.refdes = tmp[-1].split(".")[0]
      if (ln.startswith("location-xy")):
        tmp = re.split(r"location-xy: +\(([0-9-.]+) ([0-9-.]+)\).*", ln)
        self.center = geo.Point2D(tmp[1], tmp[2])
    #print("DBG", self)
    #raise RuntimeError

  def _parse_VIA(self, lstLn):
    for ln in lstLn:
      ln = ln.strip()
      if (ln.startswith("origin-xy")):
        tmp = re.split(r"origin-xy: +\(([0-9-.]+) ([0-9-.]+)\).*", ln)
        self.center = geo.Point2D(tmp[1], tmp[2])
    #print("DBG", self)
    #raise RuntimeError

  def _parse_SHAPE(self, lstLn):
    bBound = False
    for ln in lstLn[1:]:
      ln = ln.strip()
      if (ln.startswith("class")):
        self._class = ln.split()[-1]
      if (ln.startswith("subclass")):
        self._subclass = ln.split()[-1]
      if (ln.startswith("Area")):
        self.area = float(ln.split()[1])
      if (ln.startswith("Exterior boundary")):
        bBound = True
      if (bBound):
        self.boundary.append(ln)
    #print("DBG", self)
    #raise RuntimeError

  def _parse_LINE_ARC(self, lstLn):
    for ln in lstLn:
      ln = ln.strip()
      if (ln.startswith("class")):
        self._class = ln.split()[-1]
      if (ln.startswith("subclass")):
        self._subclass = ln.split()[-1]
      if (ln.startswith("Length")):
        self.length = float(ln.split()[-1])
      if (ln.startswith("Segment length")):
        self.length = float(ln.split()[-1])
      if (ln.startswith("center-xy")):
        tmp = re.split(r"center-xy: +\(([0-9-.]+) ([0-9-.]+)\) radius +\(([0-9-.]+)\) .*",
                       ln)
        self.center = geo.Point2D(tmp[1], tmp[2])
        self.radius = float(tmp[3])
    #print("DBG", self)
    #raise RuntimeError

  def _parse_NET(self, lstLn):
    for ln in lstLn:
      ln = ln.strip()
      if (ln.startswith("Net Name")):
        self.netName = ln.split()[-1]
      if (ln.startswith("Total etch length")):
        self.length = float(ln.split()[-1])
    #print("DBG", self)
    #raise RuntimeError

  def _parse_COMP(self, lstLn):
    for ln in lstLn:
      ln = ln.strip()
      if (ln.startswith("Reference Designator")):
        self.refdes = ln.split()[-1]
      if (ln.startswith("origin-xy")):
        tmp = re.split(r"origin-xy: +\(([0-9-.]+) ([0-9-.]+)\).*", ln)
        self.center = geo.Point2D(tmp[1], tmp[2])
    #print("DBG", self)
    #raise RuntimeError

  def _parse_SYMBOL(self, lstLn):
    for ln in lstLn:
      ln = ln.strip()
      if (ln.startswith("RefDes")):
        self.refdes = ln.split()[-1]
      if (ln.startswith("origin-xy")):
        tmp = re.split(r"origin-xy: +\(([0-9-.]+) ([0-9-.]+)\).*", ln)
        self.center = geo.Point2D(tmp[1], tmp[2])
    #print("DBG", self)
    #raise RuntimeError

  def _parse_DRC(self, lstLn):
    for ln in lstLn:
      ln = ln.strip()
      if (ln.startswith("class")):
        self._class = ln.split()[-1]
      if (ln.startswith("subclass")):
        self._subclass = ln.split()[-1]
      if (ln.startswith("Origin xy")):
        tmp = re.split(r"Origin xy: +\(([0-9-.]+) ([0-9-.]+)\).*", ln)
        self.center = geo.Point2D(tmp[1], tmp[2])
    #print("DBG", self)
    #raise RuntimeError

  def parse(self, lstLn):
    #print("DBG", lstLn)
    if (lstLn[0].startswith("Item")):
      tmp = re.split(r"Item ([0-9]+) +< ([A-Z\- ]+).* >", lstLn[0])
      print("DBG", lstLn[0], tmp)
      self._id = int(tmp[1])
      self._type = tmp[2].strip()
    else:
      raise RuntimeError

    if (self._id is None):
      msg = "CANNOT parse id\n{}".format(lstLn)
      logger.error(msg)
      raise RuntimeError(msg)
    if (self._type is None):
      msg = "CANNOT parse type\n{}".format(lstLn)
      logger.error(msg)
      raise RuntimeError(msg)
    #print("DBG", self._id, self._type)

    if (self._type == "PIN"):
      self._parse_PIN(lstLn[1:])
    elif (self._type == "VIA"):
      self._parse_VIA(lstLn[1:])
    elif (self._type == "SHAPE"):
      self._parse_SHAPE(lstLn[1:])
    elif ((self._type == "LINE") or (self._type == "ODD-ANGLE LINE SEGMENT")
          or (self._type == "VERTICAL LINE SEGMENT")
          or (self._type == "HORIZONTAL LINE SEGMENT") or (self._type == "ARC SEGMENT")):
      self._parse_LINE_ARC(lstLn[1:])
    elif (self._type == "NET"):
      self._parse_NET(lstLn[1:])
    elif (self._type == "COMPONENT INSTANCE"):
      self._parse_COMP(lstLn[1:])
    elif (self._type == "SYMBOL"):
      self._parse_SYMBOL(lstLn[1:])
    elif (self._type == "DRC ERROR"):
      self._parse_DRC(lstLn[1:])
    else:
      raise NotImplementedError("{} {}".format(self._id, self._type))

class PCBItemFilter(object):
  """
  :param regexType: regex to match the type of Item() searched
  :param dim: list of dimensions used for filtering
  :param callback: function to process the Item()
  :param lstRes: list of PCBItem
  """
  regexType = None
  regexRefdes = None
  dim = []
  callback = None

  def filterNone(self, it: PCBItem):
    """
    :return: True|False
    """
    return True

  def filterType(self, it: PCBItem):
    """
    filters based on PCBItem type
    :return: True|False
    """
    if (re.match(self.regexType, it.type) is None):
      return False

    return True

  def filterTypeRefdes(self, it: PCBItem):
    """
    filters based on PCBItem type and refdes (if exists)
    :param it:
    :return: True|False
    """
    if (re.match(self.regexType, it.type) is None):
      return False
    if (self.regexRefdes is None):
      return False
    if (re.match(self.regexRefdes, it.refdes) is None):
      return False

    return True

  def filterTypeRadius(self, it: PCBItem):
    """
    filters based on PCBItem type and radius (if exists)
    :param it:
    :return: True|False
    """
    if (re.match(self.regexType, it.type) is None):
      return False
    if (it.radius is None):
      return False
    if (len(self.dim) == 0):
      return False

    if (self.dim[0] <= 0.0):
      return False
    if (math.fabs(it.radius - self.dim[0]) > (0.01 * self.dim[0])):
      return False

    return True

def readAllegroInfo(fPath: str, flt: PCBItemFilter):
  """
  reads an Allegro "info" file, breaks it into items and filters them based on ItemFilter criteria

  :param fPath:
  :param flt: PCBItemFilter
  :return: list of PCBItem
  """
  pyauto_base.fs.chkPath_File(fPath)

  lstItems = []
  bItem = False
  lst_ln = []
  logger.info("reading {}".format(fPath))
  with open(fPath, "r") as fIn:
    for ln in [t.strip("\n\r") for t in fIn.readlines()]:
      if (len(ln) == 0):
        continue
      if (ln.startswith("Item")):
        bItem = True
        lst_ln = [ln]
        continue
      if ("~" in ln):
        bItem = False
        it = PCBItem()
        it.parse(lst_ln)
        if (flt.callback(it)):
          lstItems.append(it)
        continue

      if (bItem):
        lst_ln.append(ln)

  logger.info("read {:d} items(s)".format(len(lstItems)))
  return lstItems

def writeAllegroScript(fPath, lstLn):
  if (len(lstLn) == 0):
    logger.warning("NOTHING to write")
    return

  fPath = pyauto_base.fs.mkAbsolutePath(fPath, bExists=False)
  with open(fPath, "w") as fOut:
    fOut.write("# Allegro script\n")
    fOut.write("\n")
    fOut.write("setwindow pcb\n")
    #fOut.write("generaledit\n")
    for ln in lstLn:
      fOut.write(ln + "\n")
    fOut.write("done\n")

  logger.info("{} written".format(fPath))
  logger.info("use \"replay {}\" from Allegro command".format(os.path.basename(fPath)))
