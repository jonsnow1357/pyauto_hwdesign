#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_hw.config
import pyauto_hw.CAD.constants as CADct

def mainApp():
  brdCfg = pyauto_hw.config.BoardConfig(cliArgs["brd"])
  if (cliArgs["list"]):
    brdCfg.showInfo()
    return

  t0 = datetime.datetime.now()

  if ((brdCfg.pcb is None) or (brdCfg.pcb.artwork is None)):
    msg = "NO artwork found for {}".format(brdCfg.name)
    logger.error(msg)
    raise RuntimeError(msg)

  if (brdCfg.pcb.isConsistent() is not True):
    msg = "INCORRECT stackup/artwork"
    logger.error(msg)
    raise RuntimeError(msg)

  if (pyauto_base.fs.chkPath_Dir(brdCfg.path["pcb"], bDie=False)):
    out_dir = brdCfg.path["pcb"]
  else:
    out_dir = pyauto_base.fs.mkOutFolder()
  for f in os.listdir(out_dir):
    if (f.endswith("layers.txt")):
      pyauto_base.fs.rm(os.path.join(out_dir, f))

  out_path = os.path.join(out_dir, "{}.layers.txt".format(brdCfg.name))
  brdCfg.pcb.artwork.writeAllegroScript(out_path)

  if (cliArgs["details"]):
    set_doc_cls = set()
    for ly in brdCfg.pcb.artwork.layers:
      if (ly.type == CADct.pcbArtDOC):
        for v in ly.content:
          set_doc_cls.add(v)
    for v in sorted(set_doc_cls):
      logger.info(v)

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "creates Allegro artwork definition"
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  parser.add_argument("brd", help="board config path")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  parser.add_argument("-l",
                      "--list",
                      action="store_true",
                      default=False,
                      help="list config file options")
  parser.add_argument("-d",
                      "--details",
                      action="store_true",
                      default=False,
                      help="show list of used sub-classes")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
