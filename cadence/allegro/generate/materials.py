#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse
import json

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_hw.config
import pyauto_hw.CAD.constants as CADct

def mainApp():
  pyauto_base.fs.chkPath_File(cliArgs["cfg"])
  pyauto_base.fs.chkPath_Dir(cliArgs["mat"])

  t0 = datetime.datetime.now()

  lst_materials = json.load(open(cliArgs["cfg"], "r"))
  logger.info("found {} materials".format(len(lst_materials)))

  tpl_path = os.path.join("templates", "materials.dat.tpl")
  out_path = os.path.join(cliArgs["mat"], "materials.dat")
  dict_tpl = {"lstData": lst_materials}
  pyauto_base.misc.templateWheezy(tpl_path, out_path, dict_tpl)

  tpl_path = os.path.join("templates", "materials.html.tpl")
  out_path = os.path.join(cliArgs["mat"], "materials.html")
  dict_tpl = {
      "dictInfo": {
          "timestamp": pyauto_base.misc.getTimestamp()
      },
      "lstData": lst_materials
  }
  pyauto_base.misc.templateWheezy(tpl_path, out_path, dict_tpl)

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "creates Allegro materials.dat"
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  parser.add_argument("cfg", help="config path")
  parser.add_argument("mat", help="material path")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
