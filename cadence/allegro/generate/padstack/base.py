#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")

dictConvLayers = {
    "TOP": "begin_layer",
    "BOT": "end_layer",
    "INT": "default_internal",
    "SMT": "soldermask_top",
    "SMB": "soldermask_bottom",
    "PMT": "pastemask_top",
    "PMB": "pastemask_bottom"
}
dictConvUnits = {"mm": "Millimeter", "mils": "Mils"}
dictConvShapes = {
    None: "Null",
    "sq": "Square",
    "cir": "Circle",
    "rec": "Rectangle",
    "obl": "Oblong"
}
dictConvDrills = {"cir": "Circle Drill", "obl": "Oval Slot"}

pad_layers = ("begin_layer", "default_internal", "end_layer", "soldermask_top",
              "soldermask_bottom", "pastemask_top", "pastemask_bottom")
pad_units = ("Mils", "Inch", "Millimeter", "Centimeter", "Micron")
pad_shapes = ("Null", "Circle", "Square", "Oblong", "Rectangle", "Octagon", "Shape")
pad_drill_shapes = ("Circle Drill", "Oval Slot", "Rectangle Slot")
pad_drill_figures = ("Null", "Circle", "Square", "Hexagon X", "Hexagon Y", "Octagon",
                     "Cross", "Diamond", "Triangle", "Oblong X", "Oblong Y", "Rectangle")

def checkDimension(val, valId=None):
  if (val < 0.0):
    if (valId is None):
      valId = "dimension"
    msg = "{} must be GREATER THAN 0: {}".format(valId, val)
    logger.error(msg)
    raise RuntimeError(msg)

class PadStack(object):

  def __init__(self):
    self.unit = "mm"
    self.decimals = 2

    self.drl = 3 * [None]
    self.drl_plate = True
    self.drl_tol = [0.0, 0.0]
    self.drl_fig = ["Null", "-", 0.0, 0.0]
    self.drl_via = False

    self.ly = {
        "TOP": {
            "pad": 3 * [None],
            "th": 3 * [None],
            "ap": 3 * [None]
        },
        "INT": {
            "pad": 3 * [None],
            "th": 3 * [None],
            "ap": 3 * [None]
        },
        "BOT": {
            "pad": 3 * [None],
            "th": 3 * [None],
            "ap": 3 * [None]
        },
        "SMT": {
            "pad": 3 * [None],
            "th": 3 * [None],
            "ap": 3 * [None]
        },
        "SMB": {
            "pad": 3 * [None],
            "th": 3 * [None],
            "ap": 3 * [None]
        },
        "PMT": {
            "pad": 3 * [None],
            "th": 3 * [None],
            "ap": 3 * [None]
        },
        "PMB": {
            "pad": 3 * [None],
            "th": 3 * [None],
            "ap": 3 * [None]
        },
    }

    self.IPCname = None  # https://blogs.mentor.com/tom-hausherr/blog/tag/padstack/
    self.valid = False

  def getLayerVal(self, lyId, valId):
    return self.ly[lyId][valId]

  def __str__(self):
    res = [
        "{}: {} [{}.{:d}]".format(self.__class__.__name__, self.IPCname, self.unit,
                                  self.decimals)
    ]
    for lyId in ("TOP", "INT", "BOT", "SMT", "SMB", "PMT", "PMB"):
      pad = self.getLayerVal(lyId, "pad")
      th = self.getLayerVal(lyId, "th")
      ap = self.getLayerVal(lyId, "ap")
      if ((pad[0] is None) and (th[0] is None) and (ap[0] is None)):
        continue

      ln = [
          lyId, "none" if (pad[0] is None) else "{}_{}x{}".format(pad[0], pad[1], pad[2]),
          "none" if (th[0] is None) else "{}_{}x{}".format(th[0], th[1], th[2]), "none" if
          (ap[0] is None) else "{}_{}x{}".format(ap[0], ap[1], ap[2])
      ]
      res.append(" ".join(ln))

    if (self.drl[0] is not None):
      res.append("{} {}_{}x{}_+{}-{} ({: <2}) {}".format(
          "via" if (self.drl_via) else "drl", self.drl[0], self.drl[1], self.drl[2],
          self.drl_tol[0], self.drl_tol[1], "P" if (self.drl_plate) else "NP",
          ",".join([str(t) for t in self.drl_fig])))
    return "\n".join(res)

  def setUnit(self, unit, decimals=None):
    if (unit not in dictConvUnits.keys()):
      msg = "UNKNOWN unit: {}".format(unit)
      logger.error(msg)
      raise RuntimeError(msg)
    if (decimals is None):
      if (unit == "Mils"):
        decimals = 2
      else:
        decimals = 4
    if ((decimals < 0) or (decimals > 8)):
      msg = "UNKNOWN decimals value: {}".format(decimals)
      logger.error(msg)
      raise RuntimeError(msg)

    self.unit = unit
    self.decimals = decimals

  def _setLayerVal(self, lyId, valId, shape, dim1, dim2):
    #logger.info([lyId, valId, shape, dim1, dim2])
    if (shape is None):
      self.ly[lyId][valId] = [None, None, None]
    elif ((shape is not None) and (dim1 is None) and (dim2 is None)):
      self.ly[lyId][valId] = [None, None, None]
      logger.warning("changed {} {} shape to none".format(lyId, valId))
    elif ((shape is not None) and (dim1 == 0.0) and (dim2 is None)):
      self.ly[lyId][valId] = [None, None, None]
      logger.warning("changed {} {} shape to none".format(lyId, valId))
    elif (shape in ("sq", "cir")):
      checkDimension(dim1, "{} {} dimension 1".format(lyId, valId))
      self.ly[lyId][valId] = [shape, round(dim1, self.decimals), round(dim1, self.decimals)]
    elif (shape == "rec"):
      checkDimension(dim1, "{} {} dimension 1".format(lyId, valId))
      if (dim1 == dim2):
        self.ly[lyId][valId] = [
            "sq", round(dim1, self.decimals),
            round(dim1, self.decimals)
        ]
        logger.warning("changed {} {} shape to sq".format(lyId, valId))
      else:
        checkDimension(dim2, "{} {} dimension 2".format(lyId, valId))
        self.ly[lyId][valId] = [
            shape, round(dim1, self.decimals),
            round(dim2, self.decimals)
        ]
    elif (shape == "obl"):
      checkDimension(dim1, " {} {} dimension 1".format(lyId, valId))
      if (dim1 == dim2):
        self.ly[lyId][valId] = [
            "cir", round(dim1, self.decimals),
            round(dim1, self.decimals)
        ]
        logger.warning("changed {} {} shape to cir".format(lyId, valId))
      else:
        checkDimension(dim2, "{} {} dimension 2".format(lyId, valId))
        self.ly[lyId][valId] = [
            shape, round(dim1, self.decimals),
            round(dim2, self.decimals)
        ]
    else:
      msg = "INCORRECT {} {} values: {}".format(lyId, valId, [shape, dim1, dim2])
      logger.error(msg)
      raise RuntimeError(msg)

  def setLayer(self,
               lyId,
               pad_sh,
               pad1,
               pad2=None,
               th_sh=None,
               th1=None,
               th2=None,
               ap_sh=None,
               ap1=None,
               ap2=None):
    if (lyId not in dictConvLayers.keys()):
      msg = "UNKNOWN layer: {}".format(lyId)
      logger.error(msg)
      raise RuntimeError(msg)
    if (pad_sh not in dictConvShapes.keys()):
      msg = "UNKNOWN {} pad shape: {}".format(lyId, pad_sh)
      logger.error(msg)
      raise RuntimeError(msg)
    if (th_sh not in dictConvShapes.keys()):
      msg = "UNKNOWN {} th shape: {}".format(lyId, th_sh)
      logger.error(msg)
      raise RuntimeError(msg)
    if (ap_sh not in dictConvShapes.keys()):
      msg = "UNKNOWN {} ap shape: {}".format(lyId, ap_sh)
      logger.error(msg)
      raise RuntimeError(msg)

    shape = [t for t in [pad_sh, th_sh, ap_sh] if (t is not None)]
    if (len(set(shape)) != 1):
      msg = "INCONSISTENT {} shapes: {}".format(lyId, [pad_sh, th_sh, ap_sh])
      logger.error(msg)
      raise RuntimeError(msg)

    self._setLayerVal(lyId, "pad", pad_sh, pad1, pad2)
    self._setLayerVal(lyId, "th", th_sh, th1, th2)
    self._setLayerVal(lyId, "ap", ap_sh, ap1, ap2)

  def setDrillFigure(self, drl_fig=None, drl_ch=None, drl_chw=None, drl_chh=None):
    if (drl_fig is None):
      drl_fig = "Null"
    if (drl_fig not in pad_drill_figures):
      msg = "UNKNOWN drill figure: {}".format(drl_fig)
      logger.error(msg)
      raise RuntimeError(msg)
    if (drl_ch is None):
      drl_ch = "-"
    if (len(drl_ch) != 1):
      msg = "INCORRECT drill char: {}".format(drl_ch)
      logger.error(msg)
      raise RuntimeError(msg)
    if (drl_chw is None):
      drl_chw = 0.0
    if (drl_chh is None):
      drl_chh = 0.0
    checkDimension(drl_chw, "drill char width")
    checkDimension(drl_chh, "drill char width")

    self.drl_fig = [drl_fig, drl_ch, drl_chw, drl_chh]

  def setDrill(self, shape, bPlated, dh1, dh2=None, dtp=0.0, dtn=0.0, bVia=False):
    if (shape not in dictConvDrills.keys()):
      msg = "UNKNOWN drill shape: {}".format(shape)
      logger.error(msg)
      raise RuntimeError(msg)
    if (bVia and (shape != "cir")):
      msg = "UNSUPPORTED drill shape for via: {}".format(shape)
      logger.error(msg)
      raise RuntimeError(msg)

    if (shape is None):
      self.drl = [None, None, None]
    elif ((shape is not None) and (dh1 is None) and (dh2 is None)):
      self.drl = [None, None, None]
      logger.warning("changed drl shape to none")
    elif (shape == "cir"):
      checkDimension(dh1, "drl dimension 1")
      self.drl = [shape, round(dh1, self.decimals), round(dh1, self.decimals)]
    elif (shape == "obl"):
      checkDimension(dh1, "drl dimension 1")
      if (dh1 == dh2):
        self.drl = ["cir", round(dh1, self.decimals), round(dh1, self.decimals)]
        logger.warning("changed drl shape to cir")
      else:
        checkDimension(dh2, "drl dimension 2")
        self.drl = [shape, round(dh1, self.decimals), round(dh2, self.decimals)]
    else:
      msg = "INCORRECT drl values: {}".format([shape, dh1, dh2])
      logger.error(msg)
      raise RuntimeError(msg)

    self.drl_plate = bPlated
    self.drl_via = bVia
    if (self.drl_via):
      self.drl_tol = [0.0, (-1.0 * max(self.drl[1], self.drl[2]))]
    else:
      self.drl_tol = [dtp, dtn]

  def isValid(self):
    shape = []
    for lyId in self.ly.keys():
      if (self.ly[lyId]["pad"][0] is not None):
        shape.append(self.ly[lyId]["pad"][0])

    if (len(set(shape)) != 1):
      msg = "INCORRECT padstack shapes"
      logger.error(msg)
      raise RuntimeError(msg)

    pad1 = []
    pad2 = []
    th1 = []
    th2 = []
    ap1 = []
    ap2 = []
    for lyId in ("TOP", "INT", "BOT"):
      if (self.ly[lyId]["pad"][1] is not None):
        pad1.append(self.ly[lyId]["pad"][1])
      if (self.ly[lyId]["pad"][2] is not None):
        pad2.append(self.ly[lyId]["pad"][2])
      if (self.ly[lyId]["th"][1] is not None):
        th1.append(self.ly[lyId]["th"][1])
      if (self.ly[lyId]["th"][2] is not None):
        th2.append(self.ly[lyId]["th"][2])
      if (self.ly[lyId]["ap"][1] is not None):
        ap1.append(self.ly[lyId]["ap"][1])
      if (self.ly[lyId]["ap"][2] is not None):
        ap2.append(self.ly[lyId]["ap"][2])

    if (len(set(pad1)) != 1):
      msg = "INCORRECT Cu dimensions: pad1"
      logger.error(msg)
      raise RuntimeError(msg)
    if (len(set(pad2)) != 1):
      msg = "INCORRECT Cu dimensions: pad2"
      logger.error(msg)
      raise RuntimeError(msg)
    if (len(set(th1)) != 1):
      msg = "INCORRECT Cu dimensions: th1"
      logger.error(msg)
      raise RuntimeError(msg)
    if (len(set(th2)) != 1):
      msg = "INCORRECT Cu dimensions: th2"
      logger.error(msg)
      raise RuntimeError(msg)
    if (len(set(ap1)) != 1):
      msg = "INCORRECT Cu dimensions: ap1"
      logger.error(msg)
      raise RuntimeError(msg)
    if (len(set(ap2)) != 1):
      msg = "INCORRECT Cu dimensions: ap2"
      logger.error(msg)
      raise RuntimeError(msg)
    '''
    pad1 = []
    pad2 = []
    th1 = []
    th2 = []
    ap1 = []
    ap2 = []
    for lyId in ("SMT", "SMB"):
      if(self.ly[lyId]["pad"][1] is not None):
        pad1.append(self.ly[lyId]["pad"][1])
      if(self.ly[lyId]["pad"][2] is not None):
        pad2.append(self.ly[lyId]["pad"][2])
      if(self.ly[lyId]["th"][1] is not None):
        th1.append(self.ly[lyId]["th"][1])
      if(self.ly[lyId]["th"][2] is not None):
        th2.append(self.ly[lyId]["th"][2])
      if(self.ly[lyId]["ap"][1] is not None):
        ap1.append(self.ly[lyId]["ap"][1])
      if(self.ly[lyId]["ap"][2] is not None):
        ap2.append(self.ly[lyId]["ap"][2])

    if(len(set(pad1)) != 1):
      msg = "INCORRECT SM dimensions: pad1"
      logger.error(msg)
      raise RuntimeError(msg)
    if(len(set(pad2)) != 1):
      msg = "INCORRECT SM dimensions: pad2"
      logger.error(msg)
      raise RuntimeError(msg)
    if(len(set(th1)) != 1):
      msg = "INCORRECT SM dimensions: th1"
      logger.error(msg)
      raise RuntimeError(msg)
    if(len(set(th2)) != 1):
      msg = "INCORRECT SM dimensions: th2"
      logger.error(msg)
      raise RuntimeError(msg)
    if(len(set(ap1)) != 1):
      msg = "INCORRECT SM dimensions: ap1"
      logger.error(msg)
      raise RuntimeError(msg)
    if(len(set(ap2)) != 1):
      msg = "INCORRECT SM dimensions: ap2"
      logger.error(msg)
      raise RuntimeError(msg)

    pad1 = []
    pad2 = []
    th1 = []
    th2 = []
    ap1 = []
    ap2 = []
    for lyId in ("PMT", "PMB"):
      if(self.ly[lyId]["pad"][1] is not None):
        pad1.append(self.ly[lyId]["pad"][1])
      if(self.ly[lyId]["pad"][2] is not None):
        pad2.append(self.ly[lyId]["pad"][2])
      if(self.ly[lyId]["th"][1] is not None):
        th1.append(self.ly[lyId]["th"][1])
      if(self.ly[lyId]["th"][2] is not None):
        th2.append(self.ly[lyId]["th"][2])
      if(self.ly[lyId]["ap"][1] is not None):
        ap1.append(self.ly[lyId]["ap"][1])
      if(self.ly[lyId]["ap"][2] is not None):
        ap2.append(self.ly[lyId]["ap"][2])

    if(len(set(pad1)) != 1):
      msg = "INCORRECT PM dimensions: pad1"
      logger.error(msg)
      raise RuntimeError(msg)
    if(len(set(pad2)) != 1):
      msg = "INCORRECT PM dimensions: pad2"
      logger.error(msg)
      raise RuntimeError(msg)
    if(len(set(th1)) != 1):
      msg = "INCORRECT PM dimensions: th1"
      logger.error(msg)
      raise RuntimeError(msg)
    if(len(set(th2)) != 1):
      msg = "INCORRECT PM dimensions: th2"
      logger.error(msg)
      raise RuntimeError(msg)
    if(len(set(ap1)) != 1):
      msg = "INCORRECT PM dimensions: ap1"
      logger.error(msg)
      raise RuntimeError(msg)
    if(len(set(ap2)) != 1):
      msg = "INCORRECT PM dimensions: ap2"
      logger.error(msg)
      raise RuntimeError(msg)
  '''

  def _getPadName_Dim(self, dimId):
    if (dimId == "drl"):
      dim = self.drl[1:3]
    else:
      dim = self.getLayerVal(dimId, "pad")[1:3]

    if ((dim[0] is None) and (dim[1] is None)):
      res = "0"
    elif (dim[0] == dim[1]):
      if (self.unit == "mils"):
        res = "{}".format(dim[0])
      else:
        res = "{}".format(round(dim[0] * 100))
    else:
      if (self.unit == "mils"):
        res = "{}_{}".format(dim[0], dim[1])
      else:
        res = "{}_{}".format(round(dim[0] * 100), round(dim[1] * 100))
    res = res.replace(".0", "")
    return res

  def getPadName(self):
    self.isValid()

    pad = self.getLayerVal("TOP", "pad")
    res = []
    if (pad[0] == "sq"):
      res.append("s")
    elif (pad[0] == "cir"):
      if (self.drl_via):
        res.append("v")
      else:
        res.append("c")
    elif (pad[0] == "rec"):
      res.append("r")
    elif (pad[0] == "obl"):
      res.append("b")
    else:
      msg = "CANNOT generate padstack name"
      logger.error(self)
      logger.error(msg)
      raise RuntimeError(msg)
    res.append(self._getPadName_Dim("TOP"))

    smt = self.getLayerVal("SMT", "pad")
    pmt = self.getLayerVal("PMT", "pad")
    if (self.drl[0] is None):
      if ((smt[0] != pad[0]) or (smt[1] != pad[1]) or (smt[2] != pad[2])):
        res.append("m")
        res.append(self._getPadName_Dim("SMT"))
      if ((pmt[0] != pad[0]) or (pmt[1] != pad[1]) or (pmt[2] != pad[2])):
        res.append("p")
        res.append(self._getPadName_Dim("PMT"))
    else:
      if ((smt[0] != pad[0]) or (smt[1] != pad[1]) or (smt[2] != pad[2])):
        res.append("m")
        res.append(self._getPadName_Dim("SMT"))
      if ((pmt[0] is not None)
          and ((pmt[0] != pad[0]) or (pmt[1] != pad[1]) or (pmt[2] != pad[2]))):
        res.append("p")
        res.append(self._getPadName_Dim("PMT"))

      if (self.drl_plate):
        res.append("h")
      else:
        res.append("hn")
      res.append(self._getPadName_Dim("drl"))

    if (self.unit == "mils"):
      res.append("_mils")

    res = [str(t) for t in res]  # convert all values to string
    self.IPCname = "".join(res)
    return self.IPCname

def _getAllegroScr_opts(unit, decimals, bSuppress=True, bARK=False):
  """
  returns Allegro script lines for setting the dimension and associated parameters
  :param unit: measurement unit
  :param decimals: [positive int] number of decimals for representing dimension
  :param bSuppress: [True|False] suppress unused pads
  :param bARK: [True|False] anti-pad as route keep-out
  """
  res = []
  if (unit not in pad_units):
    msg = "UNKNOWN unit: {}".format(unit)
    logger.error(msg)
    raise RuntimeError(msg)
  if ((decimals < 0) or (decimals > 4)):
    msg = "UNSUPPORTED decimals value: {}".format(decimals)
    logger.error(msg)
    raise RuntimeError(msg)

  res.append("FORM padedit units {}".format(unit))
  if (unit not in ("Mils", "Inch")):
    res.append("fillin yes")
  res.append("FORM padedit decimal_places {}".format(decimals))
  if (bSuppress):
    res.append("FORM padedit supress_internal YES")
  else:
    res.append("FORM padedit supress_internal NO")

  if (bARK):
    res.append("FORM padedit antipad_keepouts YES")
  else:
    res.append("FORM padedit antipad_keepouts NO")

  return res

def _getAllegroScr_drl(drlShape, bPlated, dh1, dh2, dtp, dtn):
  """
  Returns Allegro script lines for setting the drill and associated parameters
  This function will set the x and y tolerances to 0.0,
  the x and y offsets to 0 and the non-standard drill to none

  :param drlShape:
  :param bPlated: [True|False]
  :param dh1: drill hole dimension 1
  :param dh2: drill hole dimension 2
  :param dtp: drill hole tolerance pos
  :param dtn: drill hole tolerance neg
  """
  res = []
  if (drlShape not in pad_drill_shapes):
    msg = "UNKNOWN drill shape: {}".format(drlShape)
    logger.error(msg)
    raise RuntimeError(msg)
  res.append("FORM padedit hole_type {}".format(drlShape))
  if (bPlated):
    res.append("FORM padedit plating Plated")
  else:
    res.append("FORM padedit plating Non-Plated")
  res.append("FORM padedit hole_size_x {}".format(dh1))
  if (drlShape != "Circle Drill"):
    res.append("FORM padedit hole_size_y {}".format(dh2))
  res.append("FORM padedit hole_pos_tolerance {}".format(dtp))
  res.append("FORM padedit hole_neg_tolerance {}".format(dtn))
  res.append("FORM padedit hole_x_offset 0")
  res.append("FORM padedit hole_y_offset 0")
  res.append("FORM padedit hole_non_standard_drill")
  return res

def _getAllegroScr_drl_fig(drlFig, ch, chW, chH):
  """
  returns Allegro script lines for setting the drill figure and associated parameters
  :param drlFig:
  :param ch:
  :param chW:
  :param chH:
  """
  res = []
  if (drlFig not in pad_drill_figures):
    msg = "UNKNOWN drill symbol: {}".format(drlFig)
    logger.error(msg)
    raise RuntimeError(msg)

  res.append("FORM padedit drill_fig {}".format(drlFig))
  if (drlFig == "Null"):
    res.append("FORM padedit characters -")
    res.append("FORM padedit drill_width 0")
    res.append("FORM padedit drill_height 0")
  else:
    res.append("FORM padedit characters {}".format(ch))
    res.append("FORM padedit drill_width {}".format(chW))
    res.append("FORM padedit drill_height {}".format(chH))
  return res

def _getAllegroScr_layer(layer, padShape, padX, padY, trShape, trX, trY, apadShape, apadX,
                         apadY):
  """
  returns Allegro script lines for setting the padstack on a single layer
  :param layer:
  :param padShape: pad shape
  :param padX: [positive int] pad width
  :param padY: [positive int] pad height
  :param trShape: thermal relief shape
  :param trX: [positive int] thermal relief width
  :param trY - [positive int] thermal relief height
  :param apadShape - anti-pad shape
  :param apadX - [positive int] anti-pad width
  :param apadY - [positive int] anti-pad height
  """
  if (layer not in pad_layers):
    msg = "UNKNOWN layer: {}".format(layer)
    logger.error(msg)
    raise RuntimeError(msg)
  if (padShape not in pad_shapes):
    msg = "UNKNOWN padShape: {}".format(padShape)
    logger.error(msg)
    raise RuntimeError(msg)
  if (trShape not in pad_shapes):
    msg = "UNKNOWN thShape: {}".format(trShape)
    logger.error(msg)
    raise RuntimeError(msg)
  if (apadShape not in pad_shapes):
    msg = "UNKNOWN apadShape: {}".format(apadShape)
    logger.error(msg)
    raise RuntimeError(msg)

  res = [
      "FORM padedit grid row {}".format(layer), "FORM padedit geometry {}".format(padShape)
  ]
  if (padShape == "Null"):
    res.append("FORM padedit width 0")
    res.append("FORM padedit height 0")
  else:
    res.append("FORM padedit width {}".format(padX))
    res.append("FORM padedit height {}".format(padY))

  res.append("FORM padedit geometry_th {}".format(trShape))
  if (trShape == "Null"):
    res.append("FORM padedit width_th 0")
    res.append("FORM padedit height_th 0")
  else:
    res.append("FORM padedit width_th {}".format(trX))
    res.append("FORM padedit height_th {}".format(trY))

  res.append("FORM padedit geometry_ant {}".format(apadShape))
  if (apadShape == "Null"):
    res.append("FORM padedit width_ant 0")
    res.append("FORM padedit height_ant 0")
  else:
    res.append("FORM padedit width_ant {}".format(apadX))
    res.append("FORM padedit height_ant {}".format(apadY))
  return res

def write_AllegroScr(fPath, psmPath, ps):
  """
  make Allegro .scr file for PTH padstack
  :param fPath: folder path for .scr file
  :param psmPath: folder where the .pad file should be saved
  :param ps: PadStack()
  """
  padName = ps.getPadName()
  logger.info("pad name: {}".format(padName))
  fOutName = os.path.join(fPath, "{}.scr".format(padName))

  content = [
      "# Allegro script",
      "# generated by {}".format(os.path.basename(__file__)),
      #"version 16.2",
      "",
      "setwindow form.padedit",
      "FORM padedit parameters"
  ]
  content += _getAllegroScr_opts(dictConvUnits[ps.unit], ps.decimals)
  content.append("")
  if (ps.drl[0] is None):
    content += _getAllegroScr_drl("Circle Drill", False, 0, 0, 0, 0)
  else:
    content += _getAllegroScr_drl(dictConvDrills[ps.drl[0]], ps.drl_plate, ps.drl[1],
                                  ps.drl[2], ps.drl_tol[0], ps.drl_tol[1])
  content.append("")
  content += _getAllegroScr_drl_fig(ps.drl_fig[0], ps.drl_fig[1], ps.drl_fig[2],
                                    ps.drl_fig[3])
  content.append("")

  content.append("FORM padedit layers")
  content.append("FORM padedit single NO")
  content.append("")
  for lyId in ("TOP", "INT", "BOT"):
    if (ps.ly[lyId]["pad"][0] is not None):
      if (ps.drl_via):
        content += _getAllegroScr_layer(dictConvLayers[lyId],
                                        dictConvShapes[ps.ly[lyId]["pad"][0]],
                                        ps.ly[lyId]["pad"][1], ps.ly[lyId]["pad"][2],
                                        dictConvShapes[None], None, None,
                                        dictConvShapes[None], None, None)
        content.append("")
      else:
        content += _getAllegroScr_layer(dictConvLayers[lyId],
                                        dictConvShapes[ps.ly[lyId]["pad"][0]],
                                        ps.ly[lyId]["pad"][1], ps.ly[lyId]["pad"][2],
                                        dictConvShapes[ps.ly[lyId]["th"][0]],
                                        ps.ly[lyId]["th"][1], ps.ly[lyId]["th"][2],
                                        dictConvShapes[ps.ly[lyId]["ap"][0]],
                                        ps.ly[lyId]["ap"][1], ps.ly[lyId]["ap"][2])
        content.append("")
  for lyId in ("SMT", "SMB", "PMT", "PMB"):
    if (ps.ly[lyId]["pad"][0] is not None):
      content += _getAllegroScr_layer(
          dictConvLayers[lyId], dictConvShapes[ps.ly[lyId]["pad"][0]],
          ps.ly[lyId]["pad"][1], ps.ly[lyId]["pad"][2],
          dictConvShapes[ps.ly[lyId]["th"][0]], ps.ly[lyId]["th"][1], ps.ly[lyId]["th"][2],
          dictConvShapes[ps.ly[lyId]["ap"][0]], ps.ly[lyId]["ap"][1], ps.ly[lyId]["ap"][2])
      content.append("")

  tmp = [
      t for t in [
          ps.getLayerVal("TOP", "pad")[1],
          ps.getLayerVal("INT", "pad")[1],
          ps.getLayerVal("BOT", "pad")[1]
      ] if (t is not None)
  ]
  max_pad1 = max(tmp)
  tmp = [
      t for t in [
          ps.getLayerVal("TOP", "pad")[2],
          ps.getLayerVal("INT", "pad")[2],
          ps.getLayerVal("BOT", "pad")[2]
      ] if (t is not None)
  ]
  max_pad2 = max(tmp)
  content.append("FORM padedit parameters")
  content.append("")

  content.append("setwindow form.padedit")
  content.append("pse_check")
  if ((ps.drl[0] is not None) and ((ps.drl[1] > max_pad1) or (ps.drl[2] > max_pad2))):
    content.append("setwindow text")
    content.append("close")
  content.append("")

  content.append("setwindow form.padedit")
  content.append("pse_save_as")
  if ((ps.drl[0] is not None) and ((ps.drl[1] > max_pad1) or (ps.drl[2] > max_pad2))):
    content.append("setwindow text")
    content.append("close")
    content.append("fillin yes")
  content.append("fillin \"{}\"".format(
      os.path.abspath(os.path.join(psmPath, "{}.pad".format(padName)))))
  content.append("")

  content.append("setwindow form.padedit")
  content.append("pse_exit")

  with open(fOutName, "w") as fOut:
    for ln in content:
      fOut.write(ln + "\n")
  logger.info("{} written".format(fOutName))
