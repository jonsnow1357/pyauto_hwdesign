#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.config
import pyauto_base.db
import pyauto_base.cli

dbCtl = pyauto_base.db.DBController()
dbCfg = pyauto_base.config.DBConfig()

def _make_SM():
  dbCtl.dbInfo = dbCfg.dictDBInfo["0"]
  dbCtl.connect()
  rset = dbCtl.selectTable("pad_SM")
  dbCtl.disconnect()

  for row in rset:
    dictOpts = {
        "unit": row[0],
        "shape": row[1],
        "cu": row[2],
        "sm": row[3],
        "pm": row[4],
        "path_psm": dbCfg.path["psm"]
    }
    #logger.info(dictOpts)
    pyauto_base.misc.runScriptMainApp(
        "pyauto_hwdesign.cadence.allegro.generate.padstack.mkscrSMPad",
        logger,
        appDir=appDir,
        appCfgPath=appCfgPath,
        dictCLIArgs=dictOpts)

def _make_PTH():
  dbCtl.dbInfo = dbCfg.dictDBInfo["0"]
  dbCtl.connect()
  rset = dbCtl.selectTable("pad_PTH")
  dbCtl.disconnect()

  for row in rset:
    dictOpts = {
        "unit": row[0],
        "shape": row[1],
        "cu": row[2],
        "sm": row[3],
        "pm": row[4],
        "drl": row[5],
        "dsz": row[6],
        "dfig": row[7],
        "via": pyauto_base.misc.isTrueString(row[8]),
        "path_psm": dbCfg.path["psm"]
    }
    #logger.info(dictOpts)
    pyauto_base.misc.runScriptMainApp(
        "pyauto_hwdesign.cadence.allegro.generate.padstack.mkscrPTHPad",
        logger,
        appDir=appDir,
        appCfgPath=appCfgPath,
        dictCLIArgs=dictOpts)

def _make_NPTH():
  dbCtl.dbInfo = dbCfg.dictDBInfo["0"]
  dbCtl.connect()
  rset = dbCtl.selectTable("pad_NPTH")
  dbCtl.disconnect()

  for row in rset:
    dictOpts = {
        "unit": row[0],
        "shape": row[1],
        "cu": row[2],
        "sm": row[3],
        "pm": row[4],
        "drl": row[5],
        "dsz": row[6],
        "dfig": row[7],
        "via": False,
        "path_psm": dbCfg.path["psm"]
    }
    #logger.info(dictOpts)
    pyauto_base.misc.runScriptMainApp(
        "pyauto_hwdesign.cadence.allegro.generate.padstack.mkscrNPTHPad",
        logger,
        appDir=appDir,
        appCfgPath=appCfgPath,
        dictCLIArgs=dictOpts)

def _writeScript():
  os.chdir("data_out")
  fOutName = "pad.bat"
  with open(fOutName, "w") as fOut:
    for f in os.listdir(""):
      if (f.endswith(".scr")):
        #fOut.write("del {}\n".format(os.path.join(appCfg.path["psm"], f[:-4]+".pad")))
        fOut.write("pad_designer.exe -nographic -s {}\n".format(f))
  logger.info("{} written".format(fOutName))

  fOutName = "pad.sh"
  with open(fOutName, "w") as fOut:
    fOut.write("#! /bin/bash")
    fOut.write("\n\n")
    for f in os.listdir(""):
      if (f.endswith(".scr")):
        fOut.write("./pad_designer -nographic -s {}\n".format(f))
  logger.info("{} written".format(fOutName))

def _callPadDesigner():
  for f in os.listdir(""):
    if (f.endswith(".scr")):
      fPath = os.path.join(dbCfg.path["psm"], (f[:-4] + ".pad"))
      if (os.path.isfile(fPath)):
        logger.info("RM: {}".format(fPath))
        os.remove(fPath)

      cmd = "pad_designer.exe"
      lstArgs = ["-nographic", "-s", f]
      logger.info("RUN: {} {}".format(cmd, lstArgs))
      res = pyauto_base.cli.callGenericCmd(cmd, lstArgs)
      if (res != 0):
        msg = "pad_designer FAIL"
        logger.error(msg)
        raise RuntimeError(msg)

def mainApp():
  logger.info("using config file: {}".format(os.path.realpath(cliArgs["cfg"])))
  dbCfg.loadXmlFile(cliArgs["cfg"])
  if (cliArgs["list"]):
    dbCfg.showInfo()
    return

  t0 = datetime.datetime.now()

  pyauto_base.fs.mkOutFolder(bClear=True)

  if (cliArgs["smt"]):
    _make_SM()
  if (cliArgs["pth"]):
    _make_PTH()
  if (cliArgs["npth"]):
    _make_NPTH()

  if (cliArgs["yes"]):
    _writeScript()
    _callPadDesigner()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  appCfgPath = os.path.join(appDir, (modName + ".xml"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "creates .scr files for padstacks"
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  parser.add_argument("-f", "--cfg", default=appCfgPath, help="configuration file path")
  parser.add_argument("-l",
                      "--list",
                      action="store_true",
                      default=False,
                      help="list config file options")
  parser.add_argument("-s",
                      "--smt",
                      action="store_true",
                      default=False,
                      help="SMT pads only")
  parser.add_argument("-p",
                      "--pth",
                      action="store_true",
                      default=False,
                      help="PTH pads only")
  parser.add_argument("-n",
                      "--npth",
                      action="store_true",
                      default=False,
                      help="NPTH pads only")
  parser.add_argument("-y", "--yes", action="store_true", default=False, help="write pads")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
