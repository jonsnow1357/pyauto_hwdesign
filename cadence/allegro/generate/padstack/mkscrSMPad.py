#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_hwdesign.cadence.allegro.generate.padstack.base as libPadstack

def mainApp():
  fOutPath = pyauto_base.fs.mkOutFolder()
  if (cliArgs["path_psm"] is None):
    cliArgs["path_psm"] = os.path.abspath(fOutPath)
  pyauto_base.fs.chkPath_Dir(cliArgs["path_psm"])
  logger.info("path (*.psm): {}".format(cliArgs["path_psm"]))

  try:
    tmp = [float(t) for t in cliArgs["cu"].split(",")]
    if (len(tmp) == 1):
      cu_d1 = tmp[0]
      cu_d2 = None
      cu_th1 = 1.1 * tmp[0]
      cu_th2 = None
      cu_ap1 = 1.1 * tmp[0]
      cu_ap2 = None
    elif (len(tmp) == 2):
      cu_d1 = tmp[0]
      cu_d2 = tmp[1]
      cu_th1 = 1.1 * tmp[0]
      cu_th2 = 1.1 * tmp[1]
      cu_ap1 = 1.1 * tmp[0]
      cu_ap2 = 1.1 * tmp[1]
    elif (len(tmp) == 3):
      cu_d1 = tmp[0]
      cu_d2 = tmp[1]
      cu_th1 = tmp[2] * tmp[0]
      cu_th2 = tmp[2] * tmp[1]
      cu_ap1 = None
      cu_ap2 = None
    elif (len(tmp) == 4):
      cu_d1 = tmp[0]
      cu_d2 = tmp[1]
      cu_th1 = tmp[2] * tmp[0]
      cu_th2 = tmp[2] * tmp[1]
      cu_ap1 = tmp[3] * tmp[0]
      cu_ap2 = tmp[3] * tmp[1]
    else:
      raise ValueError
  except ValueError:
    msg = "WRONG cu: '{}'".format(cliArgs["cu"])
    logger.error(msg)
    raise RuntimeError(msg)

  if (pyauto_base.misc.isEmptyString(cliArgs["sm"])):
    sm_d1 = cu_d1
    sm_d2 = cu_d2
    sm_th1 = None
    sm_th2 = None
    sm_ap1 = None
    sm_ap2 = None
  else:
    try:
      tmp = [float(t) for t in cliArgs["sm"].split(",")]
      if (len(tmp) == 1):
        sm_d1 = tmp[0]
        sm_d2 = None
        sm_th1 = None
        sm_th2 = None
        sm_ap1 = None
        sm_ap2 = None
      elif (len(tmp) == 2):
        sm_d1 = tmp[0]
        sm_d2 = tmp[1]
        sm_th1 = None
        sm_th2 = None
        sm_ap1 = None
        sm_ap2 = None
      elif (len(tmp) == 3):
        sm_d1 = tmp[0]
        sm_d2 = tmp[1]
        sm_th1 = tmp[2] * tmp[0]
        sm_th2 = tmp[2] * tmp[1]
        sm_ap1 = None
        sm_ap2 = None
      elif (len(tmp) == 4):
        sm_d1 = tmp[0]
        sm_d2 = tmp[1]
        sm_th1 = tmp[2] * tmp[0]
        sm_th2 = tmp[2] * tmp[1]
        sm_ap1 = tmp[3] * tmp[0]
        sm_ap2 = tmp[3] * tmp[1]
      else:
        raise ValueError
    except ValueError:
      msg = "WRONG sm: '{}'".format(cliArgs["sm"])
      logger.error(msg)
      raise RuntimeError(msg)

  if (pyauto_base.misc.isEmptyString(cliArgs["pm"])):
    pm_d1 = cu_d1
    pm_d2 = cu_d2
    pm_th1 = None
    pm_th2 = None
    pm_ap1 = None
    pm_ap2 = None
  else:
    try:
      tmp = [float(t) for t in cliArgs["pm"].split(",")]
      if (len(tmp) == 1):
        pm_d1 = tmp[0]
        pm_d2 = None
        pm_th1 = None
        pm_th2 = None
        pm_ap1 = None
        pm_ap2 = None
      elif (len(tmp) == 2):
        pm_d1 = tmp[0]
        pm_d2 = tmp[1]
        pm_th1 = None
        pm_th2 = None
        pm_ap1 = None
        pm_ap2 = None
      elif (len(tmp) == 3):
        pm_d1 = tmp[0]
        pm_d2 = tmp[1]
        pm_th1 = tmp[2] * tmp[0]
        pm_th2 = tmp[2] * tmp[1]
        pm_ap1 = None
        pm_ap2 = None
      elif (len(tmp) == 4):
        pm_d1 = tmp[0]
        pm_d2 = tmp[1]
        pm_th1 = tmp[2] * tmp[0]
        pm_th2 = tmp[2] * tmp[1]
        pm_ap1 = tmp[3] * tmp[0]
        pm_ap2 = tmp[3] * tmp[1]
      else:
        raise ValueError
    except ValueError:
      msg = "WRONG pm: '{}'".format(cliArgs["pm"])
      logger.error(msg)
      raise RuntimeError(msg)

  ps = libPadstack.PadStack()
  ps.setUnit(cliArgs["unit"])
  ps.setLayer("TOP", cliArgs["shape"], cu_d1, cu_d2, cliArgs["shape"], cu_th1, cu_th2,
              cliArgs["shape"], cu_ap1, cu_ap2)
  ps.setLayer("SMT", cliArgs["shape"], sm_d1, sm_d2, cliArgs["shape"], sm_th1, sm_th2,
              cliArgs["shape"], sm_ap1, sm_ap2)
  ps.setLayer("PMT", cliArgs["shape"], pm_d1, pm_d2, cliArgs["shape"], pm_th1, pm_th2,
              cliArgs["shape"], pm_ap1, pm_ap2)
  logger.info(ps)
  libPadstack.write_AllegroScr(fOutPath, cliArgs["path_psm"], ps)

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "creates .scr files for SMT padstacks"
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  parser.add_argument("shape", help="pad shape", choices=("sq", "cir", "rec", "obl"))
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("-u", "--unit", default="mm", help="unit")
  parser.add_argument("--cu",
                      default="1.0",
                      help="pad dimension(s) d1[,d2,th_ratio,ap_ratio]")
  parser.add_argument("--sm", help="solder-mask dimension(s) d1[,d2,th_ratio,ap_ratio]")
  parser.add_argument("--pm", help="paste-mask dimension(s) d1[,d2,th_ratio,ap_ratio]")
  parser.add_argument("--path_psm", help="path for *.psm files")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
