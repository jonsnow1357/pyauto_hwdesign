#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.config
import pyauto_hw.geometry as geo
import pyauto_hwdesign.cadence.allegro.generate.base as genBase

appCfg = pyauto_base.config.SimpleConfig()

def _point_symmetry(pt, deg, d):
  """
  given a point 'pt', an angle value 'deg' and a distance 'd',
  - pt is defined as "(x.xxxx, y.yyyy)"
  - d is a positive value
  :return: the symmetric points situated at distance d on a line oriented at deg
  """
  pt1 = geo.Circle2D(pt=pt, r=d).pointAt(geo.degNormalize(deg))
  pt2 = geo.Circle2D(pt=pt, r=d).pointAt(geo.degNormalize(deg + 180))
  return [pt1, pt2]

def _list():
  flt = genBase.PCBItemFilter()
  flt.callback = flt.filterNone
  lst_items = genBase.readAllegroInfo(cliArgs["txt"], flt)

  dict_type = {}
  for it in lst_items:
    if (it.type in dict_type.keys()):
      dict_type[it.type] += 1
    else:
      dict_type[it.type] = 1
  for k, v in dict_type.items():
    logger.info("{: >24} {}".format(k, v))

def _vias():
  """
  looks for circles of a certain radius and gets the center coordinates to place vias there
  """
  flt = genBase.PCBItemFilter()
  flt.regexType = r"(ARC SEGMENT|LINE)"
  flt.dim = [cliArgs["d1"], cliArgs["d2"]]
  flt.callback = flt.filterTypeRadius
  lst_items = genBase.readAllegroInfo(cliArgs["txt"], flt)

  set_coord = set()
  for it in lst_items:
    set_coord.add("{:.9f}_{:.9f}".format(it.center.x, it.center.y))
  logger.info("found {} item(s)".format(len(set_coord)))

  flt.regexType = r"SHAPE"
  flt.dim = [cliArgs["d1"], cliArgs["d2"]]
  flt.callback = flt.filterType
  lst_items = genBase.readAllegroInfo(cliArgs["txt"], flt)

  for it in lst_items:
    for ln in it.boundary:
      if ("radius" in ln):
        tmp = ln.split()
        x = float(tmp[1].strip("()"))
        y = float(tmp[2].strip("()"))
        r = float(tmp[4].strip("()"))
        if (math.fabs(r - flt.dim[0]) <= (0.01 * flt.dim[0])):
          set_coord.add("{:.9f}_{:.9f}".format(x, y))
  logger.info("found {} item(s)".format(len(set_coord)))

  lst_ln = []
  for val in set_coord:
    tmp = [float(t) for t in val.split("_")]
    lst_ln.append("pick {} y {}".format(round(tmp[0], 4), round(tmp[1], 4)))

  f_out_path = os.path.join(appCfg.path["scripts"], "filter_copy.scr")
  genBase.writeAllegroScript(f_out_path, lst_ln)

def _pins_net():
  """
  looks for pins location to assign net on overlapping shape
  """
  flt = genBase.PCBItemFilter()
  flt.regexType = r"PIN"
  flt.regexRefdes = cliArgs["refdes"]
  flt.callback = flt.filterTypeRefdes
  lst_items = genBase.readAllegroInfo(cliArgs["txt"], flt)

  set_coord = set()
  for it in lst_items:
    set_coord.add("{:.9f}_{:.9f}".format(it.center.x, it.center.y))
  logger.info("found {} item(s)".format(len(set_coord)))

  lst_ln = []
  for val in set_coord:
    tmp = [float(t) for t in val.split("_")]
    lst_ln.append("pick grid {} {}".format(round(tmp[0], 4), round(tmp[1], 4)))
    lst_ln.append("prepopup {} {}".format(round(tmp[0], 4), round(tmp[1], 4)))
    lst_ln.append("pop net list")
    lst_ln.append("pick grid {} {}".format(round(tmp[0], 4), round(tmp[1], 4)))
    lst_ln.append("next")

  f_out_path = os.path.join(appCfg.path["scripts"], "filter_assign_net.scr")
  genBase.writeAllegroScript(f_out_path, lst_ln)

def _pins_shape():
  """
  looks for pins location and adds circular shapes/voids around them
  """
  flt = genBase.PCBItemFilter()
  flt.regexType = r"PIN"
  flt.callback = flt.filterType
  lst_items = genBase.readAllegroInfo(cliArgs["txt"], flt)

  set_coord = set()
  for it in lst_items:
    set_coord.add("{:.9f}_{:.9f}".format(it.center.x, it.center.y))
  logger.info("found {} item(s)".format(len(set_coord)))

  lst_ln = []
  for val in set_coord:
    tmp = [float(t) for t in val.split("_")]
    lst_ln.append("pick grid {} {}".format(round(tmp[0], 4), round(tmp[1], 4)))
    lst_ln.append("ipick {}".format(cliArgs["d1"]))

  f_out_path = os.path.join(appCfg.path["scripts"], "filter_pins_shape.scr")
  genBase.writeAllegroScript(f_out_path, lst_ln)

def _vias_net():
  """
  looks for via location to assign net on overlapping shape
  """
  flt = genBase.PCBItemFilter()
  flt.regexType = r"VIA"
  flt.callback = flt.filterType
  lst_items = genBase.readAllegroInfo(cliArgs["txt"], flt)

  set_coord = set()
  for it in lst_items:
    set_coord.add("{:.9f}_{:.9f}".format(it.center.x, it.center.y))
  logger.info("found {} item(s)".format(len(set_coord)))

  lst_ln = []
  for val in set_coord:
    tmp = [float(t) for t in val.split("_")]
    lst_ln.append("pick grid {} {}".format(round(tmp[0], 4), round(tmp[1], 4)))
    lst_ln.append("prepopup {} {}".format(round(tmp[0], 4), round(tmp[1], 4)))
    lst_ln.append("pop net list")
    lst_ln.append("pick grid {} {}".format(round(tmp[0], 4), round(tmp[1], 4)))
    lst_ln.append("next")

  f_out_path = os.path.join(appCfg.path["scripts"], "filter_assign_net.scr")
  genBase.writeAllegroScript(f_out_path, lst_ln)

def _route_keepout():
  """
  looks for segments and arcs and determines a route keepout shape of clearance around them
  """
  clearance = cliArgs["d1"]
  regexSegment = r" *segment:xy +\([0-9-\. ]+\) xy +\([0-9-\. ]+\) .*"
  regexArc = r" *arc seg:xy +\([0-9-\. ]+\) xy +\([0-9-\. ]+\) width.*"
  regexCenter = r" *center-xy: +\([0-9-\. ]+\) radius +\([0-9-\. ]+\) .*"
  koType = None
  x1 = None
  y1 = None
  x2 = None
  y2 = None
  x_c = None
  y_c = None
  r_c = None

  with open(cliArgs["txt"], "r") as fIn:
    for ln in [t.strip("\n\r") for t in fIn.readlines()]:
      if (re.match(regexSegment, ln) is not None):
        tmp = ln.split()
        x1 = float(tmp[1].replace("(", "").replace(")", ""))
        y1 = float(tmp[2].replace("(", "").replace(")", ""))
        x2 = float(tmp[4].replace("(", "").replace(")", ""))
        y2 = float(tmp[5].replace("(", "").replace(")", ""))
        koType = "segment"
      if (re.match(regexArc, ln) is not None):
        tmp = ln.split()
        x1 = float(tmp[2].replace("(", "").replace(")", ""))
        y1 = float(tmp[3].replace("(", "").replace(")", ""))
        x2 = float(tmp[5].replace("(", "").replace(")", ""))
        y2 = float(tmp[6].replace("(", "").replace(")", ""))
        koType = "arc"
      if (re.match(regexCenter, ln) is not None):
        tmp = ln.split()
        x_c = float(tmp[1].replace("(", "").replace(")", ""))
        y_c = float(tmp[2].replace("(", "").replace(")", ""))
        r_c = float(tmp[4].replace("(", "").replace(")", ""))

  logger.info("== {} route keepout:".format(koType))
  if (koType == "segment"):
    #logger.info([x1, y1, x2, y2])
    seg = geo.Segment2D(x1=x1, y1=y1, x2=x2, y2=y2)
    slope_perpendicular = geo.degNormalize(seg.slope + 90)
    [pt1, pt2] = _point_symmetry(pt=geo.Point2D(x=x1, y=y1),
                                 deg=slope_perpendicular,
                                 d=clearance)
    [pt3, pt4] = _point_symmetry(pt=geo.Point2D(x=x2, y=y2),
                                 deg=slope_perpendicular,
                                 d=clearance)
    logger.info("line : ({}) -> ({})".format(pt1.toString(), pt2.toString()))
    logger.info("line : ({}) -> ({})".format(pt2.toString(), pt4.toString()))
    logger.info("line : ({}) -> ({})".format(pt4.toString(), pt3.toString()))
    logger.info("line : ({}) -> ({})".format(pt3.toString(), pt1.toString()))
  elif (koType == "arc"):
    #logger.info([x1, y1, x2, y2])
    #logger.info([x_c, y_c, r_c])
    geo.epsilon = 5e-3
    cir = geo.Circle2D(x=x_c, y=y_c, r=r_c)
    deg1 = cir.degAt(geo.Point2D(x=x1, y=y1))
    deg2 = cir.degAt(geo.Point2D(x=x2, y=y2))
    geo.setEpsilon()
    r_min = (r_c - clearance)
    cir_min = geo.Circle2D(x=x_c, y=y_c, r=r_min)
    pt1 = cir_min.pointAt(deg1)
    pt2 = cir_min.pointAt(deg2)
    r_max = (r_c + clearance)
    cir_max = geo.Circle2D(x=x_c, y=y_c, r=r_max)
    pt3 = cir_max.pointAt(deg2)
    pt4 = cir_max.pointAt(deg1)
    logger.info("arc  : ({}) -> ({}), r={}".format(pt1.toString(), pt2.toString(), (r_min)))
    logger.info("line : ({}) -> ({})".format(pt2.toString(), pt3.toString()))
    logger.info("arc  : ({}) -> ({}), r={}".format(pt3.toString(), pt4.toString(), (r_max)))
    logger.info("line : ({}) -> ({})".format(pt4.toString(), pt1.toString()))
  else:
    msg = "UNKNOWN keepout type: {}".format(koType)
    logger.error(msg)
    raise RuntimeError(msg)
  #print("----")
  #geo.epsilon = 4e-5
  #cir = geo.Circle2D(x=23.1308,  y=-26.9464, r=8.1308)
  #print(cir.degAt(geo.Point2D(x=15.4557, y=-29.6302)))
  #[pt1, pt2] = _point_symmetry(pt=geo.Point2D(x=15.4557, y=-29.6302), deg=199.2735, d=clearance)
  #pt2.show()
  #pt1.show()

def _drc_net():
  """
  looks for DRC location to assign net on overlapping shape
  """
  flt = genBase.PCBItemFilter()
  flt.regexType = r"VIA"
  flt.callback = flt.filterType
  lst_items = genBase.readAllegroInfo(cliArgs["txt"], flt)

  set_coord = set()
  for it in lst_items:
    set_coord.add("{:.9f}_{:.9f}".format(it.center.x, it.center.y))
  logger.info("found {} item(s)".format(len(set_coord)))

  lst_ln = []
  for val in set_coord:
    tmp = [float(t) for t in val.split("_")]
    lst_ln.append("shape select")
    lst_ln.append("pick grid {} {}".format(round(tmp[0], 4), round(tmp[1], 4)))
    lst_ln.append("prepopup {} {}".format(round(tmp[0], 4), round(tmp[1], 4)))
    lst_ln.append("pop net list")
    lst_ln.append("pick grid {} {}".format(round(tmp[0], 4), round(tmp[1], 4)))
    lst_ln.append("prepopup {} {}".format(round(tmp[0], 4), round(tmp[1], 4)))
    #lst_ln.append("next")

  f_out_path = os.path.join(appCfg.path["scripts"], "filter_assign_net.scr")
  genBase.writeAllegroScript(f_out_path, lst_ln)

def mainApp():
  logger.info("using config file: {}".format(os.path.realpath(cliArgs["cfg"])))
  appCfg.loadCfgFile(cliArgs["cfg"])
  if (cliArgs["list"]):
    appCfg.showInfo()
    return

  if (not pyauto_base.fs.chkPath_Dir(appCfg.path["scripts"], bDie=False)):
    appCfg.path["scripts"] = "./data_out"
  pyauto_base.fs.mkOutFolder(appCfg.path["scripts"])

  if (cliArgs["action"] == "list"):
    _list()
  elif (cliArgs["action"] == "vias"):
    _vias()
  elif (cliArgs["action"] == "pins_net"):
    _pins_net()
  elif (cliArgs["action"] == "pins_shape"):
    _pins_shape()
  elif (cliArgs["action"] == "vias_net"):
    _vias_net()
  elif (cliArgs["action"] == "route_ko"):
    _route_keepout()
  elif (cliArgs["action"] == "drc"):
    _drc_net()
  else:
    msg = "UNKNOWN action: {}".format(cliArgs["action"])
    logger.error(msg)
    raise RuntimeError(msg)

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  appDir = os.getcwd()  # current folder
  appCfgPath = os.path.join(appDir, "filter.cfg")
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "filter Cadence elements and make helper scripts"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("action",
                      help="action",
                      choices=("list", "vias", "pins_net", "pins_shape", "vias_net",
                               "route_ko", "drc"))
  parser.add_argument("txt", help="<file_name>.txt")
  parser.add_argument("-f", "--cfg", default=appCfgPath, help="configuration file path")
  parser.add_argument("-l",
                      "--list",
                      action="store_true",
                      default=False,
                      help="list config file options")
  parser.add_argument("--refdes", default=".*", type=str, help="refdes filter")
  parser.add_argument("-d1", default=0.0, type=float, help="1st dimension")
  parser.add_argument("-d2", default=0.0, type=float, help="2nd dimension")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
