@require(dictInfo, lstData)
<!doctype html>
<html lang="en">
  <head>
    <title>Cadence PCB Materials</title>
    <!--
    <link rel="stylesheet" type="text/css" href="static/{{ modName }}.css">
    <script src="https://code.jquery.com/jquery-1.12.3.min.js"></script>
    -->
    <style>
body {
  font-family: sans-serif;
}

table {
  border-collapse: collapse;
}

table, th, td {
  border: 1px solid black;
}

thead {
  background-color: deepskyblue;
}

tfoot {
  background-color: limegreen;
}

tr:nth-child(even) {
  background-color: lightgrey;
}

th, td {
  padding: 0.2em;
}
    </style>
  </head>

  <body>
    <h1>Cadence PCB Materials</h1>
    <p>Generated on: @{dictInfo["timestamp"]}</p>

    <table>
      <thead>
        <tr>
          <th>Name</th>
          <th>&epsilon;<sub>r</sub></th>
          <th>tan &delta;</th>
        </tr>
      </thead>
      <tbody>
      @for mat in lstData:
        <tr>
          <td>@mat["name"]</td>
          @if mat["type"] == "Cu":
          <td></td>
          <td></td>
          @elif mat["type"] == "dielectric":
          <td>@mat["e_r"]</td>
          <td>@mat["loss_tangent"]</td>
          @end
        </tr>
      @end
      </tbody>
    </table>

  </body>
</html>
