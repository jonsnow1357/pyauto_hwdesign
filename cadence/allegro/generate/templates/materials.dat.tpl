@require(lstData)
#     PCB material properties file.
#     You may set these standard properties:
#
# E_CONDUCTIVITY  - electrical conductivity
# DIELECTRIC      - relative dielectric permittivity
# LOSS_TANGENT    - loss tangent of dielectric
#
#     PCB material parameters for Allegro PCB Cross Section Form
#
# THICKNESS       - standard sheet thickness
# THICKNESS_PTOL  - standard sheet thickness plus tolerance
# THICKNESS_MTOL  - standard sheet thickness minus tolerance
#
#     Any of the above parameters my be omitted.
#     Use any units found in the units.dat file.
#     Values without unit specifiers are presumed to be in SI units.
#
#     The default materials for Allegro PCB layers are COPPER, FR-4, and AIR.
#
# ======================================================================
#
# NOTES:
#
#   1) DIELECTRIC or CONDUCTOR ??
#
#          The Cross section editor (xsection command) splits and
#        presents the materials in two groupings - "conductive" and
#        "non-conductive" materials.
#
#          This determination is based upon the material's electrical-
#        conductivity (E_CONDUCTIVITY) according to the following rule:
#
#           E_CONDUCTIVITY <  10,000 mhos/m  =  "non-conductive"
#           E_CONDUCTIVITY >= 10,000 mhos/m  =  "conductive"
#
#        There is currently NO support for semiconductor materials.
#
# ======================================================================

@for mat in lstData:
MATERIAL_NAME=@mat["name"]
@if mat["type"] == "Cu":
E_CONDUCTIVITY=595900mho/cm
@elif mat["type"] == "dielectric":
E_CONDUCTIVITY=0
DIELECTRIC=@mat["e_r"]
LOSS_TANGENT=@mat["loss_tangent"]
@end
THICKNESS=@mat["thickness"].replace(" ", "")

@end
