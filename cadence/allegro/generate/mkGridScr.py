#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.config
import pyauto_hwdesign.cadence.allegro.generate.base as genBase

appCfg = pyauto_base.config.SimpleConfig()

def _script_set_grid(lstLn, nonEtch, etch):
  lstLn.append("generaledit\n")
  lstLn.append("define grid")
  lstLn.append("setwindow form.grid")
  lstLn.append("FORM grid non_etch non_etch_x_grids {}".format(nonEtch))
  lstLn.append("FORM grid non_etch non_etch_y_grids {}".format(nonEtch))
  lstLn.append("FORM grid all_etch all_etch_x_grids {}".format(etch))
  lstLn.append("FORM grid all_etch all_etch_y_grids {}".format(etch))
  lstLn.append("FORM grid done")
  lstLn.append("setwindow pcb")

def mainApp():
  logger.info("using config file: {}".format(os.path.realpath(cliArgs["cfg"])))
  appCfg.loadCfgFile(cliArgs["cfg"])

  pyauto_base.fs.mkOutFolder(appCfg.path["scripts"])

  lstLn = []
  fOutPath = os.path.join(appCfg.path["scripts"], "grid_0p001.scr")
  _script_set_grid(lstLn, 0.001, 0.001)
  genBase.writeAllegroScript(fOutPath, lstLn)

  lstLn = []
  fOutPath = os.path.join(appCfg.path["scripts"], "grid_0p005.scr")
  _script_set_grid(lstLn, 0.005, 0.005)
  genBase.writeAllegroScript(fOutPath, lstLn)

  lstLn = []
  fOutPath = os.path.join(appCfg.path["scripts"], "grid_0p01.scr")
  _script_set_grid(lstLn, 0.01, 0.01)
  genBase.writeAllegroScript(fOutPath, lstLn)

  lstLn = []
  fOutPath = os.path.join(appCfg.path["scripts"], "grid_0p05.scr")
  _script_set_grid(lstLn, 0.05, 0.05)
  genBase.writeAllegroScript(fOutPath, lstLn)

  lstLn = []
  fOutPath = os.path.join(appCfg.path["scripts"], "grid_0p1.scr")
  _script_set_grid(lstLn, 0.1, 0.1)
  genBase.writeAllegroScript(fOutPath, lstLn)

  lstLn = []
  fOutPath = os.path.join(appCfg.path["scripts"], "grid_0p5.scr")
  _script_set_grid(lstLn, 0.5, 0.5)
  genBase.writeAllegroScript(fOutPath, lstLn)

  lstLn = []
  fOutPath = os.path.join(appCfg.path["scripts"], "grid_1.scr")
  _script_set_grid(lstLn, 1, 1)
  genBase.writeAllegroScript(fOutPath, lstLn)

  lstLn = []
  fOutPath = os.path.join(appCfg.path["scripts"], "grid_5.scr")
  _script_set_grid(lstLn, 5, 5)
  genBase.writeAllegroScript(fOutPath, lstLn)

  lstLn = []
  fOutPath = os.path.join(appCfg.path["scripts"], "grid_10.scr")
  _script_set_grid(lstLn, 10, 10)
  genBase.writeAllegroScript(fOutPath, lstLn)

  lstLn = []
  fOutPath = os.path.join(appCfg.path["scripts"], "grid_50.scr")
  _script_set_grid(lstLn, 50, 50)
  genBase.writeAllegroScript(fOutPath, lstLn)

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  appDir = os.getcwd()  # current folder
  appCfgPath = os.path.join(appDir, "filter.cfg")
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "create predefined grid scripts"
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  parser.add_argument("-f", "--cfg", default=appCfgPath, help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
