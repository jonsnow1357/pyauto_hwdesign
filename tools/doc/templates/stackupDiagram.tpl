@require(stackup, layers, vias)
<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>stackup</title>
  <style>
body {
  font-family: sans-serif;
}

table {
  border-collapse: collapse;
  margin: 0.5em;
}

thead, th {
  padding: 0.2em;
}

td {
  padding: 0em 0.5em 0em 0.5em;
  border: 1px solid black;
  margin: 0em;
}

tr.Cu {
  background-color: sandybrown;
}

#content {
  float: left;
  width: auto;
  //border: 1px solid blue;
}

#sidebar {
  overflow: hidden;
  width: 25%;
  //border: 1px solid red;
}

  </style>
</head>
<body>

<h2>Stackup: @{stackup["id"]}</h2>

<div>
  <div id="content">
    <table>
      <thead>
        <tr>
          <th>Type</th>
          <th>Thickness [mm]</th>
          <th>Thickness [mils]</th>
          <th>Material</th>
          <th>Structure</th>
        </tr>
      </thead>
      <tbody>
        @for ly in layers:
        <tr class="@{ly["type"]}">
          <td>@{ly["type"]}</td>
          <td>@{ly["thickness_mm"]}</td>
          <td>@{ly["thickness_mils"]}</td>
          <td>@{ly["material"]}</td>
          <td>@{ly["structure"]}</td>
        </tr>
        @end
      </tbody>
    </table>

    <table>
      <thead>
        <tr>
          <th>via</th>
          <th>Drill Size [mm]</th>
          <th>Drill Size [mils]</th>
          <th>Description</th>
        </tr>
      </thead>
      <tbody>
      @for via in vias:
      <tr>
        <td>@{via["via"]}</td>
        <td>@{via["drill_mm"]}</td>
        <td>@{via["drill_mils"]}</td>
        <td>@{via["desc"]}</td>
      </tr>
      @end
      </tbody>
    </table>
  </div>

  <div id="sidebar">
    <img src="@{stackup["svg_path"]}" alt="stackup" style="height: 100%; width: 100%; object-fit: contain"/>
  </div>

  <div style="clear: both;"></div>

  <p>Total Thickness: @{stackup["thickness_total_mm"]} mm (@{stackup["thickness_total_mils"]} mils)</p>
  <p>Total Thickness (over plating): @{stackup["thickness_nosm_mm"]} mm (@{stackup["thickness_nosm_mils"]} mils)</p>
</div>

</body>
