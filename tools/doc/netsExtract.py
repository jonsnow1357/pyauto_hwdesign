#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_hw.config
import pyauto_hw.utils
import pyauto_hw.CAD.base as CADbase
import pyauto_hw.CAD.constants as CADct
#import pyauto_hw.CAD.Allegro
#import pyauto_hw.CAD.DX
import pyauto_hw.CAD.HDL

class _PinoutReport(object):

  def __init__(self):
    self.fName = None
    self._data = []

  def __len__(self):
    return len(self._data)

  def add(self, pinNo, netName):
    self._data.append([netName, pinNo])

  def getData(self):
    return self._data

  def writeCSV(self, fPath):
    data = self.getData()

    with open(fPath, "w") as fOut:
      csvOut = csv.writer(fOut, lineterminator="\n")
      lstColHdr = ["net name", "pin", "I/O type"]
      csvOut.writerow(lstColHdr)

      for t in data:
        csvOut.writerow(t + ["???"])
    logger.info("{} written".format(fPath))

def _get_net_series_component(ntl, netName):
  #logger.info("_get_net_series_component: {}".format(netName))
  net = ntl.getNetByName(netName)
  if (net.nRefdes < 2):
    msg = "INCORRECT naming convention (1) for '{}'".format(netName)
    logger.error(msg)
    raise RuntimeError(msg)

  lst_pins = [t for t in net.pinInfo if (t.startswith("R") or t.startswith("C"))]
  lst_comps = []
  for pin in lst_pins:
    refdes = pin.split(".")[0]
    cp = ntl.getComponentByRefdes(refdes)
    if (cp.dnp is False):
      lst_comps.append(cp)
  if (len(lst_comps) == 0):
    return netName
  elif (len(lst_comps) > 1):
    msg = "INCORRECT naming convention (2) for '{}'".format(netName)
    logger.error(msg)
    raise RuntimeError(msg)

  cp = lst_comps[0]
  if (len(cp.netNames) != 2):
    msg = "INCORRECT naming convention (3) for '{}'".format(netName)
    logger.error(msg)
    raise RuntimeError(msg)
  tmp = set(cp.netNames) - {netName}
  #logger.info("net: {}".format(tmp))
  return list(tmp)[0]

def mainApp():
  brdCfg = pyauto_hw.config.BoardConfig(cliArgs["brd"])
  if (cliArgs["list"]):
    brdCfg.showInfo()
    return

  if ("netlist" not in brdCfg.path.keys()):
    msg = "MISSING 'netlist' path"
    logger.error(msg)
    raise RuntimeError(msg)
  if (pyauto_base.misc.isEmptyString(brdCfg.path["netlist"])):
    msg = "MISSING 'netlist' path"
    logger.error(msg)
    raise RuntimeError(msg)
  ntl = pyauto_hw.utils.readNetlist(brdCfg.path["netlist"])

  report = _PinoutReport()
  #regexPinoutIgnore = re.compile(brdCfg.vars["regexPinoutIgnore"])
  cp = ntl.getComponentByRefdes(cliArgs["refdes"])
  for net_name, net in ntl.getAllNetsByName().items():
    if (cp.refdes not in net.refdes):
      continue
    if (CADbase.isGround(net_name)):
      continue
    if (CADbase.isPower(net_name)):
      continue
    if (re.match(CADct.regexNetNC, net_name) is not None):
      continue
    #if (regexPinoutIgnore.match(net_name) is not None):
    #  continue

    for pi in net.pinInfo:
      if (pi.startswith(cp.refdes)):
        if (net_name.startswith("R_")):
          tmp = _get_net_series_component(ntl, net_name)
          report.add(pi.replace("{}.".format(cp.refdes), ""), tmp)
        elif (net_name.startswith("C_")):
          tmp = _get_net_series_component(ntl, net_name)
          report.add(pi.replace("{}.".format(cp.refdes), ""), tmp)
        else:
          report.add(pi.replace("{}.".format(cp.refdes), ""), net_name)

  outPath = "data_out"
  pyauto_base.fs.mkOutFolder(outPath)
  fOutPath = os.path.join(outPath, "pinout_{}.csv".format(cp.refdes))
  report.writeCSV(fOutPath)

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "pinout report"
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  parser.add_argument("brd", help="board config path")
  parser.add_argument("refdes", help="refdes")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  parser.add_argument("-l",
                      "--list",
                      action="store_true",
                      default=False,
                      help="list config file options")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
