#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_base.cli
import pyauto_base.misc

def writeStackup_HTML(brdCfg, pathHTML):
  dict_tpl = {
      "stackup": {
          "id":
          brdCfg.name,
          "svg_path":
          "stackup_{}.svg".format(brdCfg.name),
          "thickness_total_mm":
          "{:.6f}".format(brdCfg.pcb.stackup.thickness_total.to("mm").magnitude),
          "thickness_total_mils":
          "{:.6f}".format(brdCfg.pcb.stackup.thickness_total.to("thou").magnitude),
          "thickness_nosm_mm":
          "{:.6f}".format(brdCfg.pcb.stackup.thickness_noSMask.to("mm").magnitude),
          "thickness_nosm_mils":
          "{:.6f}".format(brdCfg.pcb.stackup.thickness_noSMask.to("thou").magnitude)
      },
      "layers": [],
      "vias": [],
  }

  for ly in brdCfg.pcb.stackup.layers:
    dict_tpl["layers"].append({
        "type":
        ly.type,
        "thickness_mm":
        "{:.6f}".format(ly.thickness.to("mm").magnitude),
        "thickness_mils":
        "{:.6f}".format(ly.thickness.to("thou").magnitude),
        "material":
        ly.material if (ly.material is not None) else "",
        "structure":
        ly.structure
    })
  for via in brdCfg.pcb.stackup.vias:
    dict_tpl["vias"].append({
        "via":
        "{} - {}".format(via.startLy, via.stopLy),
        "drill_mm":
        "{:.6f}".format(via.drillSize.to("mm").magnitude),
        "drill_mils":
        "{:.6f}".format(via.drillSize.to("thou").magnitude),
        "desc":
        ""
    })
  #print(appParams.dict_tpl)

  pyauto_base.misc.templateWheezy(
      os.path.join(os.path.dirname(__file__), "templates", "stackupDiagram.tpl"), pathHTML,
      dict_tpl)

def writeStackup_PNG(pathHTML, pathPNG):
  res = pyauto_base.cli.popenGenericCmd("wkhtmltoimage", ["--version"])
  logger.info(res[0])

  res = pyauto_base.cli.popenGenericCmd(
      "wkhtmltoimage",
      ["--enable-local-file-access", "--log-level", "info", pathHTML, pathPNG])
  for ln in res:
    logger.info(ln)
  logger.info("{} written".format(pathPNG))
