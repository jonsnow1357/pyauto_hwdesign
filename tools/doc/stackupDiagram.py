#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.svg
import pyauto_base.doc
import pyauto_hw.config
import pyauto_hwdesign.tools.doc.utils

_modDir = os.path.dirname(os.path.abspath(__file__))
_pathOut = os.path.join(_modDir, "data_out")

def mainApp():
  t0 = datetime.datetime.now()

  dict_boards = pyauto_hw.config.getBoardConfig(cliArgs["path"])
  for brdCfg in dict_boards.values():
    if ((brdCfg.pcb is None) or (brdCfg.pcb.stackup is None)):
      msg = "NO stackup found for {}".format(brdCfg.name)
      logger.error(msg)
      raise RuntimeError(msg)

    if (brdCfg.pcb.isConsistent() is not True):
      msg = "INCORRECT stackup/artwork"
      logger.error(msg)
      raise RuntimeError(msg)

    logger.info(brdCfg.pcb.stackup)
    pyauto_base.fs.mkOutFolder(_pathOut)

    path_out_svg = os.path.join(_pathOut, "stackup_{}.svg".format(brdCfg.name))
    path_out_html = os.path.join(_pathOut, "stackup_{}.html".format(brdCfg.name))
    path_out_png = os.path.join(_pathOut, "stackup_{}.png".format(brdCfg.name))

    brdCfg.pcb.stackup.writeSVG(path_out_svg)
    pyauto_hwdesign.tools.doc.utils.writeStackup_HTML(brdCfg, path_out_html)

    pyauto_hwdesign.tools.doc.utils.writeStackup_PNG(path_out_html, path_out_png)

    docConv = pyauto_base.doc.DocConversion()
    docInfo = pyauto_base.doc.DocInfo(brdCfg.name, path_out_html)
    #docInfo.docNo = "xxxxxxxx"
    #docInfo.docVer = "01"
    docInfo.convHTML2PDF = True
    #docInfo.convOrientation = "landscape"
    #logger.info(docInfo)
    docConv.addDocInfo(docInfo)

    #logger.info(docConv)
    docConv.writeScript(fPath=_pathOut)

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "create stack-up diagram"
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  parser.add_argument("path", help="board config path")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  parser.add_argument("-l",
                      "--list",
                      action="store_true",
                      default=False,
                      help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
