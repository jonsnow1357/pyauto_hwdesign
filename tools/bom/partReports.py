#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""apps"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_hw.config
import pyauto_base.convert
import pyauto_hw.utils
import pyauto_hw.CAD.constants as CADct
import pyauto_hw.CAD.base as CADbase

def _report_capacitors(ntl, fPath):
  logger.info("-- capacitors report")

  dictCaps = {}
  for refdes in ntl.refdes:
    if (re.match(CADct.regexPartCap, refdes) is not None):
      comp = ntl.getComponentByRefdes(refdes)
      #print("DBG", comp.refdes, comp.value, comp.paramsCAD)
      page = comp.paramsCAD["page"]
      pn = comp.paramsCAD["local_pn"]
      mfr_pn = comp.paramsCAD["mfr_pn"]
      key = "{},{},{}".format(page, pn, comp.value)
      if (key in dictCaps.keys()):
        dictCaps[key][3].append(comp.refdes)
      else:
        dictCaps[key] = [mfr_pn, comp.footprint, comp.paramsCAD["type"], [comp.refdes]]

  with open(fPath, "w") as fOut:
    csvOut = csv.writer(fOut, lineterminator="\n")
    csvOut.writerow(
        ["PAGE", "PN", "VALUE", "DIELECTRIC", "FOOTPRINT", "QTY", "MFR_PN", "REFDES"])
    for k, v in dictCaps.items():
      csvOut.writerow(k.split(",") + [v[2], v[1], len(v[3]), v[0], ",".join(v[3])])
  logger.info("{} written".format(fPath))

def _report_resistors(ntl, fPath):
  logger.info("-- resistors report")

  dictRes = {}
  for refdes in ntl.refdes:
    if (re.match(CADct.regexPartRes, refdes) is not None):
      comp = ntl.getComponentByRefdes(refdes)
      #print("DBG", comp.refdes, comp.value, comp.paramsCAD)
      page = comp.paramsCAD["page"]
      pn = comp.paramsCAD["local_pn"]
      mfr_pn = comp.paramsCAD["mfr_pn"]
      key = "{},{},{}".format(page, pn, comp.value)
      if (key in dictRes.keys()):
        dictRes[key][2].append(comp.refdes)
      else:
        dictRes[key] = [mfr_pn, comp.footprint, [comp.refdes]]

  with open(fPath, "w") as fOut:
    csvOut = csv.writer(fOut, lineterminator="\n")
    csvOut.writerow(["PAGE", "PN", "VALUE", "MFR_PN", "FOOTPRINT", "QTY"])
    for k, v in dictRes.items():
      csvOut.writerow(k.split(",") + [v[0], v[1], len(v[2])])
  logger.info("{} written".format(fPath))

def _report_inductors(ntl, fPath):
  logger.info("-- inductors report")

  dictRes = {}
  for refdes in ntl.refdes:
    if (re.match(CADct.regexPartInd, refdes) is not None):
      comp = ntl.getComponentByRefdes(refdes)
      #print("DBG", comp.refdes, comp.value, comp.paramsCAD)
      page = comp.paramsCAD["page"]
      pn = comp.paramsCAD["local_pn"]
      mfr_pn = comp.paramsCAD["mfr_pn"]
      key = "{},{},{}".format(page, pn, comp.value)
      if (key in dictRes.keys()):
        dictRes[key][2].append(comp.refdes)
      else:
        dictRes[key] = [mfr_pn, comp.footprint, [comp.refdes]]

  with open(fPath, "w") as fOut:
    csvOut = csv.writer(fOut, lineterminator="\n")
    csvOut.writerow(["PAGE", "PN", "VALUE", "MFR_PN", "FOOTPRINT", "QTY"])
    for k, v in dictRes.items():
      csvOut.writerow(k.split(",") + [v[0], v[1], len(v[2])])
  logger.info("{} written".format(fPath))

def _report_ferrites(ntl, fPath):
  logger.info("-- ferrite beads report")

  dictRes = {}
  for refdes in ntl.refdes:
    if (re.match(CADct.regexPartFB, refdes) is not None):
      comp = ntl.getComponentByRefdes(refdes)
      #print("DBG", comp.refdes, comp.value, comp.paramsCAD)
      page = comp.paramsCAD["page"]
      pn = comp.paramsCAD["local_pn"]
      mfr_pn = comp.paramsCAD["mfr_pn"]
      key = "{},{},{}".format(page, pn, comp.value)
      if (key in dictRes.keys()):
        dictRes[key][2].append(comp.refdes)
      else:
        dictRes[key] = [mfr_pn, comp.footprint, [comp.refdes]]

  with open(fPath, "w") as fOut:
    csvOut = csv.writer(fOut, lineterminator="\n")
    csvOut.writerow(["PAGE", "PN", "VALUE", "MFR_PN", "FOOTPRINT", "QTY"])
    for k, v in dictRes.items():
      csvOut.writerow(k.split(",") + [v[0], v[1], len(v[2])])
  logger.info("{} written".format(fPath))

def _report_ICs(ntl, fPath):
  logger.info("-- ICs report")

  dictICs = {}
  for refdes in ntl.refdes:
    if (re.match(CADct.regexPartICorConn, refdes) is not None):
      comp = ntl.getComponentByRefdes(refdes)
      #print("DBG", comp.refdes, comp.value, comp.paramsCAD)
      page = comp.paramsCAD["page"]
      pn = comp.paramsCAD["local_pn"]
      mfr_pn = comp.paramsCAD["mfr_pn"]
      key = "{},{},{}".format(page, pn, comp.value)
      if (key in dictICs.keys()):
        dictICs[key][2].append(comp.refdes)
      else:
        dictICs[key] = [mfr_pn, comp.footprint, [comp.refdes]]

  with open(fPath, "w") as fOut:
    csvOut = csv.writer(fOut, lineterminator="\n")
    csvOut.writerow(["PAGE", "PN", "VALUE", "MFR_PN", "FOOTPRINT", "REFDES"])
    for k, v in dictICs.items():
      csvOut.writerow(k.split(",") + [v[0], v[1], ",".join(v[2])])
  logger.info("{} written".format(fPath))

def _report_decoupling(ntl, fPath):
  logger.info("-- decoupling capacitors report")

  dictCaps = {}
  for refdes in ntl.refdes:
    if (re.match(CADct.regexPartCap, refdes) is not None):
      comp = ntl.getComponentByRefdes(refdes)
      # print("DBG", comp.refdes, comp.value, comp.paramsCAD)
      pn = comp.paramsCAD["local_pn"]
      mfr_pn = comp.paramsCAD["mfr_pn"]
      netName0, netName1 = comp.netNames
      key = None
      if (CADbase.isPower(netName0) and CADbase.isGround(netName1)):
        key = "{},{},{}".format(netName0, pn, comp.value)
      elif (CADbase.isGround(netName0) and CADbase.isPower(netName1)):
        key = "{},{},{}".format(netName1, pn, comp.value)
      if (key is not None):
        if (key in dictCaps.keys()):
          dictCaps[key][3].append(comp.refdes)
        else:
          dictCaps[key] = [mfr_pn, comp.footprint, comp.paramsCAD["type"], [comp.refdes]]

  with open(fPath, "w") as fOut:
    csvOut = csv.writer(fOut, lineterminator="\n")
    csvOut.writerow(["RAIL", "PN", "VALUE", "DIELECTRIC", "FOOTPRINT", "QTY", "MFR_PN"])
    for k, v in dictCaps.items():
      csvOut.writerow(k.split(",") + [v[2], v[1], len(v[3]), v[0]])
  logger.info("{} written".format(fPath))

def _report_AC_coupling(ntl, fPath):
  logger.info("-- AC coupling report")

  lstRes1 = []
  for refdes in ntl.refdes:
    if (re.match(CADct.regexPartCap, refdes) is not None):
      comp = ntl.getComponentByRefdes(refdes)
      # print("DBG", comp.refdes, comp.value, comp.paramsCAD)
      netName0, netName1 = comp.netNames
      if (CADbase.isGround(netName0)):
        continue
      if (CADbase.isGround(netName1)):
        continue
      if (CADbase.isPower(netName0)):
        continue
      if (CADbase.isPower(netName1)):
        continue
      lstRes1.append([netName0, netName1, comp.refdes, comp.value, comp.footprint])

  lstRes2 = []
  for val in lstRes1:
    net0 = ntl.getNetByName(val[0])
    net1 = ntl.getNetByName(val[1])
    #print("DBG", net0.name, net0.refdes, net1.name, net1.refdes)
    IC_cnt = 0
    for r in net0.refdes:
      if (re.match(CADct.regexPartICorConn, r) is not None):
        IC_cnt += 1
    for r in net1.refdes:
      if (re.match(CADct.regexPartICorConn, r) is not None):
        IC_cnt += 1

    if (IC_cnt == 2):
      lstRes2.append(val)
    elif (IC_cnt > 2):
      logger.warning("Confusing nets:\n  {} {}\n  {} {}".format(net0.name, net0.refdes,
                                                                net1.name, net1.refdes))

  with open(fPath, "w") as fOut:
    csvOut = csv.writer(fOut, lineterminator="\n")
    csvOut.writerow(["NET_0", "NET_1", "REFDES", "VALUE", "FOOTPRINT"])
    for val in lstRes2:
      csvOut.writerow(val)
  logger.info("{} written".format(fPath))

class I2CBus(object):

  def __init__(self):
    self.name = None
    self.net_names = []
    self.refdes = set()
    self.R_s = {"scl": set(), "sda": set()}
    self.R_pu = {"scl": None, "sda": None}
    self.voltage = {"scl": None, "sda": None}

  def fail(self, msg):
    tmp = "{} {}: {}".format(self.__class__.__name__, self.name, msg)
    logger.error(tmp)
    raise RuntimeError(tmp)

def _report_I2C(ntl, fPath):
  logger.info("-- I2C report")

  dictI2C = {}
  for nname in ntl.netNames:
    if (nname.startswith("UNNAMED_")):
      continue

    if (nname.endswith("_SCL") or nname.endswith("_SDA")):
      if (nname.startswith("R_")):
        bus_name = nname[2:-4]
      else:
        bus_name = nname[:-4]

      if (bus_name not in dictI2C.keys()):
        bus = I2CBus()
        bus.name = bus_name
        dictI2C[bus.name] = bus
      else:
        bus = dictI2C[bus_name]
      bus.net_names.append(nname)

  for bus in dictI2C.values():
    #print("DBG process {}".format(bus.name))
    if ((len(bus.net_names) != 2) and (len(bus.net_names) != 4)):
      bus.fail("INCORRECT nets {}".format(bus.net_names))

    for nname in bus.net_names:
      #print("DBG process {}".format(nname))
      net = ntl.getNetByName(nname)
      for refdes in net.refdes:
        if (refdes.startswith("R")):
          comp = ntl.getComponentByRefdes(refdes)
          if (pyauto_base.convert.value2float(comp.value) < 50.0):
            if (nname.endswith("SCL")):
              bus.R_s["scl"].add(refdes)
            if (nname.endswith("SDA")):
              bus.R_s["sda"].add(refdes)
          else:
            if (nname.endswith("SCL")):
              if (bus.R_pu["scl"] is None):
                bus.R_pu["scl"] = refdes
                bus.voltage["scl"] = min(set(comp.netNames) - {nname})
              else:
                bus.fail("MULTIPLE pull-up res on SCL {}, {}".format(
                    bus.R_pu["scl"], refdes))
            if (nname.endswith("SDA")):
              if (bus.R_pu["sda"] is None):
                bus.R_pu["sda"] = refdes
                bus.voltage["sda"] = min(set(comp.netNames) - {nname})
              else:
                bus.fail("MULTIPLE pull-up res on SDA {}, {}".format(
                    bus.R_pu["sda"], refdes))
        else:
          bus.refdes.add(refdes)

  with open(fPath, "w") as fOut:
    csvOut = csv.writer(fOut, lineterminator="\n")
    csvOut.writerow(["BUS", "PULL-UP", "VOLTAGE", "REFDES"])
    for k, v in dictI2C.items():
      pull_up = set(v.R_pu.values())
      voltage = set(v.voltage.values())
      csvOut.writerow([k, pull_up, voltage, v.refdes])
  logger.info("{} written".format(fPath))

def mainApp():
  brdCfg = pyauto_hw.config.BoardConfig(cliArgs["brd"])
  if (cliArgs["list"]):
    brdCfg.showInfo()
    return

  t0 = datetime.datetime.now()

  if ("netlist" not in brdCfg.path.keys()):
    msg = "MISSING 'netlist' path"
    logger.error(msg)
    raise RuntimeError(msg)
  if (pyauto_base.misc.isEmptyString(brdCfg.path["netlist"])):
    msg = "MISSING 'netlist' path"
    logger.error(msg)
    raise RuntimeError(msg)
  if (os.path.isabs(brdCfg.path["netlist"])):
    path_ntl = brdCfg.path["netlist"]
  else:
    path_ntl = pyauto_base.fs.mkAbsolutePath(brdCfg.path["netlist"],
                                             rootPath=cliArgs["brd"])
  ntl = pyauto_hw.utils.readNetlist(path_ntl)

  if (pyauto_base.misc.isEmptyString(brdCfg.name)):
    path_folder = pyauto_base.fs.mkOutFolder()
  else:
    path_folder = pyauto_base.fs.mkOutFolder(os.path.join("data_out", brdCfg.name))

  _report_capacitors(ntl, os.path.join(path_folder, "capacitors.csv"))
  _report_resistors(ntl, os.path.join(path_folder, "resistors.csv"))
  _report_inductors(ntl, os.path.join(path_folder, "inductors.csv"))
  _report_ferrites(ntl, os.path.join(path_folder, "ferrites.csv"))
  _report_ICs(ntl, os.path.join(path_folder, "ic.csv"))

  _report_decoupling(ntl, os.path.join(path_folder, "decoupling_capacitors.csv"))
  _report_AC_coupling(ntl, os.path.join(path_folder, "ac_coupling.csv"))
  _report_I2C(ntl, os.path.join(path_folder, "i2c.csv"))

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = ""
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  parser.add_argument("brd", help="board config path")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  parser.add_argument("-l",
                      "--list",
                      action="store_true",
                      default=False,
                      help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
