#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

import math
import csv
import json

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc

class MTBFData(object):
  """
  :var fit: fit value
  :var std: stddev
  :var qual: quality factor
  :var elc: electrical stress curve index
  :var elp: electrical stress percentage
  :var thc: temperature stress curve index
  :var temp: temperature
  """

  def __init__(self):
    self.fit = None
    self.std = None
    self.qual = None
    self.elc = None
    self.elp = None
    self.thc = None
    self.temp = None

  def __str__(self):
    return "{}: {}".format(self.__class__.__name__, self.__dict__)

class MTBFStandard(object):

  def __init__(self):
    self.stdName = None
    self.partName = None
    self.defaultFit = {}
    self.runParams = {}

  def getReportName(self):
    return "{}.{}.m{}.e{}.t{}.env{}.mtbf".format(self.partName, self.stdName,
                                                 self.runParams["method"],
                                                 self.runParams["elStressPercent"],
                                                 self.runParams["tempOperating"],
                                                 self.runParams["environment"])

  def _getParameters_NicePrint(self):
    raise NotImplementedError

  def showParameters(self):
    res = self._getParameters_NicePrint()

    logger.info("{}".format(res[0]))
    for i in range(1, len(res)):
      logger.info("  {}".format(res[i]))

  def setDefaultValues(self, fPath):
    logger.info("-- set default values for {}".format(self.stdName))
    tmp = ""
    with open(fPath) as fIn:
      for ln in [t.strip("\n\r") for t in fIn.readlines()]:
        tmp += (ln + " ")
    self.defaultFit = json.loads(tmp)

  def processBOM(self, bom):
    """
    Combines BOM data and MTBF database data to get quality, stress,
    thermal and first-year factors for each device.

    :param bom: BOM object with valid MTBF data in paramsDB["mtbf"]
    :return: list of [<pn>, <pn_qty>, <fit>, <pi_Qi>, <pi_Si>, <pi_Ti>, <pi_FYi>]
    """
    raise NotImplementedError

  def mtbf(self, mtbfData):
    """
    :param mtbfData: as returned by processBOM()
    :return: list of csv report lines
    """
    raise NotImplementedError

  def showSummary(self):
    raise NotImplementedError

  def _writeCsvReport_Header(self, csvOut):
    res = self._getParameters_NicePrint()

    csvOut.writerow(["Generated on: {}".format(pyauto_base.misc.getTimestamp())])
    for ln in res:
      csvOut.writerow([ln])

  def _writeCsvReport_Footer(self, csvOut):
    raise NotImplementedError

  def writeCSVReport(self, mtbfData):
    fOutName = self.getReportName() + ".csv"
    fOutPath = os.path.join(pyauto_base.fs.mkOutFolder(), fOutName)
    with open(fOutPath, "w") as fOut:
      csvOut = csv.writer(fOut, lineterminator="\n")
      self._writeCsvReport_Header(csvOut)
      for row in mtbfData:
        csvOut.writerow(row)

      self._writeCsvReport_Footer(csvOut)
    logger.info("{} written".format(fOutPath))

class MTBF_TR332(MTBFStandard):

  def __init__(self):
    super(MTBF_TR332, self).__init__()
    self.stdName = "TR-332"
    #environment factor (table 11-8)
    self.stdEnv = {
        "GB": 1.0,
        "GF": 2.0,
        "GM": 6.0,
        "AC": 10.0,
        "SC": 15.0,
    }
    #quality factor (table 11-4)
    self.stdQual = {
        "0": 6.0,
        "1": 3.0,
        "2": 1.0,
        "3": 0.9,
    }
    #electrical stress factor (table 11-6)
    self.stdEl = {
        "A": 0.6,
        "B": 0.9,
        "C": 1.3,
        "D": 1.9,
        "E": 2.4,
        "F": 2.9,
        "G": 3.5,
        "H": 4.1,
        "I": 4.6,
        "J": 5.9,
        "K": 0.6,
    }
    #temperature factor (table 11-7)
    self.stdTemp = {
        "1": 0.05,
        "2": 0.1,
        "3": 0.15,
        "4": 0.22,
        "5": 0.28,
        "6": 0.35,
        "7": 0.4,
        "8": 0.45,
        "9": 0.56,
        "10": 0.7,
    }
    lclDir = os.path.dirname(os.path.realpath(__file__))
    self.setDefaultValues(os.path.join(lclDir, "{}.default_fit.cfg".format(self.stdName)))

    # run values
    self.showDNS = True  # prints DNS parts
    self.partTotal = 0  # total number of parts (from BOM)
    self.partDNS = 0  # total number of DNS parts (from BOM)
    self.lambda_SS = -1.0
    self.pi_FY = -1.0

  def _getParameters_NicePrint(self):
    res = [
        "reliability standard: {}".format(self.stdName),
        "method {} - case {}".format(self.runParams["method"], self.runParams["case"]),
        "electrical stress: {}%".format(self.runParams["elStressPercent"]),
        "operating temperature: {}C".format(self.runParams["tempOperating"]),
        "environment: {}".format(self.runParams["environment"])
    ]

    return res

  def processBOM(self, bom):
    mtbfData = []

    dictParts = bom.getAllPartsByPN()
    if (len(dictParts.keys()) < 2):
      dictParts = bom.getAllPartsByValue()
    if (len(dictParts.keys()) < 2):
      msg = "WRONG netlist: no PNs, or values"
      logger.error(msg)
      raise RuntimeError(msg)

    for k in dictParts.keys():
      for p in dictParts[k]:
        #logger.info([p.getId(), str(p.paramsDB["mtbf"])])
        if (p.paramsDB["mtbf"] is None):
          logger.warning("part {} has NO MTBF information".format(p.id))
          continue
        if (p.dnp):
          if (self.showDNS):
            logger.info("ignoring DNS part {}*{}".format(p.qty, p.id))
          continue

        # quality factor
        pi_Qi = self.stdQual[self.runParams["qualityLvl"]]  # default
        if (p.paramsDB["mtbf"].qual in self.stdQual):
          pi_Qi = self.stdQual[p.paramsDB["mtbf"].qual]

        # stress factor
        elCurve = self.stdEl[self.runParams["elStressCurve"]]  # default
        elPercentage = float(self.runParams["elStressPercent"]) / 100.0  # default
        if (p.paramsDB["mtbf"].elc in self.stdEl):
          elCurve = self.stdEl[p.paramsDB["mtbf"].elc]
        if (p.paramsDB["mtbf"].elp is not None):
          elPercentage = float(p.paramsDB["mtbf"].elp) / 100.0
        pi_Si = math.exp(elCurve * (elPercentage - 0.5))  # page 11-25
        if (elCurve == 0.6):  # curve K
          if (pi_Si < 1.0):
            pi_Si = 1.0
        pi_Si = round(pi_Si, 1)

        # temperature factor
        tempCurve = self.stdTemp[self.runParams["tempStressCurve"]]
        tempOp = float(self.runParams["tempOperating"])
        if (p.paramsDB["mtbf"].thc in self.stdTemp):
          tempCurve = self.stdTemp[p.paramsDB["mtbf"].thc]
        if (p.paramsDB["mtbf"].temp is not None):
          tempOp = float(p.paramsDB["mtbf"].temp)
        # page 11-27
        pi_Ti = math.exp((tempCurve / 8.62e-5) * ((1 / 313.0) - (1 / (tempOp + 273.0))))
        # curve 1; not clear from standard but it seems correct from Table 11-7
        if (tempCurve == 0.05):
          if (pi_Ti < 1.0):
            pi_Ti = 1.0
        pi_Ti = round(pi_Ti, 1)

        # first-year multiplier
        case = int(self.runParams["case"])
        if (case == 1):
          pi_FYi = 4.0
        else:
          msg = "UNKNOWN case"  # TODO:
          logger.error(msg)
          raise RuntimeError(msg)

        mtbfData.append([p.id, p.qty, p.paramsDB["mtbf"].fit, pi_Qi, pi_Si, pi_Ti, pi_FYi])

    #for t in mtbfData:
    #  logger.info(t)
    self.partTotal = bom.nRefdes
    self.partDNS = bom.nRefdes_DNP
    return mtbfData

  def _mtbf_Method1(self, mtbfData):
    pi_E = self.stdEnv[self.runParams["environment"]]
    self.showParameters()
    logger.info("MTBF for part {}".format(self.partName))

    res = [[
        "PN/VALUE", "qty", "failure rate", "quality factor", "electrical stress",
        "temp stress", "FY factor", "total failure rate", "total FY factor"
    ]]
    lambda_SS = 0.0
    pi_FY = 0.0

    for p in mtbfData:
      #logger.info(p)
      [pn, Ni, lambda_Gi, pi_Qi, pi_Si, pi_Ti, pi_FYi] = p
      self.partTotal += Ni
      lambda_SSi = lambda_Gi * pi_Qi * pi_Si * pi_Ti  # eq. 6-1
      #logger.info("device steady-state failure rate: {:f}".format(lambda_SSi))
      res.append([
          pn, Ni, lambda_Gi, pi_Qi, pi_Si, pi_Ti, pi_FYi, (Ni * lambda_SSi),
          (Ni * lambda_SSi * pi_FYi)
      ])
      lambda_SS += (Ni * lambda_SSi)  # eq. 6-3
      pi_FY += (Ni * lambda_SSi * pi_FYi)

    res.append(["", "", "", "", "", "", "", lambda_SS, pi_FY, "FIT TOTAL"])

    pi_FY = pi_FY / lambda_SS  # section 6.3.3
    lambda_SS *= pi_E  # eq. 6-3

    res.append(["", "", "", "", "", "", "", "", pi_FY, "EARLY LIFE FACTOR"])
    res.append(["", "", "", "", "", "", "", lambda_SS, "", "FIT TOTAL (env)"])

    self.lambda_SS = lambda_SS
    self.pi_FY = pi_FY

    if (self.partDNS > 0):
      logger.info("{:d} + {:d} DNS = {:d} total parts".format(
          self.partTotal, self.partDNS, (self.partTotal + self.partDNS)))
    #for t in res:
    #  logger.info(t)
    return res

  def mtbf(self, mtbfData):
    if (self.runParams["method"] == '1'):
      return self._mtbf_Method1(mtbfData)
    else:
      msg = "UNKNOWN method"  # TODO:
      logger.error(msg)
      raise RuntimeError(msg)

  def showSummary(self):
    logger.info("unit steady-state failure rate: {:.2f}".format(self.lambda_SS))
    logger.info("unit early life factor {:.2f}".format(self.pi_FY))

    mtbf_h = 1.0e9 / self.lambda_SS
    mtbf_y = mtbf_h / (24.0 * 365.0)
    logger.info("MTBF: {:.2f} hrs".format(mtbf_h))
    logger.info("MTBF: {:.2f} yrs".format(mtbf_y))

  def _writeCsvReport_Footer(self, csvOut):
    mtbf_h = 1.0e9 / self.lambda_SS
    mtbf_y = mtbf_h / (24.0 * 365.0)
    csvOut.writerow(["", "", "", "", "", "", "", mtbf_h, "", "MTBF (hours)"])
    csvOut.writerow(["", "", "", "", "", "", "", mtbf_y, "", "MTBF (years)"])

class MTBF_SR332(MTBFStandard):

  def __init__(self):
    super(MTBF_SR332, self).__init__()
    self.stdName = "SR-332"
    #environment factor (table 9-5)
    self.stdEnv = {
        "GB": 1.0,
        "GL": 1.5,
        "GF": 2.0,
        "GM": 4.0,
        "AC": 6.0,
        "SC": 15.0,
    }
    #quality factor (table 9-4)
    self.stdQual = {
        "0": 6.0,
        "1": 3.0,
        "2": 1.0,
        "3": 0.8,
    }
    #electrical stress factor (table 9-2)
    self.stdEl = {
        "A": 0.6,
        "B": 0.9,
        "C": 1.3,
        "D": 1.9,
        "E": 2.4,
        "F": 2.9,
        "G": 3.5,
        "H": 4.1,
        "I": 4.6,
        "J": 5.9,
        "K": 0.6,
    }
    #temperature factor (table 9-1)
    self.stdTemp = {
        "1": 0.05,
        "2": 0.1,
        "3": 0.15,
        "4": 0.22,
        "5": 0.28,
        "6": 0.35,
        "7": 0.4,
        "8": 0.45,
        "9": 0.56,
        "10": 0.7,
    }
    lclDir = os.path.dirname(os.path.realpath(__file__))
    self.setDefaultValues(os.path.join(lclDir, "{}.default_fit.cfg".format(self.stdName)))

    # run values
    self.showDNS = True  # prints DNS parts
    self.partTotal = 0  # total number of parts (from BOM)
    self.partDNS = 0  # total number of DNS parts (from BOM)
    self.lambda_SS = -1.0
    self.sigma_SS = -1.0
    self.pi_EL = -1.0
    self.UCL = -1.0

  def _getParameters_NicePrint(self):
    res = [
        "reliability standard: {}".format(self.stdName),
        "method {}".format(self.runParams["method"]),
        "electrical stress: {}%".format(self.runParams["elStressPercent"]),
        "operating temperature: {}C".format(self.runParams["tempOperating"]),
        "environment: {}".format(self.runParams["environment"])
    ]

    return res

  def processBOM(self, bom):
    mtbfData = []

    dictParts = bom.getAllPartsByPN()
    if (len(dictParts.keys()) < 2):
      dictParts = bom.getAllPartsByValue()
    if (len(dictParts.keys()) < 2):
      msg = "WRONG netlist: no PNs, or values"
      logger.error(msg)
      raise RuntimeError(msg)

    for k in dictParts.keys():
      for p in dictParts[k]:
        #logger.info([p.getId(), str(p.paramsDB["mtbf"])])
        if (p.paramsDB["mtbf"] is None):
          logger.warning("part {} has NO MTBF information".format(p.id))
          continue
        if (p.dnp):
          if (self.showDNS):
            logger.info("ignoring DNS part {}*{}".format(p.qty, p.id))
          continue

        # quality factor
        pi_Qi = self.stdQual[self.runParams["qualityLvl"]]  # default
        if (p.paramsDB["mtbf"].qual in self.stdQual):
          pi_Qi = self.stdQual[p.paramsDB["mtbf"].qual]

        # stress factor
        elCurve = self.stdEl[self.runParams["elStressCurve"]]  # default
        elPercentage = float(self.runParams["elStressPercent"]) / 100.0  # default
        if (p.paramsDB["mtbf"].elc in self.stdEl):
          elCurve = self.stdEl[p.paramsDB["mtbf"].elc]
        if (p.paramsDB["mtbf"].elp is not None):
          elPercentage = float(p.paramsDB["mtbf"].elp) / 100.0
        pi_Si = math.exp(elCurve * (elPercentage - 0.5))  # eq. 9-2
        if (elCurve == 0.6):  # curve K
          if (pi_Si < 1.0):
            pi_Si = 1.0
        pi_Si = round(pi_Si, 1)

        # temperature factor
        tempCurve = self.stdTemp[self.runParams["tempStressCurve"]]
        tempOp = float(self.runParams["tempOperating"])
        if (p.paramsDB["mtbf"].thc in self.stdTemp):
          tempCurve = self.stdTemp[p.paramsDB["mtbf"].thc]
        if (p.paramsDB["mtbf"].temp is not None):
          tempOp = float(p.paramsDB["mtbf"].temp)
        # eq. 9-1
        pi_Ti = math.exp((tempCurve / 8.62e-5) * ((1 / 313.0) - (1 / (tempOp + 273.0))))
        pi_Ti = round(pi_Ti, 1)

        # first-year multiplier
        if ((pi_Si * pi_Ti) < 1.14):
          pi_ELi = (4.0 / math.pow(pi_Si * pi_Ti, 0.75))  # eq. 4-1
        else:
          pi_ELi = (1.0 + 3.0 / (pi_Si * pi_Ti))  # eq. 4-2
        pi_ELi = round(pi_ELi, 2)

        mtbfData.append([
            p.id, p.qty, p.paramsDB["mtbf"].fit, p.paramsDB["mtbf"].std, pi_Qi, pi_Si,
            pi_Ti, pi_ELi
        ])

    #for t in mtbfData:
    #  logger.info(t)
    return mtbfData

  def _mtbf_Method1(self, mtbfData):
    pi_E = self.stdEnv[self.runParams["environment"]]
    self.showParameters()
    logger.info("MTBF for part {}".format(self.partName))

    res = [[
        "PN/VALUE", "qty", "failure rate", "stddev", "quality factor", "electrical stress",
        "temp stress", "EL factor", "total failure rate", "total stddev", "total EL factor"
    ]]
    lambda_PC = 0.0
    sigma_PC = 0.0
    pi_EL = 0.0

    for p in mtbfData:
      #logger.info(p)
      [pn, Ni, lambda_Gi, sigma_Gi, pi_Qi, pi_Si, pi_Ti, pi_ELi] = p
      #lambda_Gi = dictMTBFdb[pn]["fit"]
      #sigma_Gi = dictMTBFdb[pn]["std"]
      #logger.info("{}*{} {}".format(Ni, pn, [lambda_Gi, sigma_Gi, pi_Qi, pi_Si, pi_Ti]))
      lambda_BBi = lambda_Gi * pi_Qi * pi_Si * pi_Ti  # eq. 3-1
      sigma_BBi = sigma_Gi * pi_Qi * pi_Si * pi_Ti  # eq. 3-2
      Li = (Ni * lambda_BBi)  # eq. 5-3
      Ci = (Ni * sigma_BBi)  # eq. 5-3

      #logger.info("device steady-state failure rate: {:f}+/-{:f}".format(lambda_BBi, sigma_BBi))
      res.append(
          [pn, Ni, lambda_Gi, sigma_Gi, pi_Qi, pi_Si, pi_Ti, pi_ELi, Li, Ci, (Li * pi_ELi)])
      lambda_PC += Li  # eq. 5-1
      sigma_PC += (Ci * Ci)  # eq. 5-2
      pi_EL += (Li * pi_ELi)  # eq. 5-13

    res.append(["", "", "", "", "", "", "", "", lambda_PC, "", pi_EL, "FIT TOTAL"])

    pi_EL = pi_EL / lambda_PC  # eq. 5-13, done here to avoid multiplication and division by pi_E
    lambda_PC = round(pi_E * lambda_PC, 2)  # eq. 5-1
    sigma_PC = pi_E * math.sqrt(sigma_PC)  # eq. 5-2
    sigma_PC = round(sigma_PC, 2)

    res.append(["", "", "", "", "", "", "", "", "", sigma_PC, "", "FIT STDDEV (rms)"])
    res.append(["", "", "", "", "", "", "", "", "", "", pi_EL, "EARLY LIFE FACTOR"])
    res.append(["", "", "", "", "", "", "", "", lambda_PC, "", "", "FIT TOTAL (env)"])

    # TODO: calculate UCL (section 7.1)

    self.lambda_SS = lambda_PC
    self.sigma_SS = sigma_PC
    self.pi_EL = pi_EL

    if (self.partDNS > 0):
      logger.info("{:d} + {:d} DNS = {:d} total parts".format(
          self.partTotal, self.partDNS, (self.partTotal + self.partDNS)))
    #for t in res:
    #  logger.info(t)
    return res

  def mtbf(self, mtbfData):
    if (self.runParams["method"] == "I"):
      return self._mtbf_Method1(mtbfData)
    else:
      msg = "UNKNOWN method"  # TODO:
      logger.error(msg)
      raise RuntimeError(msg)

  def showSummary(self):
    logger.info("unit steady-state failure rate: {:.2f} +/- {:.2f}".format(
        self.lambda_SS, self.sigma_SS))
    logger.info("unit early life factor {:.2f}".format(self.pi_EL))

    mtbf_h = 1.0e9 / self.lambda_SS
    mtbf_y = mtbf_h / (24.0 * 365.0)
    logger.info("MTBF: {:.2f} hrs".format(mtbf_h))
    logger.info("MTBF: {:.2f} yrs".format(mtbf_y))

  def _writeCsvReport_Footer(self, fOut):
    mtbf_h = 1.0e9 / self.lambda_SS
    mtbf_y = mtbf_h / (24.0 * 365.0)
    fOut.writerow(["", "", "", "", "", "", "", "", mtbf_h, "", "", "MTBF (hours)"])
    fOut.writerow(["", "", "", "", "", "", "", "", mtbf_y, "", "", "MTBF (years)"])

dictStandards = {
    "TR-332": MTBF_TR332(),
    "SR-332": MTBF_SR332(),
}
