#!/usr/bin/env python
# template: plot
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app with plots"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

import math
#import csv
import argparse
import numpy
import scipy.special

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.plot

class FanParameters(object):

  def __init__(self):
    self.wb_shape = 3.0
    self.wb_scale = 0.0
    self.lifeId = "L10"
    self.life_exp = 0  # hours
    self.mtbfLimit = 1.0 - (1.0 / math.e)

fanParams = FanParameters()

def getDecade(n):
  res = 0

  if (n > 10.0):
    while True:
      n /= 10.0
      res += 1
      if (n < 10.0):
        break
  elif (n < 1.0):
    while True:
      n *= 10.0
      res -= 1
      if (n >= 1.0):
        break

  return [res, (res + 1)]

def _processData(plot):
  fanParams.life_exp = cliArgs["life"]
  logger.info("fan life expectancy: {:d}h".format(fanParams.life_exp))

  [e1, e2] = getDecade(fanParams.life_exp)
  t = numpy.arange((10**(e1 - 1)), (10**e1), (10**(e1 - 1)))
  for i in range(e1, (e2 + 2)):
    t = numpy.concatenate((t, numpy.arange((10**i), (10**(i + 1)), (10**i))))
  t = numpy.concatenate((t, [10**(e2 + 2)]))

  if (fanParams.lifeId == "L10"):
    lp = 0.9  # 1.0 - percentage of failures
    fanParams.wb_scale = fanParams.life_exp / numpy.exp(
        numpy.log(numpy.log(1.0 / lp)) / fanParams.wb_shape)
    MTTF_coef = scipy.special.gamma(1.0 + (1.0 / fanParams.wb_shape)) / numpy.power(
        numpy.log(1.0 / lp), (1.0 / fanParams.wb_shape))
    MTTF_IPC = fanParams.life_exp * MTTF_coef
  else:
    msg = "incorrect life expectancy"
    logger.error(msg)
    raise RuntimeError(msg)

  logger.info("Weibull parameters: [{}, {}]".format(fanParams.wb_scale, fanParams.wb_shape))
  cdf = 1.0 - numpy.exp(-1.0 * ((t / fanParams.wb_scale)**fanParams.wb_shape))
  '''
  http://www.reliabilityanalytics.com/blog/2012/06/14/estimating-average-failure-rate-based-on-l10-life/
  '''
  logger.info("== MTBF estimate as Weibull characteristic life")
  MTBF = fanParams.wb_scale
  #logger.info("MTBF limit: {}".format(fanParams.mtbfLimit))
  logger.info("MTBF: {:.0f}h".format(MTBF))
  logger.info("FIT: {:.0f}".format(1.0e9 / MTBF))

  logger.info("== MTTF estimate (IPC-9591 or eq.)")
  logger.info("MTTF: {:.0f}h".format(MTTF_IPC))
  logger.info("FIT: {:.0f}".format(1.0e9 / MTTF_IPC))

  plot.addData(t, cdf, xunit="h", legend="CDF")
  plot.addHline(fanParams.mtbfLimit, legend="MTBF limit")
  plot.addVline(fanParams.life_exp, legend="life expectancy")

def mainApp():
  plot = pyauto_base.plot.SimplePlot()

  _processData(plot)

  plot.title = "Weibull CDF (shape = {:.2f})".format(fanParams.wb_shape)
  plot.xlabel = cliArgs["xlbl"]
  plot.ylabel = cliArgs["ylbl"]

  plot.legend = True
  plot.legend_loc = cliArgs["legend"]
  plot.ax_minticks = cliArgs["minticks"]
  plot.ax_grid = cliArgs["grid"]
  plot.ax_scale = cliArgs["scale"]

  if (cliArgs["save"]):
    fOutPath = pyauto_base.fs.mkOutFolder()
    title = plot.title
    plot.fPath = os.path.join(fOutPath, "{}.png".format(title))
    plot.write(xtnd_x=cliArgs["xtend"])
  else:
    plot.show()

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "fan MTBF plot"
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("--hline", help="add horizontal lines")
  parser.add_argument("--vline", help="add vertical lines")
  parser.add_argument("--minticks",
                      action="store_true",
                      default=False,
                      help="show minor ticks")
  parser.add_argument("--grid",
                      choices=("no", "min", "maj", "both"),
                      default="maj",
                      help="change grid")
  parser.add_argument("--legend",
                      choices=("best", "ur", "cr", "lr", "ul", "cl", "ll"),
                      default="best",
                      help="legend location")
  parser.add_argument("--scale",
                      choices=("log_x", "log_y", "log_both"),
                      default="log_both",
                      help="axis scale")
  parser.add_argument("--xlbl", default="time", help="label for x axis")
  parser.add_argument("--ylbl", default="probability", help="label for y axis")
  parser.add_argument("--xtend",
                      type=int,
                      default=1,
                      help="when saving make image n times longer")
  parser.add_argument("-s",
                      "--save",
                      action="store_true",
                      default=False,
                      help="save plot as image")
  parser.add_argument("--life", type=int, default=80000, help="fan life expectancy")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
