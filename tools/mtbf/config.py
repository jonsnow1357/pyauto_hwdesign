#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
import csv

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.config
import pyauto_base.db
import pyauto_hwdesign.tools.mtbf.libMTBF as libMTBF

class MTBFConfig(pyauto_base.config.DBConfig):

  def __init__(self):
    super(MTBFConfig, self).__init__()
    self.standards = {}
    self.showDNS = not True

  def clear(self):
    self.standards = {}
    self.showDNS = not True

  def parseCfg(self):
    super(MTBFConfig, self).parseCfg()

    for s in self._cfgObj.sections():
      if (s.startswith("standard_")):
        sName = s[9:]
        self.standards[sName] = {}
        self.standards[sName]["name"] = sName
        for k, v in self._cfgObj.items(s):
          if (k.startswith("param_")):
            self.standards[sName][k[6:]] = v

  def showInfo(self):
    super(MTBFConfig, self).showInfo()

    for k in self.standards.keys():
      logger.info("{}: {}".format(k, self.standards[k]))

def chkPartData(dictPart, stdName):
  #logger.info(dictPart)
  if (("PN" not in dictPart.keys()) and ("VALUE" not in dictPart.keys())):
    logger.error("PN / VALUE missing from database")
    return False
  elif (("PN" in dictPart.keys()) and ("VALUE" in dictPart.keys())):
    logger.error("PN and VALUE present in database")
    return False

  std = libMTBF.dictStandards[stdName]
  k = "{}_TYPE".format(std.stdName)
  if (k not in dictPart.keys()):
    logger.error("{} missing from database".format(k))
    return False
  pt = dictPart[k]
  if (pt not in std.defaultFit.keys()):
    logger.error("type {} NOT DEFINED for {}".format(pt, std.stdName))
    return False

  k = "{}_FIT".format(std.stdName)
  if (k not in dictPart.keys()):
    logger.error("{} missing from database".format(k))
    return False
  pfit = dictPart[k]
  try:
    pfit = float(pfit)
  except ValueError:
    if (pfit != "-"):
      logger.error("INCORRECT {} value: {}".format(k, dictPart))
      return False
  k = "{}_STDDEV".format(std.stdName)
  if (k not in dictPart.keys()):
    logger.error("{} missing from database".format(k))
    return False
  psd = dictPart[k]
  try:
    psd = float(psd)
  except ValueError:
    if (psd != "-"):
      logger.error("INCORRECT {} value: {}".format(k, dictPart))
      return False

  k = "{}_QUAL".format(std.stdName)
  if (k in dictPart.keys()):
    pqual = dictPart[k]
    try:
      pqual = int(pqual)
    except ValueError:
      if (dictPart[k] != "-"):
        logger.error("INCORRECT {} value: {}".format(k, dictPart))
        return False

  if (pfit == "-"):
    if (psd != "-"):
      logger.error("INCORRECT STDDEV {} for FIT {}: {}".format(psd, pfit, dictPart))
      return False
  else:
    dfit = std.defaultFit[pt]["FIT"]
    dsd = std.defaultFit[pt]["STDDEV"]
    if ((pfit != dfit) or (psd != dsd)):
      if (dictPart["COMMENT"] == ""):
        logger.warning("FIT and STDDEDV different than defaults: {}".format(
            [pfit, psd, dfit, dsd]))
        logger.warning(dictPart)
        return False

  return True

class MTBF_DBController(pyauto_base.db.DBController):
  lstTables = ["electrical", "mechanical", "pcba", "misc"]

  def __init__(self):
    super(MTBF_DBController, self).__init__()

    self.bIgnoreFans = False
    if (self.bIgnoreFans):
      logger.info("Fan FIT will be ignored")

  def _getCSVFolder(self):
    csvFolder = self.dbInfo.dbname
    if (csvFolder.startswith("..")):
      lclDir = sys.path[0]
      csvFolder = os.path.join(lclDir, csvFolder)
    pyauto_base.fs.chkPath_Dir(csvFolder)
    return csvFolder

  def _getCSVFileData(self, fPath, stdName):
    lstRes = pyauto_base.misc.readCSV_asRows(fPath)
    dictMTBF = {}  # dictionary of {<part_id>: MTBFData()}
    std = libMTBF.dictStandards[stdName]

    for part in lstRes:
      #logger.info(part)
      if (not chkPartData(part, stdName)):
        msg = "INCORRECT file: {}, line: {}".format(fPath, part)
        logger.error(msg)
        raise RuntimeError(msg)
      mtbf = libMTBF.MTBFData()
      pid = None
      if ("PN" in part.keys()):
        pid = part["PN"]
      elif ("VALUE" in part.keys()):
        pid = part["VALUE"]
      if (pyauto_base.misc.isEmptyString(pid)):
        logger.error("EMPTY PN / VALUE (file {})".format(fPath))
        continue

      if (self.bIgnoreFans and (part["{}_TYPE".format(stdName)] == "FAN")):
        mtbf.fit = 0.0
      else:
        tmp = part["{}_FIT".format(stdName)]
        if (tmp == "-"):
          mtbf.fit = std.defaultFit[part["{}_TYPE".format(stdName)]]["FIT"]
        else:
          mtbf.fit = float(tmp)
      tmp = part["{}_STDDEV".format(stdName)]
      if (tmp == "-"):
        mtbf.std = std.defaultFit[part["{}_TYPE".format(stdName)]]["STDDEV"]
      else:
        mtbf.std = float(tmp)

      if ("{}_QUAL".format(stdName) in part.keys()):
        mtbf.qual = part["{}_QUAL".format(stdName)]
      if ("{}_ELC".format(stdName) in part.keys()):
        mtbf.elc = part["{}_ELC".format(stdName)]
      if ("{}_ELP".format(stdName) in part.keys()):
        mtbf.elp = part["{}_ELP".format(stdName)]
      if ("{}_THC".format(stdName) in part.keys()):
        mtbf.thc = part["{}_THC".format(stdName)]
      if ("{}_TEMP".format(stdName) in part.keys()):
        mtbf.temp = part["{}_TEMP".format(stdName)]
      if (pid in dictMTBF.keys()):
        logger.error("duplicate part found in database: {} (file {})".format(pid, fPath))
        continue
      #logger.info([pid, str(mtbf)])
      dictMTBF[pid] = mtbf

    return dictMTBF

  def getData(self, stdName, tblId):
    logger.info("-- reading MTBF database [{}, table {}]".format(stdName, tblId))

    if (self.backend == pyauto_base.db.dbBackendCsv):
      csvFolder = self._getCSVFolder()
      csvFile = "{}.csv".format(tblId)
      fPath = os.path.join(csvFolder, csvFile)
      dictMTBF = self._getCSVFileData(fPath, stdName)
    else:
      msg = "INCORRECT database: {}".format(self.dbInfo)
      logger.error(msg)
      raise RuntimeError(msg)

    logger.info("-- done reading {:d} part(s)".format(len(dictMTBF)))
    return dictMTBF

  def getAllData(self, stdName):
    logger.info("-- reading MTBF database [{}]".format(stdName))
    dictMTBF = {}  # dictionary of {<part_id>: MTBFData()}

    for tblId in self.lstTables:
      res = self.getData(stdName, tblId)
      for k, v in res.items():
        if (k in dictMTBF.keys()):
          msg = "DUPLICATE part: {} in {}".format(k, tblId)
          logger.error(msg)
          raise RuntimeError(msg)
        dictMTBF[k] = v

    logger.info("-- done reading {:d} part(s)".format(len(dictMTBF)))
    return dictMTBF

  def _updateCSVFile(self, fit, stddev, stdName, dbKey, fPath):
    with open(fPath, "r") as fIn:
      csvIn = csv.reader(fIn)
      csvContent = []
      csvUpdate = False
      colIdxFit = -1
      colIdxStd = -1
      for row in csvIn:
        if (csvIn.line_num == 1):
          colIdxFit = row.index("{}_FIT".format(stdName))
          colIdxStd = row.index("{}_STDDEV".format(stdName))
        if (dbKey == row[0]):
          #logger.info(row)
          row[colIdxFit] = str(fit)
          row[colIdxStd] = str(stddev)
          #logger.info(row)
          csvUpdate = True
        csvContent.append(row)

    if (csvUpdate):
      with open(fPath, "w") as fOut:
        csvOut = csv.writer(fOut, lineterminator="\n")
        for row in csvContent:
          csvOut.writerow(row)

  def updateFitData(self, fit, stddev, stdName, dbKey):
    if (self.backend == pyauto_base.db.dbBackendCsv):
      folder = self._getCSVFolder()
      fPath = os.path.join(folder, "pcba.csv")
      self._updateCSVFile(fit, stddev, stdName, dbKey, fPath)
      folder = self._getCSVFolder()
      fPath = os.path.join(folder, "misc.csv")
      self._updateCSVFile(fit, stddev, stdName, dbKey, fPath)
