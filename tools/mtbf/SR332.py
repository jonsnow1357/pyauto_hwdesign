#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_hw.CAD.base as CADbase
import pyauto_hwdesign.tools.mtbf.config
import pyauto_hwdesign.tools.mtbf.libMTBF as libMTBF

appCfg = pyauto_hwdesign.tools.mtbf.config.MTBFConfig()
appCtl = pyauto_hwdesign.tools.mtbf.config.MTBF_DBController()

def mainApp():
  logger.info("using config file: {}".format(os.path.realpath(cliArgs["cfg"])))
  appCfg.loadCfgFile(cliArgs["cfg"])
  if (cliArgs["list"]):
    appCfg.showInfo()
    return

  mtbfStd = libMTBF.MTBF_SR332()
  mtbfStd.runParams = appCfg.standards[mtbfStd.stdName]
  #logger.info(mtbfStd.runParams)
  if (mtbfStd.stdName != mtbfStd.runParams["name"]):
    msg = "WRONG standard name in config file: {} - {}".format(mtbfStd.runParams["name"],
                                                               mtbfStd.stdName)
    logger.error(msg)
    raise RuntimeError(msg)

  # read parts database
  appCtl.dbInfo = appCfg.dictDBInfo["0"]
  dictMTBFdb = appCtl.getAllData(mtbfStd.stdName)
  if ((dictMTBFdb is None) or (len(dictMTBFdb) == 0)):
    msg = "EMPTY database: {}".format(appCfg.dictDBInfo["0"])
    logger.error(msg)
    raise RuntimeError(msg)
  if (cliArgs["db"]):
    return

  # read BOM
  bom = CADbase.BOM()
  if (cliArgs["BOM"].endswith("epicor.tracker.csv")):
    bom.readCSV(cliArgs["BOM"], CADbase.bomFileEpicor)
    mtbfStd.partName = os.path.basename(cliArgs["BOM"])[:-19]
  elif (cliArgs["BOM"].endswith("rpt")):
    bom.readCSV(cliArgs["BOM"], CADbase.bomFileHDL)
    mtbfStd.partName = os.path.basename(cliArgs["BOM"])[:-4]
  elif (cliArgs["BOM"].endswith("dx.csv")):
    bom.readCSV(cliArgs["BOM"], CADbase.bomFileDX)
    mtbfStd.partName = os.path.basename(cliArgs["BOM"])[:-4]
  else:
    msg = "UNKNOWN BOM: {}".format(cliArgs["BOM"])
    logger.error(msg)
    raise RuntimeError(msg)
  if (bom.nRefdes == 0):
    msg = "EMPTY BOM: {}".format(cliArgs["BOM"])
    logger.error(msg)
    raise RuntimeError(msg)

  # add parts database information to BOM
  dictPartsByPN = bom.getAllPartsByPN()
  for pn in dictPartsByPN.keys():
    for p in dictPartsByPN[pn]:
      p.paramsDB["mtbf"] = None
      if ((p.pn != "") and (p.pn in dictMTBFdb.keys())):
        p.paramsDB["mtbf"] = dictMTBFdb[p.pn]
      if ((p.value != "") and (p.value in dictMTBFdb.keys())):
        p.paramsDB["mtbf"] = dictMTBFdb[p.value]
      if (p.paramsDB["mtbf"] is None):
        logger.warning("part {} not in database".format(p.id))
        continue
      #logger.info([p.getId(), str(p.paramsDB["mtbf"])])
  if (cliArgs["bom"]):
    return

  mtbfData = mtbfStd.mtbf(mtbfStd.processBOM(bom))
  mtbfStd.showSummary()
  mtbfStd.writeCSVReport(mtbfData)

  if (mtbfStd.partName in dictMTBFdb.keys()):
    fit = round(mtbfStd.lambda_SS, 2)
    std = round(mtbfStd.sigma_SS, 2)
    if ((fit != dictMTBFdb[mtbfStd.partName]["fit"])
        or (std != dictMTBFdb[mtbfStd.partName]["std"])):
      logger.warning("FIT value for {} will be updated".format(mtbfStd.partName))
      appCtl.updateFitData(fit, std, mtbfStd.stdName, mtbfStd.partName)
  else:
    logger.warning("{} not in database".format(mtbfStd.partName))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "reliability prediction for a board"
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  parser.add_argument("BOM", help="BOM file")
  parser.add_argument("-f", "--cfg", default=appCfgPath, help="configuration file path")
  parser.add_argument("-l",
                      "--list",
                      action="store_true",
                      default=False,
                      help="list config file options")
  parser.add_argument("-d",
                      "--db",
                      action="store_true",
                      default=False,
                      help="stop after loading database")
  parser.add_argument("-b",
                      "--bom",
                      action="store_true",
                      default=False,
                      help="stop after BOM parsing")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
