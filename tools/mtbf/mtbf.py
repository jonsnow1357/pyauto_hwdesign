#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse
import copy

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")

def mainApp():
  if (cliArgs["action"] in ("all", "TR-332")):
    import pyauto_hwdesign.tools.mtbf.TR332
    tmp = copy.deepcopy(cliArgs)
    tmp["list"] = False
    tmp["db"] = False
    tmp["bom"] = False
    pyauto_hwdesign.tools.mtbf.TR332.cliArgs = tmp
    pyauto_hwdesign.tools.mtbf.TR332.logger = logger
    pyauto_hwdesign.tools.mtbf.TR332.mainApp()

  if (cliArgs["action"] in ("all", "SR-332")):
    import pyauto_hwdesign.tools.mtbf.SR332
    tmp = copy.deepcopy(cliArgs)
    tmp["list"] = False
    tmp["db"] = False
    tmp["bom"] = False
    pyauto_hwdesign.tools.mtbf.SR332.cliArgs = tmp
    pyauto_hwdesign.tools.mtbf.SR332.logger = logger
    pyauto_hwdesign.tools.mtbf.SR332.mainApp()

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "reliability prediction for a board"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("action", help="action", choices=("TR-332", "SR-332", "all"))
  parser.add_argument("BOM", help="BOM file (*.csv)")
  parser.add_argument("-f", "--cfg", default=appCfgPath, help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
