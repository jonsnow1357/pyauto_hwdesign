#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import shutil

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc

strTypeExport = "export"
strTypeMulti = "multi"
strTypeFOTF = "fotf"
lstTypes = (strTypeExport, strTypeMulti, strTypeFOTF)

strExport_Txt = "txt"
strExport_H = "h"
strFOTF_N2 = "N2"
strFOTF_N3 = "N3"
lstFiles_FOTF = (strFOTF_N2, strFOTF_N3)

_bin_path = "C:\\Program Files (x86)\\Silicon Laboratories\\ClockBuilder Pro\\Bin"
_REGS_exe = os.path.join(_bin_path, "CBProProjectRegistersExport.exe")
_MULTI_exe = os.path.join(_bin_path, "CBProMultiEditAndExport.exe")
_FOTF_exe = os.path.join(_bin_path, "CBProFOTF1.exe")

def getExecPath():
  return {strTypeExport: _REGS_exe, strTypeMulti: _MULTI_exe, strTypeFOTF: _FOTF_exe}

pyauto_base.fs.chkPath_File(_REGS_exe)
pyauto_base.fs.chkPath_File(_MULTI_exe)
pyauto_base.fs.chkPath_File(_FOTF_exe)

def _cp(src, dst):
  pyauto_base.fs.chkPath_File(src)
  #print("CP ", src, dst)
  shutil.copy(src, dst)

def _cp_append(lstSrc, dst):
  for src in lstSrc:
    pyauto_base.fs.chkPath_File(src)
    #print("CPa", src, dst)
    with open(dst, "a") as fOut:
      with open(src, "r") as fIn:
        fOut.write(fIn.read())

class CBProject(object):

  def __init__(self, strType, strPath):
    self.type = strType
    pyauto_base.fs.chkPath_File(strPath)
    self.dirPath = os.path.dirname(os.path.abspath(strPath))
    self.fName = os.path.basename(strPath)
    self.outFolder = None
    self._params = {}
    self.fotfPlan = None
    self.fotfType = None
    self.fotfName = []

  def __str__(self):
    return "{}: [{}, {}] {}".format(self.__class__.__name__, self._type, len(self._params),
                                    self.fName)

  @property
  def type(self):
    return self._type

  @type.setter
  def type(self, val):
    if (val not in lstTypes):
      msg = "INCORRECT {} type: {}".format(self.__class__.__name__, val)
      logger.error(msg)
      raise RuntimeError(msg)
    self._type = val

  def addOutputFile(self, strType):
    if (self.type == strTypeExport):
      if (strType == strExport_H):
        self._params[strExport_H] = os.path.basename(self.fName)[:-13] + ".h"
      elif (strType == strExport_Txt):
        self._params[strExport_Txt] = os.path.basename(self.fName)[:-13] + ".txt"
      else:
        raise NotImplementedError
    else:
      raise RuntimeError

  def addChangeFile(self, strPath):
    raise NotImplementedError

  def addFOTFFile(self, strType, strPath):
    if (self.type == strTypeFOTF):
      if (strType not in lstFiles_FOTF):
        raise RuntimeError
      if (os.path.isabs(strPath)):
        pyauto_base.fs.chkPath_File(strPath)
      else:
        pyauto_base.fs.chkPath_File(os.path.join(self.dirPath, strPath))
      self._params[strType] = strPath
      self.outFolder = self.fName[:-13]
    else:
      raise RuntimeError

  def _getScript_Export(self):
    lstLn = []
    for t, p in self._params.items():
      lstLn.append("DEL /Q /F \"{}\"".format(p))
      if (t == strExport_H):
        lstLn.append("\"{}\" --project \"{}\" --outfile \"{}\" "
                     "--format cheader --include-load-writes".format(
                         _REGS_exe, self.fName, p))
      elif (t == strExport_Txt):
        lstLn.append("\"{}\" --project \"{}\" --outfile \"{}\" "
                     "--format csv --include-load-writes".format(_REGS_exe, self.fName, p))
      else:
        raise NotImplementedError
    return lstLn

  def _getScript_Multi(self):
    lstLn = []
    return lstLn

  def _getScript_FOTF(self):
    lstLn = ["RMDIR /Q /S \"{}\"".format(self.outFolder)]
    cmd = ["\"{}\"".format(_FOTF_exe), "--project", "\"{}\"".format(self.fName)]
    for t, p in self._params.items():
      cmd.append("--plans-{}".format(t.lower()))
      cmd.append("\"{}\"".format(p))
    cmd += ["--out-folder \"{}\" --create-out-folder".format(self.outFolder)]
    lstLn.append(" ".join(cmd))
    return lstLn

  def getScript(self):
    if (self.type == strTypeExport):
      return self._getScript_Export()
    elif (self.type == strTypeMulti):
      raise NotImplementedError
    elif (self.type == strTypeFOTF):
      return self._getScript_FOTF()
    else:
      raise NotImplementedError

  def _copyFiles_Export(self, dstPath):
    for t, p in self._params.items():
      src = os.path.join(self.dirPath, p)
      dst = os.path.join(dstPath, os.path.basename(p))
      _cp(src, dst)
    logger.info("{}: {} file(s) copied".format(self.fName, len(self._params)))

  def _copyFiles_Multi(self, dstPath):
    raise NotImplementedError

  def _copyFiles_FOTF(self, dstPath):
    base_path = os.path.join(self.dirPath, self.outFolder)
    src = os.path.join(base_path, "Base-Plan-Register-Script.txt")
    dst = os.path.join(dstPath, self.fName[:-13] + ".txt")
    _cp(src, dst)

    lstFiles = os.listdir(base_path)
    dictPlanFiles = {}
    for f in lstFiles:
      tmp = re.split(r"(.*)-Register-Script-Plan(.*)\.csv", f)
      if (len(tmp) != 4):
        continue
      if (tmp[1] not in (lstFiles_FOTF)):
        raise RuntimeError
      k = "{:02d}".format(int(tmp[2]))
      if (k not in dictPlanFiles.keys()):
        dictPlanFiles[k] = [f]
      else:
        dictPlanFiles[k].append(f)
    #print("DBG", dictPlanFiles.keys())
    for k, lst in dictPlanFiles.items():
      if (len(lst) != len(self._params)):
        raise RuntimeError
      src = [os.path.join(base_path, f) for f in sorted(lst)]
      dst = os.path.join(dstPath, "{}_P{}.txt".format(self.fName[:-13], k))
      _cp_append(src, dst)
    logger.info("{}: {} file(s) copied".format(self.fName, len(dictPlanFiles) + 1))

  def copyFiles(self, dstPath):
    pyauto_base.fs.chkPath_Dir(dstPath)

    if (self.type == strTypeExport):
      self._copyFiles_Export(dstPath)
    elif (self.type == strTypeMulti):
      self._copyFiles_Multi(dstPath)
    elif (self.type == strTypeFOTF):
      self._copyFiles_FOTF(dstPath)
    else:
      raise NotImplementedError
