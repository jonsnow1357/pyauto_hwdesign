#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app with plots"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.plot
import pyauto_hw.config
import pyauto_hw.power as hwPwr

def _getPowerOnRampValues(time, pwrRail):
  res = len(time) * [0.0]
  for i in range(0, len(time)):
    if ((time[i] > pwrRail.t_0) and (time[i] < pwrRail.t_on)):
      slope = pwrRail.v / (pwrRail.t_on - pwrRail.t_0)
      res[i] = slope * (time[i] - pwrRail.t_0)
    elif (time[i] >= pwrRail.t_on):
      res[i] = pwrRail.v

  return res

def _filterPowerRail(prName):
  lst_filtered = cliArgs["flt"].split(",")
  if (prName in lst_filtered):
    return True
  return False

def _processData(plot, pwrTree):
  pwrTree.updateStart()

  # create time scale
  t = []
  for prName in pwrTree.getAllRailNames_preorder():
    rail = pwrTree.getRail(prName)
    t.append(rail.t_0)
    t.append(rail.t_on)

  t = list(set(t))
  if (min(t) > 0.0):
    t.append(0.0)
  if (min(t) == 0.0):
    t.append(-0.5)
  t.append(max(t) + 10.0)
  t = sorted(t)
  #print("DBG", t)

  for prName in pwrTree.getAllRailNames_preorder():
    rail = pwrTree.getRail(prName)
    if (_filterPowerRail(prName)):
      continue
    y = _getPowerOnRampValues(t, rail)
    plot.addData(t, y, xunit="ms", yunit="V", legend=rail.name)

def mainApp():
  brdCfg = pyauto_hw.config.BoardConfig(cliArgs["brd"])
  if (cliArgs["list"]):
    brdCfg.showInfo()
    return

  pwrTree = hwPwr.PowerTree(None)
  #pwrTree.modelPath = brdCfg.path["power"]
  for f in os.listdir(cliArgs["brd"]):
    if (not f.lower().endswith("pwr.seq.xml")):
      continue

    pwrTree.readXML(os.path.join(cliArgs["brd"], f))
    pwrTree.build()
    pwrTree.showTree()

    plot = pyauto_base.plot.SimplePlot()

    _processData(plot, pwrTree)
    if (cliArgs["noplot"]):
      for prName in pwrTree.getAllRailNames_preorder():
        rail = pwrTree.getRail(prName)
        logger.info("{: <20} {: >6} {: >6} {}".format(prName, rail.t_0, rail.t_on,
                                                      rail.startAfter))
      return

    if (pwrTree.name is not None):
      plotTitle = "{} power-on".format(pwrTree.name)
    else:
      plotTitle = os.path.basename(cliArgs["cfg"])[:-4]

    plot.title = plotTitle
    plot.xlabel = cliArgs["xlbl"]
    plot.ylabel = cliArgs["ylbl"]

    plot.legend = True
    plot.legend_loc = cliArgs["legend"]
    plot.ax_minticks = cliArgs["minticks"]
    plot.ax_grid = cliArgs["grid"]
    plot.ax_scale = cliArgs["scale"]

    if (cliArgs["save"]):
      fOutPath = pyauto_base.fs.mkOutFolder()
      title = plot.title.replace("?", "").strip()
      plot.fPath = os.path.join(fOutPath, "{}.png".format(title))
      plot.write(xtnd_x=cliArgs["xtend"])
    else:
      plot.show()

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "power-on plot"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("brd", help="board config path")
  parser.add_argument("-f",
                      "--flt",
                      default="",
                      help="comma separated list of net names to ignore")
  parser.add_argument("-l",
                      "--list",
                      action="store_true",
                      default=False,
                      help="list config file options")
  parser.add_argument("--hline", help="add horizontal lines")
  parser.add_argument("--vline", help="add vertical lines")
  parser.add_argument("--minticks",
                      action="store_true",
                      default=False,
                      help="show minor ticks")
  parser.add_argument("--grid",
                      choices=("no", "min", "maj", "both"),
                      default="maj",
                      help="change grid")
  parser.add_argument("--legend",
                      choices=("best", "ur", "cr", "lr", "ul", "cl", "ll"),
                      default="best",
                      help="legend location")
  parser.add_argument("--scale", choices=("log_x", "log_y", "log_both"), help="axis scale")
  parser.add_argument("--xlbl", default="time [ms]", help="label for x axis")
  parser.add_argument("--ylbl", default="voltage", help="label for y axis")
  parser.add_argument("--xtend",
                      type=int,
                      default=1,
                      help="when saving make image n times longer")
  parser.add_argument("-s",
                      "--save",
                      action="store_true",
                      default=False,
                      help="save plot as image")
  parser.add_argument("-n",
                      "--noplot",
                      action="store_true",
                      default=False,
                      help="stop before plotting")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
