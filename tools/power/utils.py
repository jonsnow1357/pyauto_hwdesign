#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_hw.config
import pyauto_hw.CAD.base as CADbase
import pyauto_hw.power
import pyauto_hw.utils

def _removeComponents(ntl, lstRefdes):
  logger.info("-- remove components")
  for val in lstRefdes:
    #logger.info("remove component {}".format(val))
    ntl.delComponent(val)

  ntl.refresh()

def _addComponents(ntl, lstComp):
  logger.info("-- add components")
  for cp in lstComp:
    refdes = cp[0]
    value = cp[1]
    lstNets = cp[2]
    #logger.info("add component {},{}".format(refdes, value))
    #print("DBG", cp[2])

    cp = CADbase.Component(strRefdes=refdes, strValue=value)
    ntl.addComponent(cp)

    pin = CADbase.Pin("{}.G".format(refdes))
    cp.addPin(pin)
    net = None
    for n in ("GND", "GND_SIGNAL", "GND_POWER"):
      try:
        net = ntl.getNetByName(n)
        break
      except KeyError:
        continue
    if (net is None):
      msg = "CANNOT find GND signal"
      logger.error(msg)
      raise RuntimeError(msg)
    net.addPin(pin)

    for i in range(0, len(lstNets)):
      pin = CADbase.Pin("{}.{}".format(refdes, (i + 1)))
      cp.addPin(pin)
      net = ntl.getNetByName(lstNets[i])
      net.addPin(pin)

  ntl.refresh()

def _removeNets(ntl, lstNetNames):
  netName = []
  netPat = []
  for val in lstNetNames:
    if ("*" in val):
      netPat.append("^" + val.replace("*", ".*") + "$")
    else:
      netName.append(val)

  logger.info("-- remove nets")
  for n in netName:
    #logger.info("remove net: {}".format(n))
    ntl.delNet(n)

  for n in ntl.netNames:
    for val in netPat:
      if (re.match(val, n)):
        #logger.info("remove net: {}".format(n))
        ntl.delNet(n)

  ntl.refresh()

def _mergeNets(ntl, lstNetNames):
  logger.info("-- merge nets")
  for val in lstNetNames:
    #logger.info("merge nets: {}".format(val))
    ntl.mergeNet(val, bFirstName=True)
    ntl.refresh()  # in the loop since multiple merges are allowed

def _renameNets(ntl, lstNetNames):
  logger.info("-- rename nets")
  for val in lstNetNames:
    #logger.info("rename net: {}".format(val))
    ntl.renameNet(val[0], val[1])
  ntl.refresh()

def modifyNetlist(ntl, cfgObj):
  logger.info("== modify netlist")

  if (len(cfgObj.compRemove) > 0):
    _removeComponents(ntl, cfgObj.compRemove)

  if (len(cfgObj.netRemove) > 0):
    _removeNets(ntl, cfgObj.netRemove)

  if (len(cfgObj.netMerge) > 0):
    _mergeNets(ntl, cfgObj.netMerge)

  if (len(cfgObj.netRename) > 0):
    _renameNets(ntl, cfgObj.netRename)

  if (len(cfgObj.compAdd) > 0):
    _addComponents(ntl, cfgObj.compAdd)

def readNetlist(brdCfg):
  logger.info("== read netlist")

  if ("netlist" not in brdCfg.path.keys()):
    msg = "MISSING 'netlist' path"
    logger.error(msg)
    raise RuntimeError(msg)
  if (pyauto_base.misc.isEmptyString(brdCfg.path["netlist"])):
    msg = "MISSING 'netlist' path"
    logger.error(msg)
    raise RuntimeError(msg)
  ntl = pyauto_hw.utils.readNetlist(brdCfg.path["netlist"])
  return ntl
