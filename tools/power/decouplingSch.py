#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_hw.config
import pyauto_hw.power
import pyauto_hw.part
import pyauto_hw.power.constants as hwPwrCt
import pyauto_hwdesign.parts_db.spice
import pyauto_hwdesign.tools.power.utils

_libPath = os.path.join(pyauto_hw.part.getPath_LocalDB(), "spice")
_MIN_CAPS = 3

def _generateSpice(path, pwrRail):
  # read spice models
  dict_spice = {}
  for mdl in pyauto_hwdesign.parts_db.spice.getSpiceModels():
    dict_spice[mdl.PN] = [mdl.spice_path, mdl.spice_subckt]

  logger.info("generate SPICE model for {}".format(pwrRail.name))
  set_includes = set()
  lst_caps = []
  for pn, qty in pwrRail.decoupling.items():
    try:
      if (dict_spice[pn][0] != ""):
        set_includes.add(os.path.normpath(os.path.join(_libPath, dict_spice[pn][0])))
        for _ in range(qty):
          lst_caps.append("XC{} {} 0 {}".format(len(lst_caps), pwrRail.name,
                                                dict_spice[pn][1]))
      else:
        logger.warning("IGNORING part with no Spice model: {}".format(pn))
    except KeyError as ex:
      msg = "{} decoupling:\n\t{}".format(pwrRail.name, list(pwrRail.decoupling.keys()))
      logger.info(msg)
      raise RuntimeError("MISSING SPICE model {}".format(ex))
  #print("DBG", lst_includes)
  #print("DBG", lst_caps)

  if (len(lst_caps) == 0):
    logger.warning("NO CAPS on {}".format(pwrRail.name))
    return
  if (len(lst_caps) < _MIN_CAPS):
    logger.warning("TOO FEW CAPS on {}".format(pwrRail.name))
    return

  tpl_path = os.path.join("templates", "supply.tpl")
  dict_tpl = {"name": pwrRail.name, "includes": list(set_includes), "parts": lst_caps}
  pyauto_base.misc.templateWheezy(tpl_path, path, dictTpl=dict_tpl)

def mainApp():
  brdCfg = pyauto_hw.config.BoardConfig(cliArgs["brd"])
  if (cliArgs["list"]):
    brdCfg.showInfo()
    return

  for pwr_cfg_id, pwr_cfg in brdCfg.pwr.items():
    logger.info("== power estimate {}/{}".format(brdCfg.name, pwr_cfg_id))
    ntl = pyauto_hwdesign.tools.power.utils.readNetlist(brdCfg)
    pyauto_hwdesign.tools.power.utils.modifyNetlist(ntl, pwr_cfg)

    pwr_tree = pyauto_hw.power.PowerTree(pwr_cfg_id)
    #pwr_tree.modelPath = brdCfg.path["power"]
    pwr_tree.cfg = {
        "cases": pwr_cfg.modelCases,
        "efficiencies": pwr_cfg.modelEfficiencies,
        "ports": pwr_cfg.powerPorts,
        "sense": pwr_cfg.powerSense
    }
    pwr_tree.readNetlist(ntl)
    pwr_tree.build()
    if (cliArgs["tree"]):
      pwr_tree.showTree()
      return

    pwr_tree.updatePower()
    logger.info(pwr_tree)

    out_folder = pyauto_base.fs.mkOutFolder(
        os.path.join("data_out", brdCfg.name.replace(" ", "_"), pwr_tree.name, "spice"))

    set_rails = set()
    for rn in pwr_tree.getAllRailNames_postorder():
      pr = pwr_tree.getRail(rn)
      set_ports = pr.setPorts
      if (hwPwrCt.PwrPortSupply in [pwr_tree.getPort(p).type for p in set_ports]):
        set_rails.add(rn)

    for rn in set_rails:
      pr = pwr_tree.getRail(rn)
      f_out_path = os.path.join(out_folder, "{}.cir".format(pr.name))
      _generateSpice(f_out_path, pr)

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "power estimate"
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  parser.add_argument("brd", help="board config path")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  parser.add_argument("-l",
                      "--list",
                      action="store_true",
                      default=False,
                      help="list config file options")
  parser.add_argument("-t",
                      "--tree",
                      action="store_true",
                      default=False,
                      help="stop after power tree processing")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
