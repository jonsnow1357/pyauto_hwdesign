@require(name, includes, parts)
.TITLE @{name} power rail decoupling
@for path in includes:
.INCLUDE @{path}
@end

ISRC @{name} 0 ac 1.0
R @{name} 0 100Meg
@for line in parts:
@{line}
@end

.control
ac dec 50 1k 10G
let z_rail = log10(v(@{name}))
set hcopydevtype = svg
set color0 = white
set color1 = black
hardcopy @{name}.svg z_rail title '@{name}' xlabel 'freq' ylabel 'log impedance [ohm]'
wrdata @{name}.txt z_rail
quit
.endc

.end
