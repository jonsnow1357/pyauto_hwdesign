#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_hw.config
import pyauto_hw.power

def _mkReports(pwrTree):
  if (pyauto_base.misc.isEmptyString(pwrTree.name)):
    path_folder = pyauto_base.fs.mkOutFolder()
  else:
    path_folder = pyauto_base.fs.mkOutFolder(
        os.path.join("data_out", pwrTree.name.replace(" ", "_")))

  pwrTree.writeReport_CSV(os.path.join(path_folder, "{}.csv".format(modName)))
  pwrTree.writeReport_HTML(os.path.join(path_folder, "{}.html".format(modName)))
  pwrTree.writeReport_sense_CSV(os.path.join(path_folder, "{}_sense.csv".format(modName)))

def _mkDiagrams(pwrTree):
  if (pyauto_base.misc.isEmptyString(pwrTree.name)):
    path_folder = pyauto_base.fs.mkOutFolder()
  else:
    path_folder = pyauto_base.fs.mkOutFolder(
        os.path.join("data_out", pwrTree.name.replace(" ", "_")))

  pwrTree.writeDot(os.path.join(path_folder, "{}_graph.dot".format(modName)))

def mainApp():
  brdCfg = pyauto_hw.config.BoardConfig(cliArgs["brd"])
  if (cliArgs["list"]):
    brdCfg.showInfo()
    return

  lst_path = []
  for f in os.listdir(cliArgs["brd"]):
    if (not f.lower().endswith("pwr.tree.xml")):
      continue
    lst_path.append(os.path.join(cliArgs["brd"], f))
  if (len(lst_path) == 0):
    return
  if (len(lst_path) != 1):
    msg = "TOO MANY power trees"
    logger.error(msg)
    raise RuntimeError(msg)

  # read netlist and create power tree
  pwr_tree = pyauto_hw.power.PowerTree(brdCfg.name)
  #pwr_tree.modelPath = brdCfg.path["power"]

  pwr_tree.readXML(lst_path[0])
  pwr_tree.build()
  if (cliArgs["tree"]):
    pwr_tree.showTree()
    return

  pwr_tree.updatePower()
  logger.info(pwr_tree)
  if (cliArgs["power"]):
    return

  _mkReports(pwr_tree)
  _mkDiagrams(pwr_tree)

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "power estimate"
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  parser.add_argument("brd", help="board config path")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  parser.add_argument("-l",
                      "--list",
                      action="store_true",
                      default=False,
                      help="list config file options")
  parser.add_argument("-t",
                      "--tree",
                      action="store_true",
                      default=False,
                      help="stop after power tree processing")
  parser.add_argument("-p",
                      "--power",
                      action="store_true",
                      default=False,
                      help="stop after power tree update")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
