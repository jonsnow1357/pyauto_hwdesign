#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_hwdesign.parts_db.model.base

tosa1 = pyauto_hwdesign.parts_db.model.base.TOSA()  # ZKTel, ZB34-5522-226
tosa1.wl = 1310
tosa1.laserType = "FP"
tosa1.rate = [1.244e9, None]
tosa1.temperature = [-40, 85]
tosa1.VF = 1.5
tosa1.Ith = {25: 15.0, 85: 35.0}
tosa1.Pout = {25: [0.3, 0.9], 85: [0.15, 0.9]}
tosa1.SE = {25: [0.01, 0.03], 85: [0.005, 0.015]}

def _getPower(tosa):
  logger.info("== using {} TOSA: {} [{}]".format(tosa.laserType, tosa.mfrPN, tosa.mfr))
  Vcc = 3.3
  I_bias = cliArgs["bias"]
  I_mod = cliArgs["mod"]
  Rd = cliArgs["Rd"]
  logger.info("VCC: {} V, coupling: {}".format(Vcc, cliArgs["coupling"].upper()))
  logger.info("bias: {:.3f} mA".format(I_bias))
  logger.info("mod:  {:.3f} mA".format(I_mod))

  if (cliArgs["coupling"] == "dc"):
    pwr_LD = tosa.VF * (I_bias + (I_mod / 2.0))
  elif (cliArgs["coupling"] == "ac"):
    pwr_LD = tosa.VF * I_bias
  else:
    raise RuntimeError()
  logger.info("pwr_LD: {:.3f} mW".format(pwr_LD))

  pwr_bias = (Vcc - tosa.VF) * (I_bias * I_bias * 1e-3)
  logger.info("pwr_bias: {:.3f} mW".format(pwr_bias))

  if (cliArgs["coupling"] == "dc"):
    pwr_mod1 = (Vcc - tosa.VF) * (I_mod / 2)
  elif (cliArgs["coupling"] == "ac"):
    pwr_mod1 = Vcc * (I_mod / 2)
  else:
    raise RuntimeError()
  logger.info("pwr_mod1: {:.3f} mW".format(pwr_mod1))

  pwr_mod2 = Vcc * (I_mod / 2)
  logger.info("pwr_mod2: {:.3f} mW".format(pwr_mod2))

  pwr = pwr_LD + pwr_bias + pwr_mod1 + pwr_mod2
  logger.info("TOSA pwr: {:.3f} mW".format(pwr))

  pwr_Rd = Rd * (I_mod / 2) * (I_mod / 2) * 1e-3
  logger.info("Rd pwr: {:.3f} mW ({:.1f} ohm)".format(pwr_Rd, Rd))

def mainApp():
  _getPower(tosa1)

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "TOSA power estimate"
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("--coupling", choices=("dc", "ac"), default="dc")
  parser.add_argument("--bias", type=float, default=15.0, help="bias current [mA]")
  parser.add_argument("--mod", type=float, default=10.0, help="mod current [mA]")
  parser.add_argument("--Rd", type=float, default=15.0, help="R series, OUT+ path [ohm]")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
