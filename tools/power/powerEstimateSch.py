#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_hw.config
import pyauto_hw.power
import pyauto_hwdesign.tools.power.utils

def mainApp():
  brdCfg = pyauto_hw.config.BoardConfig(cliArgs["brd"])
  if (cliArgs["list"]):
    brdCfg.showInfo()
    return

  for pwr_cfg_id, pwr_cfg in brdCfg.pwr.items():
    logger.info("== power estimate {}/{}".format(brdCfg.name, pwr_cfg_id))
    ntl = pyauto_hwdesign.tools.power.utils.readNetlist(brdCfg)
    pyauto_hwdesign.tools.power.utils.modifyNetlist(ntl, pwr_cfg)

    pwr_tree = pyauto_hw.power.PowerTree(pwr_cfg_id)
    #pwr_tree.modelPath = brdCfg.path["power"]
    pwr_tree.cfg = {
        "cases": pwr_cfg.modelCases,
        "efficiencies": pwr_cfg.modelEfficiencies,
        "ports": pwr_cfg.powerPorts,
        "sense": pwr_cfg.powerSense
    }
    pwr_tree.readNetlist(ntl)
    if (cliArgs["nlist"]):
      return

    pwr_tree.build()
    if (cliArgs["tree"]):
      pwr_tree.showTree()
      return

    pwr_tree.updatePower()
    logger.info(pwr_tree)
    if (cliArgs["power"]):
      return

    out_folder = pyauto_base.fs.mkOutFolder(
        os.path.join("data_out", brdCfg.name.replace(" ", "_"), pwr_tree.name))

    pwr_tree.writeReport_CSV(os.path.join(out_folder, "{}.csv".format(modName)))
    pwr_tree.writeReport_HTML(os.path.join(out_folder, "{}.html".format(modName)))
    pwr_tree.writeReport_sense_CSV(os.path.join(out_folder, "{}_sense.csv".format(modName)))
    pwr_tree.writeDot(os.path.join(out_folder, "{}_graph.dot".format(modName)))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "power estimate"
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  parser.add_argument("brd", help="board config path")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  parser.add_argument("-l",
                      "--list",
                      action="store_true",
                      default=False,
                      help="list config file options")
  parser.add_argument("-n",
                      "--nlist",
                      action="store_true",
                      default=False,
                      help="stop after netlist parsing")
  parser.add_argument("-t",
                      "--tree",
                      action="store_true",
                      default=False,
                      help="stop after power tree processing")
  parser.add_argument("-p",
                      "--power",
                      action="store_true",
                      default=False,
                      help="stop after power tree update")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
