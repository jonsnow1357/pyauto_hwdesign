#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import json
import argparse
import prettytable

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_hw.config

def _list(dictBrd):
  tbl = prettytable.PrettyTable()
  tbl.field_names = ["Name", "SCH", "PCB", "PWR"]
  for bname in sorted(dictBrd.keys()):
    brd = dictBrd[bname]
    # yapf: disable
    tbl.add_row([bname, ("-" if (brd.sch is None) else "yes"),
                 ("-" if (brd.pcb.stackup is None) else "{:d} ly".format(brd.pcb.stackup.nLayers)),
                 ("-" if (len(brd.pwr) == 0) else "yes")
                 ])
    # yapf: enable
  print(tbl)

def _list_vars(dictBrd):
  lst_vars = []
  for brd in dictBrd.values():
    lst_vars += brd.vars.keys()
  lst_vars = sorted(list(set(lst_vars)))

  tbl = prettytable.PrettyTable()
  tbl.field_names = ["Name"] + lst_vars
  for bname in sorted(dictBrd.keys()):
    brd = dictBrd[bname]
    row = [bname]
    for v in lst_vars:
      if (v in brd.vars.keys()):
        row.append(brd.vars[v])
      else:
        row.append("-")
    tbl.add_row(row)
  print(tbl)

def _list_paths(dictBrd):
  lst_paths = []
  for brd in dictBrd.values():
    lst_paths += brd.path.keys()
  lst_paths = sorted(list(set(lst_paths)))

  tbl = prettytable.PrettyTable()
  tbl.field_names = ["Name"] + lst_paths
  for bname in sorted(dictBrd.keys()):
    brd = dictBrd[bname]
    row = [bname]
    for p in lst_paths:
      if (p not in brd.path.keys()):
        row.append("-")
      elif (os.path.isdir(brd.path[p])):
        row.append("OK")
      else:
        row.append("FAIL")
    tbl.add_row(row)
  print(tbl)

def mainApp():
  logger.info("using path: '{}'".format(cliArgs["path"]))
  dict_boards = {}
  for dirpath, dirs, files in os.walk(cliArgs["path"]):
    for d in dirs:
      try:
        brd = pyauto_hw.config.BoardConfig(os.path.join(dirpath, d))
      except pyauto_hw.config.BoardConfigException:
        continue
      except json.decoder.JSONDecodeError as ex:
        logger.error(ex)
        logger.error(os.path.join(dirpath, d))
        raise RuntimeError
      dict_boards[brd.name] = brd
  logger.info("found {} boards(s)".format(len(dict_boards)))

  if (cliArgs["vars"] is True):
    _list_vars(dict_boards)
  elif (cliArgs["paths"] is True):
    _list_paths(dict_boards)
  else:
    _list(dict_boards)

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "list boards info"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("path", help="path")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("-v",
                      "--vars",
                      action="store_true",
                      default=False,
                      help="show board variables")
  parser.add_argument("-p",
                      "--paths",
                      action="store_true",
                      default=False,
                      help="show board path status")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
