#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse
import stat

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.db
import pyauto_base.config
import pyauto_base.misc
import pyauto_hw.part
import pyauto_hwdesign.parts_db.spice

_libPath = os.path.join(pyauto_hw.part.getPath_LocalDB(), "spice")
_workPath = pyauto_base.fs.mkAbsolutePath("WORK/dev/hw/spice/discretes", bExists=False)

def _check_component(cp):
  path = os.path.normpath(os.path.join(cp.spice_path))
  pyauto_base.fs.chkPath_File(path)

  subckt = None
  with open(path, "r") as fIn:
    for ln in fIn.readlines():
      if (ln.startswith(".SUBCKT")):
        subckt = ln.split()[1]
  if (subckt is None):
    raise RuntimeError
  if (subckt != cp.spice_subckt):
    logger.error("INCORRECT SUBCKT: should be '{}'".format(subckt))

def _list():
  lst_cp = pyauto_hwdesign.parts_db.spice.getSpiceModels()

  for cp in lst_cp:
    logger.info(cp)
  logger.info("found {:d} component(s)".format(len(lst_cp)))

def _check():
  lst_cp = pyauto_hwdesign.parts_db.spice.getSpiceModels()

  for cp in lst_cp:
    logger.info(cp)
    if (cp.spice_path == ""):
      continue
    _check_component(cp)
  logger.info("found {:d} component(s)".format(len(lst_cp)))

def _ngspice():
  pyauto_base.fs.mkOutFolder(_workPath, bClear=True)
  lst_cp = pyauto_hwdesign.parts_db.spice.getSpiceModels()
  lst_cp = [cp for cp in lst_cp if (cp.spice_path != "")]

  for cp in lst_cp:
    logger.info(cp)
    cp.mkSpiceFixture(_workPath)
  logger.info("found {:d} component(s)".format(len(lst_cp)))

  if (sys.platform.startswith("win")):
    path_tpl = "./templates/ngspice.bat.tpl"
    path_out = os.path.join(_workPath, "run.bat")
  elif (sys.platform.startswith("linux")):
    path_tpl = "./templates/ngspice.sh.tpl"
    path_out = os.path.join(_workPath, "run.sh")
  dict_tpl = {"lstCp": [cp.mfr_pn for cp in lst_cp if (cp.mfr_pn != "")]}
  pyauto_base.misc.templateWheezy(path_tpl, path_out, dictTpl=dict_tpl)
  st = os.stat(path_out)
  os.chmod(path_out, st.st_mode | stat.S_IEXEC)

  dictCp = {}
  for cp in lst_cp:
    if (cp.PN.startswith("test")):
      continue
    dictCp[cp.PN] = [cp.__class__.__name__.lower(), cp.mfr, cp.mfr_pn]
  path_tpl = "./templates/spice.html.tpl"
  dict_tpl = {"dictCp": dictCp}
  path_out = os.path.join(_workPath, "spice.html")
  pyauto_base.misc.templateWheezy(path_tpl, path_out, dictTpl=dict_tpl)

def mainApp():
  if (cliArgs["action"] == "list"):
    _list()
  elif (cliArgs["action"] == "check"):
    _check()
  elif (cliArgs["action"] == "spice"):
    _ngspice()

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "SPICE db manager"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("action", help="action", choices=("list", "check", "spice"))
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
