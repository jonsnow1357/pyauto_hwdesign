#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_hw.CAD.base as CADbase
import pyauto_hw.CAD.HDL as HDL

def _csv2csv():
  cp1 = CADbase.Component()
  cp1.readCSV(cliArgs["inPath"])

  cp2 = CADbase.Component()
  cp2.readCSV(cliArgs["outPath"])
  return cp1, cp2

def _csv2hdl():
  cellName = os.path.basename(cliArgs["outPath"])
  cell = HDL.Cell(cellName, cliArgs["outPath"])
  cell.read()
  logger.info(cell)
  cp1 = cell.getComponent()

  cp2 = CADbase.Component()
  cp2.readCSV(cliArgs["inPath"])

  return cp1, cp2

def mainApp():
  if (cliArgs["action"] == "csv2csv"):
    cp1, cp2 = _csv2csv()
  elif (cliArgs["action"] == "csv2hdl"):
    cp1, cp2 = _csv2hdl()
  else:
    raise RuntimeError

  cmpObj = CADbase.PinCompare()
  cmpObj.cp1 = cp1
  cmpObj.cp2 = cp2

  #cmpObj.compareName = "fuzzy=0.93"
  #cmpObj.compareName = "replace=2=.=_"
  #cmpObj.compareName = "no_underscore"
  #cmpObj.compareName = "prefix_only"
  cmpObj.bCompareType = True
  cmpObj.bCompareBank = cliArgs["bank"]
  cmpObj.bCompareSide = cliArgs["side"]
  cmpObj.compare()
  cmpObj.showInfo()
  cmpObj.showReport()

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "component pin compare"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("action", help="action", choices=("csv2csv", "csv2hdl"))
  parser.add_argument("inPath", help="input file path")
  parser.add_argument("outPath", help="output file path")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("-b",
                      "--bank",
                      action="store_true",
                      default=False,
                      help="compare banks")
  parser.add_argument("-s",
                      "--side",
                      action="store_true",
                      default=False,
                      help="compare side")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
