#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_base.fs
import pyauto_hw.CAD.base as CADbase
import pyauto_hw.CAD.HDL as HDL

def _matrix2pins():
  """
  Reads pin names in a matrix from a csv files and converts to a pins.csv file
  """
  res = []
  with open(cliArgs["inPath"], "r") as fIn:
    csvIn = csv.reader(fIn)
    for row in csvIn:
      res.append(row)

  if ((res[0][1] == "1") and (res[0][2] == "2")):
    fFormat = 1
  elif ((res[0][1] == "A") and (res[0][2] == "B")):
    fFormat = 2
  else:
    msg = "INCORRECT first row: {}".format(res[0])
    logger.error(msg)
    raise RuntimeError(msg)

  pin_no_x = {}
  cnt_x = 0
  for i, r in enumerate(res[0]):
    if (re.match(r"^([0-9]{1,2}|[A-HJ-RT-Z][A-Z]?)$", r) is None):
      continue
    cnt_x += 1
    pin_no_x[i] = r

  cnt_y = 0
  cp = CADbase.Component()
  for row in res[1:]:
    if (row[0] == ""):
      continue
    if (re.match(r"^([0-9]{1,2}|[A-HJ-RT-Z][A-Z]?)$", row[0]) is None):
      msg = "INCORRECT row: {}".format(row)
      logger.error(msg)
      raise RuntimeError(msg)

    cnt_y += 1
    pin_no_y = row[0]
    for i, r in enumerate(row):
      if i in pin_no_x.keys():
        pin_name = r
        if (pin_name == ""):
          continue
        if (fFormat == 1):
          pin_no = "{}{}".format(pin_no_y, pin_no_x[i])
        elif (fFormat == 2):
          pin_no = "{}{}".format(pin_no_x[i], pin_no_y)
        else:
          raise RuntimeError
        pin = CADbase.Pin()
        pin.name = pin_name
        pin.number = pin_no
        pin.guessType()
        #print("DBG", pin)
        cp.addPin(pin)
  logger.info("found {} x {} array".format(cnt_x, cnt_y))

  # fix duplicate pin names
  dictRes = cp.getDuplicatePinNames()
  for pin_name, lstP in sorted(dictRes.items()):
    #logger.warning("duplicate pin name: {} at {}".format(pin_name, [p.number for p in lstP]))
    cnt = 0
    for pin in sorted(lstP, key=lambda x: getattr(x, "number")):
      #pin.name = "{}{}".format(pin.name, (cnt + 1))
      #pin.name = "{}_{}".format(pin.name, (cnt + 1))
      pin.name = "{}_{}".format(pin.name, pin.number)
      cnt += 1

  logger.info(cp)
  cp.writeCSV(cliArgs["outPath"])

def _tsv2pins():
  """
  Reads a BGA tab separated txt file with pin info and converts it to a pins.csv file
  """
  pyauto_base.fs.chkPath_File(cliArgs["inPath"])

  hdrRow = ["Pin Number", "Pin Use", "Net Name"]

  cp = CADbase.Component()
  cp.value = os.path.basename(cliArgs["inPath"])[:-4].upper()
  with open(cliArgs["inPath"], "r") as fIn:
    csvIn = csv.reader(fIn, delimiter="\t")

    for row in csvIn:
      #logger.info(row)
      if (len(row) < 2):
        continue
      if (row[0] == hdrRow[0]):
        dictIdx = pyauto_base.misc.getListIndexByName(row, hdrRow)
        print("DBG", dictIdx)
        bParse = True
        continue

      if (bParse):
        pin = CADbase.Pin("U?.{}".format(row[dictIdx[hdrRow[0]]]))
        pin.name = row[dictIdx[hdrRow[2]]]
        if (pin.name == ""):
          pin.name = "NC_{}".format(pin.number)
        elif (pin.name == "VSS"):
          pin.name = "VSS_{}".format(pin.number)
        elif (pin.name == "VDD"):
          pin.name = "VDD_{}".format(pin.number)
        pin.guessType()
        cp.addPin(pin)

  logger.info(cp)
  cp.writeCSV(cliArgs["outPath"])

def _csv2json():
  cp = CADbase.Component()
  cp.readCSV(cliArgs["inPath"])
  print(cp)
  cp.writeJSON(cliArgs["outPath"])

def _json2csv():
  cp = CADbase.Component()
  cp.readJSON(cliArgs["inPath"])
  print(cp)
  cp.writeCSV(cliArgs["outPath"])

def _pins2hdlcsv():
  cp = CADbase.Component()
  cp.readCSV(cliArgs["inPath"])
  print(cp)
  cp.writeCSV_cadence_hdl(cliArgs["outPath"])

def _hdlcsv2pins():
  cp = CADbase.Component()
  cp.readCSV_cadence_hdl(cliArgs["inPath"])
  print(cp)
  cp.writeCSV(cliArgs["outPath"])

def _pins2hdlsym():
  """
  Reads a pins.csv file and coverts to an HDL symbol
  """
  cp = CADbase.Component()
  cp.readCSV(cliArgs["inPath"])
  logger.info(cp)

  cellName = os.path.basename(cliArgs["outPath"])
  cell = HDL.Cell(cellName, cliArgs["outPath"])
  cell.read()
  logger.info(cell)

  cell.updatePins(cp)
  if (cliArgs["yes"]):
    cell.write()

def _hdlsym2pins():
  """
  Reads an HDL symbol and coverts to a pins.csv file
  """
  cellName = os.path.basename(cliArgs["inPath"])
  cell = HDL.Cell(cellName, cliArgs["inPath"])
  cell.read()
  logger.info(cell)

  cp = CADbase.Component()
  for prim in cell.primitives:
    for p in prim.pins.values():
      pin = CADbase.mkPin("U?", p.number, p.name, p.type)
      cp.addPin(pin)
  logger.info(cp)
  cp.writeCSV(cliArgs["outPath"])

def mainApp():
  if (cliArgs["action"] == "mat2pins"):
    _matrix2pins()
  elif (cliArgs["action"] == "txt2pins"):
    _tsv2pins()
  elif (cliArgs["action"] == "pins2json"):
    _csv2json()
  elif (cliArgs["action"] == "json2pins"):
    _json2csv()
  elif (cliArgs["action"] == "pins2hdlcsv"):
    _pins2hdlcsv()
  elif (cliArgs["action"] == "hdlcsv2pins"):
    _hdlcsv2pins()
  elif (cliArgs["action"] == "pins2hdlsym"):
    _pins2hdlsym()
  elif (cliArgs["action"] == "hdlsym2pins"):
    _hdlsym2pins()

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "convert pin file(s)"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("action",
                      help="action",
                      choices=("mat2pins", "txt2pins", "pins2json", "json2pins",
                               "pins2hdlcsv", "hdlcsv2pins", "pins2hdlsym", "hdlsym2pins"))
  parser.add_argument("inPath", help="input file path")
  parser.add_argument("outPath", help="output file path")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("-y",
                      "--yes",
                      action="store_true",
                      default=False,
                      help="write to file/database")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
