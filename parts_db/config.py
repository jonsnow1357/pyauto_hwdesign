#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
import csv
import shutil

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.config
import pyauto_base.db

class Model_DBController(pyauto_base.db.DBController):

  def __init__(self):
    super(Model_DBController, self).__init__()

  def getAllParts_Info(self):
    res = []
    rset = self.selectTable("parts")
    if (self.dbInfo.backend == pyauto_base.db.dbBackendCsv):
      col_names = rset[0]
      #print("DBG", col_names)
      for row in rset[1:]:
        if (row[0] != ""):
          res.append(row)
    else:
      raise NotImplementedError

    return res

  def _updateModel_CSV(self, pn, mfr_pn, freq):
    path1 = os.path.join(self.dbInfo.dbname, "parts.csv")
    path2 = os.path.join(self.dbInfo.dbname, "parts.bkp")
    shutil.move(path1, path2)

    with open(path2, "r") as fIn:
      csvIn = csv.reader(fIn)
      with open(path1, "w") as fOut:
        csvOut = csv.writer(fOut, lineterminator="\n")
        for row in csvIn:
          if ((pn is None) and (mfr_pn is not None)):
            if (row[3] == mfr_pn):
              row[5] = freq
          csvOut.writerow(row)
    os.remove(path2)

  def updateModel(self, pn, mfr_pn, freq):
    logger.info("updateModel {}/{}".format(pn, mfr_pn))
    if (self.dbInfo.backend == pyauto_base.db.dbBackendCsv):
      self._updateModel_CSV(pn, mfr_pn, freq)
