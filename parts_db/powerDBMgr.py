#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_hw.power as hwPwr
import pyauto_hw.part

_libPath = os.path.join(pyauto_hw.part.getPath_LocalDB(), "power")

def _list():
  pmdb = hwPwr.PowerModelDB()
  pmdb.path = _libPath
  pmdb.load()
  for v in pmdb.getAllValues():
    mdl = pmdb.get(v)
    mdl.load()
    logger.info("  {: <24}:  {: <6} ({})".format(v, mdl.type, ",".join(mdl.cases)))

def _check():
  pmdb = hwPwr.PowerModelDB()
  pmdb.path = _libPath
  pmdb.load()
  for v in pmdb.getAllValues():
    mdl = pmdb.get(v)
    mdl.load()
    logger.info("checking {: <24}: {: <6} ({})".format(mdl.name, mdl.type,
                                                       ",".join(mdl.cases)))
    mdl.check()

def mainApp():
  if (cliArgs["action"] == "list"):
    _list()
  elif (cliArgs["action"] == "check"):
    _check()

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "power db manager"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("action", help="action", choices=("list", "check"))
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
