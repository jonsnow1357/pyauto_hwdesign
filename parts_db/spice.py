#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")

import pyauto_base.fs
import pyauto_base.db
import pyauto_base.config
import pyauto_hw.part
import pyauto_hwdesign.parts_db.config

_libPath = os.path.join(pyauto_hw.part.getPath_LocalDB(), "spice")
_dbCfg = pyauto_base.config.DBConfig()
_dbCtl = pyauto_hwdesign.parts_db.config.Model_DBController()

def getSpiceModels():
  db_info = pyauto_base.db.DBInfo()
  db_info.setInfo({"backend": "csv", "dbname": _libPath})
  _dbCfg.dictDBInfo["0"] = db_info
  #dbCfg.showInfo()

  _dbCtl.dbInfo = _dbCfg.dictDBInfo["0"]
  _dbCtl.connect()

  lst_cp = []

  for row in _dbCtl.getAllParts_Info():
    if (row[0] == pyauto_hw.part.typeCapacitor):
      cp = pyauto_hw.part.Capacitor()
    elif (row[0] == pyauto_hw.part.typeFerriteBead):
      cp = pyauto_hw.part.FerriteBead()
    else:
      msg = "UNEXPECTED type: {}".format(row[0])
      logger.error(msg)
      raise NotImplementedError(msg)
    cp.PN = row[1]
    cp.mfr = row[2]
    cp.mfr_pn = row[3]
    if (row[4] != ""):
      cp.spice_path = os.path.join(_libPath, row[0], row[2], row[4])
    cp.spice_subckt = row[5]
    lst_cp.append(cp)

  _dbCtl.disconnect()

  return lst_cp
