@require(lstCp)
#!/bin/sh
set -eu
################################################################################
rm -vf *.svg
rm -vf *.txt

@for c in lstCp:
ngspice -b @{c}.cir
if [ $? -ne 0 ]
then
  printf "== ERROR\n"
  exit
fi
@end
printf "== DONE\n"
