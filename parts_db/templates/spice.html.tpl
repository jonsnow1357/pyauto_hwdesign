@require(dictCp)
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>spice report</title>
  <style>
    table, th, td {border: 1px solid;}
  </style>
</head>

<body>

<table>
  <thead>
    <tr>
      <th>PN</th>
      <th>type</th>
      <th>MFG</th>
      <th>MFG_PN</th>
      <th>plot</th>
    </tr>
  </thead>
  <tbody>
  @for k, v in dictCp.items():
    <tr>
      <td>@{k}</td>
      <td>@{v[0]}</td>
      <td>@{v[1]}</td>
      <td>@{v[2]}</td>
      <td><a href="./@{v[2].lower()}.svg"><img src="./@{v[2].lower()}.svg" /></a></td>
    </tr>
  @end
  </tbody>
</table>

</body>
</html>
