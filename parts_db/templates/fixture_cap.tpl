@require(title, includes, parts)
.TITLE capacitor fixture for @{title}
@for path in includes:
.INCLUDE @{path}
@end

ISRC in 0 ac 1.0
R in 0 100Meg
@for line in parts:
@{line}
@end

.control
ac dec 50 1k 1G
set hcopydevtype = svg
set color0 = white
set color1 = black
hardcopy @{title}.svg db(v(in)) title '@{title}' xlabel 'freq' ylabel 'log impedance [ohm]'
wrdata @{title}.txt db(v(in))
quit
.endc

.end
