#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_hw.CAD.base as CADbase
import pyauto_hw.part

_libPath = os.path.join(pyauto_hw.part.getPath_LocalDB(), "pins")

def _check_component(cp):
  dictRes = cp.getDuplicatePinNames()
  for pn, lstP in dictRes.items():
    logger.warning("duplicate pin name: {} at {}".format(pn, [p.number for p in lstP]))
  for pn in cp.getAllPinsByNumber().keys():
    if (pn.startswith(" ")):
      logger.warning("pin number '{}' starts with space ???".format(pn))

def _list():
  lstCp = []
  for dirpath, dirs, files in os.walk(_libPath):
    for f in files:
      if (not f.lower().endswith("pins.csv")):
        continue
      cp = CADbase.Component()
      cp.readCSV(os.path.join(dirpath, f))
      lstCp.append(cp)

  for cp in lstCp:
    logger.info(cp)
  logger.info("found {:d} component(s)".format(len(lstCp)))

def _check():
  lstCp = []
  for dirpath, dirs, files in os.walk(_libPath):
    for f in files:
      if (not f.lower().endswith("pins.csv")):
        continue
      cp = CADbase.Component()
      cp.readCSV(os.path.join(dirpath, f))
      lstCp.append(cp)

  for cp in lstCp:
    logger.info(cp)
    _check_component(cp)
  logger.info("found {:d} component(s)".format(len(lstCp)))

def mainApp():
  if (cliArgs["action"] == "list"):
    _list()
  elif (cliArgs["action"] == "check"):
    if (cliArgs["pins"] is None):
      _check()
    else:
      cp = CADbase.Component()
      cp.readCSV(cliArgs["pins"])
      _check_component(cp)

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "pin db manager"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("action", help="action", choices=("list", "check"))
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("-p", "--pins", help="pins.csv path")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
