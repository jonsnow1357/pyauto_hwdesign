#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logger = logging.getLogger("lib")
import pyauto_base.test_exec

class TestBase(pyauto_base.test_exec.ContextTestCase):

  def setUp(self):
    print("")

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_alwaysPass(self):
    logger.info("using {}".format(self.TC))
    if (hasattr(self.TC, "testDelay")):
      for i in range(self.TC.testDelay):
        logger.info("{} ...".format(sys._getframe().f_code.co_name))
        time.sleep(10)

  @unittest.skip("")
  def test_alwaysSkip(self):
    logger.info("using {}".format(self.TC))

  #@unittest.skip("")
  def test_alwaysFail(self):
    logger.info("using {}".format(self.TC))
    if (hasattr(self.TC, "testDelay")):
      for i in range(self.TC.testDelay):
        logger.info("{} ...".format(sys._getframe().f_code.co_name))
        time.sleep(10)
    self.fail()

  #@unittest.skip("")
  def test_alwaysError(self):
    logger.info("using {}".format(self.TC))
    if (hasattr(self.TC, "testDelay")):
      for i in range(self.TC.testDelay):
        logger.info("{} ...".format(sys._getframe().f_code.co_name))
        time.sleep(10)
    raise NotImplementedError
