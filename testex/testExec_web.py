#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse
import json
import threading
import queue
import bottle

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.test_exec

appCfg = pyauto_base.test_exec.TestConfig()

msgq = queue.Queue(maxsize=1)
webapp = bottle.Bottle()
localIP = None

def _getLocalIP():
  import psutil

  ver = psutil.version_info
  logger.info("psutil ver: {}".format(ver))
  if (ver[0] >= 3):
    for k, v in psutil.net_if_addrs().items():
      #logger.info([k, v])
      if (sys.platform.startswith("win")):
        if (k == "Local Area Connection"):
          for nic in v:
            if (nic.family == 2):
              return nic.address
      elif (sys.platform.startswith("linux")):
        if (k == "eth0"):
          for nic in v:
            if (nic.family == 2):
              return nic.address
      else:
        msg = "UNSUPPORTED platform: {}".format(sys.platform)
        logger.error(msg)
        raise RuntimeError(msg)
  else:
    if (sys.platform.startswith("linux")):
      import socket
      #try to get the local IP, might not work reliably
      return socket.gethostbyname(socket.gethostname())
    else:
      msg = "UNSUPPORTED platform: {}".format(sys.platform)
      logger.error(msg)
      raise RuntimeError(msg)

class WorkThread(threading.Thread):

  def __init__(self):
    super(WorkThread, self).__init__()
    self.tex = pyauto_base.test_exec.TestExecutive()

  def run(self):
    appCfg.loadXmlFile(cliArgs["cfg"])
    if (cliArgs["list"]):
      appCfg.showInfo()
      return

    self.tex.paths = list(appCfg.path.values())
    self.tex.testSeq = appCfg.lstTestSeq[0]
    self.tex.TC.testDelay = 1  # for test_list.py ONLY
    self.tex.discover()
    msgq.put("done")
    while (not msgq.empty()):  # wait for GUI thread to get the message
      pass

    while (True):
      msg = msgq.get()
      logger.info("queue message: {}".format(msg))
      if (msg == "exit"):
        return
      #elif(msg == "abort"):
      #  raise NotImplementedError
      elif (msg == "run"):
        self.tex.run()
      else:
        logger.warning("UNEXPECTED queue message: {}".format(msg))

  def getStatus(self):
    res = [["status", self.tex.testStatus]]
    for tInfo in self.tex.testSeq.lstTestInfo:
      res.append([tInfo.title, tInfo.status])
    return res

workThread = WorkThread()

@webapp.get("/static/css/<filepath:re:.*\\.css>")
def css(filepath):
  return bottle.static_file(filepath, root="static/css")

@webapp.get("/static/js/<filepath:re:.*\\.js>")
def js(filepath):
  return bottle.static_file(filepath, root="static/js")

@webapp.post("/run")
def test_run():
  msgq.put("run")
  while (not msgq.empty()):
    pass

#@webapp.post("/abort")
#def test_abort():
#  msgq.put("abort")
#  while(not msgq.empty()):
#    pass

@webapp.get("/status")
def status():
  while (not msgq.empty()):
    logger.info("getStatus: waiting on message queue")

  res = workThread.getStatus()
  #res = [["test1", pyauto_base.test_exec.testStatusNotRun],
  #       ["test2", pyauto_base.test_exec.testStatusNotRun],
  #       ["test3", pyauto_base.test_exec.testStatusNotRun],]
  return json.dumps(res)

@webapp.get("/")
def index():
  dictTemplate = {
      "modName":
      modName,
      "localIP":
      localIP,
      "seqId":
      "Test Sequence" if (workThread.tex.testSeq.id is None) else workThread.tex.testSeq.id,
  }
  return bottle.template("{}.tpl".format(modName), **dictTemplate)

def mainApp():
  global localIP

  localIP = _getLocalIP()
  if (localIP is None):
    msg = "CANNOT determine local IP"
    logger.error(msg)
    raise RuntimeError(msg)

  workThread.start()
  msg = msgq.get(timeout=1.0)
  if (msg == "done"):
    logger.info("work thread started")
  else:
    msg = "UNEXPECTED reply: {}".format(msg)
    logger.error(msg)
    raise RuntimeError(msg)

  bottle.debug(True)
  #bottle.run(webapp, host="localhost", port="10001")
  bottle.run(webapp, host=localIP, port="10001")

  while (not msgq.empty()):
    msgq.get()
  msgq.put("exit")
  workThread.join()

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  appCfgPath = os.path.join(appDir, (modName + ".xml"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "test executive (web)"
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  parser.add_argument("-f", "--cfg", default=appCfgPath, help="configuration file path")
  parser.add_argument("-l",
                      "--list",
                      action="store_true",
                      default=False,
                      help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
