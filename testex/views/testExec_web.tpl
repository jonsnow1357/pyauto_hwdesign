<!doctype html>
<html lang="en">
  <head>
    <title>Test Plan for ...</title>
    <!-- <meta http-equiv="refresh" content="10"> -->
    <link rel="stylesheet" type="text/css" href="static/css/{{ modName }}.css">
    <script src="https://code.jquery.com/jquery-1.12.3.min.js"></script>
    <script src="static/js/{{ modName }}.js"></script>
  </head>

  <body>
    <div id="main_col">
      <h2 style="text-align: center">{{ seqId }}</h2>

      <div id="test_ctrl">
        <div id="test_form">
          <form>
            <fieldset>
              <legend>Test Controls</legend>
              <button type="button" id="btnRun">run</button>
            </fieldset>
          </form>
        </div>
        <div id="test_result">result</div>
        <div class="clear"></div>
      </div>

      <div id="test_list">
      </div>
    </div>
  </body>
</html>
