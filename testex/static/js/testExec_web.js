////////////////////////////////////////////////////////////////////////////////
function test_run() {
  //console.log("test_run");
  $.post("./run")
}
////////////////////////////////////////////////////////////////////////////////
function statusRefresh() {
  $.getJSON("./status").done(status);
  setTimeout(statusRefresh, 1000);
}
////////////////////////////////////////////////////////////////////////////////
function status(data) {
  var divRes = $("#test_result");
  var divLst = $("#test_list");
  divRes.empty();
  divLst.empty();
  //console.log("status: "+data.length);

  if(data.length == 0) {
    divLst.html("NO tests returned").css({"font-weight": "bold", "color": "red"});
    return;
  }

  seqResultColor = {"notrun": "lightgray", "run": "yellow", "PASS": "limegreen", "FAIL": "tomato", "ERROR": "red"};
  testResultColor = {"notrun": "", "run": "yellow", "PASS": "limegreen", "FAIL": "tomato", "ERROR": "red", "SKIP": "lightgray"};
  //console.log(data[0]);
  if(data[0][0] != "status") {
    divRes.html("NO status").css({"font-weight": "bold", "color": "red"});
    return;
  }
  seqStatus = data[0][1]
  divRes.html(seqStatus);
  divRes.css({"background-color": seqResultColor[seqStatus]});

  for(testData of data.slice(1)) {
    var testName = testData[0];
    var testStatus = testData[1];
    //console.log(testData);
    if(testStatus == "notrun") {
      var p = $("<p>").html(testName);
    }
    else {
      var p = $("<p>").html(testName + " [" + testStatus + "]").css({"background-color": testResultColor[testStatus]});
    }
    divLst.append(p);
  }
}
////////////////////////////////////////////////////////////////////////////////
$().ready(function() {
  //console.log("HERE");
  $("#btnRun").click(test_run);

  statusRefresh();
});
////////////////////////////////////////////////////////////////////////////////
